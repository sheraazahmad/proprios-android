package com.ck.proprios.dashboard.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ck.proprios.R;
import com.ck.proprios.dashboard.pojo.DashBoardMaintenance;

import java.util.ArrayList;
import java.util.List;

public class DashBoardMaintenanceAdapter extends RecyclerView.Adapter<DashBoardMaintenanceAdapter.vHolder> {

    Context mContext;
    List<DashBoardMaintenance> dashBoardMaintenanceList = new ArrayList<>();

    public DashBoardMaintenanceAdapter(Context mContext, List<DashBoardMaintenance> dashBoardMaintenanceList) {
        this.mContext = mContext;
        this.dashBoardMaintenanceList = dashBoardMaintenanceList;
    }

    @NonNull
    @Override
    public vHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.dashboard_rent_item, parent, false);
        return new DashBoardMaintenanceAdapter.vHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull vHolder holder, int position) {
        DashBoardMaintenance dashBoardMaintenance = dashBoardMaintenanceList.get(position);
        holder.rlRentName.setVisibility(View.GONE);
        holder.tvRentDatePrice.setVisibility(View.GONE);
        holder.tvUpcomingNew.setVisibility(View.GONE);
        holder.btnMore.setVisibility(View.GONE);
        holder.tvRentDate.setText(dashBoardMaintenance.getIssueDateShort());
        holder.tvRentYear.setText(dashBoardMaintenance.getIssueTime());
        holder.tvRentHeading.setText(dashBoardMaintenance.getIssueName());
        holder.tvRentSubHeading.setText(dashBoardMaintenance.getPropertyDetails());
        if (dashBoardMaintenance.getIssueStatus() == 0) {
            holder.tvRentStatusPrice.setText(dashBoardMaintenance.getIssueStatusText());
            holder.tvRentStatusPrice.setBackground(mContext.getResources().getDrawable(R.drawable.unpaid_bg));
            holder.tvRentStatusPrice.setTextColor(mContext.getResources().getColor(R.color.colorFA));
        } else if (dashBoardMaintenance.getIssueStatus() == 1) {
            holder.tvRentStatusPrice.setText(dashBoardMaintenance.getIssueStatusText());
            holder.tvRentStatusPrice.setBackground(mContext.getResources().getDrawable(R.drawable.purple_bg));
            holder.tvRentStatusPrice.setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (dashBoardMaintenance.getIssueStatus() == 2) {
            holder.tvRentStatusPrice.setText(dashBoardMaintenance.getIssueStatusText());
        } else if (dashBoardMaintenance.getIssueStatus() == 3) {
            holder.tvRentStatusPrice.setText(dashBoardMaintenance.getIssueStatusText());
            holder.tvRentStatusPrice.setBackground(mContext.getResources().getDrawable(R.drawable.owed_bg));
            holder.tvRentStatusPrice.setTextColor(mContext.getResources().getColor(R.color.colorF3));
        } else {

        }
    }

    @Override
    public int getItemCount() {
        return dashBoardMaintenanceList.size();
    }

    public class vHolder extends RecyclerView.ViewHolder {

        TextView tvRentHeading, tvRentSubHeading, tvRentDate, tvRentName, tvUpcomingNew,
                tvRentDatePrice, tvRentYear, tvRentStatusPrice;
        View dashBoardRentSeparator;
        RelativeLayout rlRentName, rlRentDateYear;
        ImageView btnMore;

        public vHolder(@NonNull View itemView) {
            super(itemView);
            rlRentName = itemView.findViewById(R.id.rlRentName);
            rlRentDateYear = itemView.findViewById(R.id.rlRentDateYear);
            tvRentStatusPrice = itemView.findViewById(R.id.tvRentStatusPrice);
            tvUpcomingNew = itemView.findViewById(R.id.tvUpcomingNew);
            tvRentDatePrice = itemView.findViewById(R.id.tvRentDatePrice);
            tvRentYear = itemView.findViewById(R.id.tvRentYear);
            dashBoardRentSeparator = itemView.findViewById(R.id.dashBoardRentSeparator);
            tvRentHeading = itemView.findViewById(R.id.tvRentHeading);
            tvRentSubHeading = itemView.findViewById(R.id.tvRentSubHeading);
            tvRentDate = itemView.findViewById(R.id.tvRentDate);
            tvRentName = itemView.findViewById(R.id.tvRentName);
            btnMore = itemView.findViewById(R.id.btnMore);
        }
    }
}
