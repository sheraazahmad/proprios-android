package com.ck.proprios.dashboard.controller.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ck.proprios.R;
import com.ck.proprios.dashboard.adapters.DashBoardUpcomingRentAdapter;
import com.ck.proprios.dashboard.pojo.DashBoardRent;
import com.ck.proprios.helpers.Loader;
import com.ck.proprios.network.APIClient;
import com.ck.proprios.network.APIInterface;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashBoardRentFragment extends Fragment {

    SwipeRefreshLayout swipeRefreshLayout;
    APIInterface apiInterface;
    Loader loader;
    RecyclerView rvDashBoardUpcomingRent;
    ImageView callUpIcon;
    TextView callUpText;
    DashBoardUpcomingRentAdapter upcomingRentAdapter;
    List<DashBoardRent.UpcomingRent> upcomingRentList = new ArrayList<>();
    List<DashBoardRent.OutstandingRent> outstandingRentList = new ArrayList<>();
    boolean refresh = false;

    public DashBoardRentFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dash_board_rent, container, false);

        apiInterface = APIClient.getAuthClient().create(APIInterface.class);
        loader = new Loader(getContext());
        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        callUpIcon = view.findViewById(R.id.callUpIcon);
        callUpText = view.findViewById(R.id.callUpText);
        rvDashBoardUpcomingRent = view.findViewById(R.id.rvDashBoardUpcomingRent);
        outstandingRentList.clear();
        upcomingRentList.clear();
        upcomingRentAdapter = new DashBoardUpcomingRentAdapter(getContext(), upcomingRentList, outstandingRentList);
        rvDashBoardUpcomingRent.setHasFixedSize(true);
        rvDashBoardUpcomingRent.setLayoutManager(new LinearLayoutManager(getContext()));
        rvDashBoardUpcomingRent.setAdapter(upcomingRentAdapter);

        callAPI();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh = true;
                callAPI();
                refresh = false;
            }
        });
        return view;
    }

    public void myView() {
        if (outstandingRentList.size() > 0) {
            callUpIcon.setVisibility(View.GONE);
            callUpText.setVisibility(View.GONE);
            rvDashBoardUpcomingRent.setVisibility(View.VISIBLE);
        } else {
            callUpIcon.setVisibility(View.VISIBLE);
            callUpText.setVisibility(View.VISIBLE);
            rvDashBoardUpcomingRent.setVisibility(View.GONE);
        }
    }

    private void callAPI() {
        if (!refresh) {
            loader.showLoader();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("request", "dashboard-rent-overview");
        map.put("customer_id", "uR765FohTs_NrL0BMWOGvQ==");
        map.put("customer_type", "private-landlord");
        map.put("customer_api_key", "9e52c2ea3e734651");
        map.put("remote_address", "Not allowed by user");
        map.put("user_agent", "Mobile|iPhone 11 Pro|iOS 13.4.1|Safari XX");

        Log.wtf("dashBoardRent", "Params: " + map);

        Call<DashBoardRent> call = apiInterface.dashboardRent(map);
        call.enqueue(new Callback<DashBoardRent>() {
            @Override
            public void onResponse(Call<DashBoardRent> call, Response<DashBoardRent> response) {
                loader.hideLoader();
                DashBoardRent model = response.body();
                outstandingRentList.clear();
                upcomingRentList.clear();
                outstandingRentList.addAll(model.getOutstandingRent());
                upcomingRentList.addAll(model.getUpcomingRent());
                upcomingRentAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                myView();
                Log.wtf("dashBoardRent", "response: " + new Gson().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<DashBoardRent> call, Throwable t) {
                loader.hideLoader();
                Toast.makeText(getContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.wtf("dashBoardRent", "error: " + t.getMessage());
            }
        });
    }
}