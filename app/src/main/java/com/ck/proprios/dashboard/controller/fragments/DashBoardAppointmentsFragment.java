package com.ck.proprios.dashboard.controller.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ck.proprios.R;
import com.ck.proprios.dashboard.adapters.DashBoardAppointmentsAdapter;
import com.ck.proprios.dashboard.pojo.DashBoardAppointments;
import com.ck.proprios.dashboard.pojo.DashBoardMaintenance;
import com.ck.proprios.helpers.Loader;
import com.ck.proprios.network.APIClient;
import com.ck.proprios.network.APIInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashBoardAppointmentsFragment extends Fragment {

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView rvDashboardAppointments;
    ImageView callUpIcon;
    TextView callUpText;
    APIInterface apiInterface;
    Loader loader;
    boolean refresh = false;
    List<DashBoardAppointments> dashBoardAppointmentsList = new ArrayList<>();
    DashBoardAppointmentsAdapter adapter;

    public DashBoardAppointmentsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dash_board_appointments, container, false);
        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        rvDashboardAppointments = view.findViewById(R.id.rvDashBoardAppointments);
        callUpIcon = view.findViewById(R.id.callUpIcon);
        callUpText = view.findViewById(R.id.callUpText);
        apiInterface = APIClient.getAuthClient().create(APIInterface.class);
        loader = new Loader(getContext());

        dashBoardAppointmentsList.clear();
        adapter = new DashBoardAppointmentsAdapter(getContext(), dashBoardAppointmentsList);
        rvDashboardAppointments.setHasFixedSize(true);
        rvDashboardAppointments.setLayoutManager(new LinearLayoutManager(getContext()));
        rvDashboardAppointments.setAdapter(adapter);
        callAPI();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh = true;
                callAPI();
                refresh = false;
            }
        });
        return view;
    }

    public void myView() {
        if (dashBoardAppointmentsList.size() > 0) {
            callUpIcon.setVisibility(View.GONE);
            callUpText.setVisibility(View.GONE);
            rvDashboardAppointments.setVisibility(View.VISIBLE);
        } else {
            callUpIcon.setVisibility(View.VISIBLE);
            callUpText.setVisibility(View.VISIBLE);
            rvDashboardAppointments.setVisibility(View.GONE);
        }
    }

    private void callAPI() {
        if (refresh) {
        } else {
            loader.showLoader();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("request", "dashboard-appointments");
        map.put("customer_id", "uR765FohTs_NrL0BMWOGvQ==");
        map.put("customer_type", "private-landlord");
        map.put("customer_api_key", "9e52c2ea3e734651");
        map.put("remote_address", "Not allowed by user");
        map.put("user_agent", "Mobile|iPhone 11 Pro|iOS 13.4.1|Safari XX");

        Log.wtf("dashBoardAppointments", "Params: " + map);

        Call<List<DashBoardAppointments>> call = apiInterface.dashboardAppointments(map);
        call.enqueue(new Callback<List<DashBoardAppointments>>() {
            @Override
            public void onResponse(Call<List<DashBoardAppointments>> call, Response<List<DashBoardAppointments>> response) {
                loader.hideLoader();
                List<DashBoardAppointments> model = response.body();
                dashBoardAppointmentsList.clear();
                dashBoardAppointmentsList.addAll(model);
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                myView();
//                Log.wtf("dashBoardRent", "response: " + new Gson().toJson(response.body()));
                Log.wtf("dashBoardAppointments", "response: " + dashBoardAppointmentsList.size());
            }

            @Override
            public void onFailure(Call<List<DashBoardAppointments>> call, Throwable t) {
                loader.hideLoader();
                Toast.makeText(getContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.wtf("dashBoardAppointments", "error: " + t.getMessage());
            }
        });
    }
}