package com.ck.proprios.dashboard.controller.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.NavGraph;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ck.proprios.R;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.dashboard.controller.DashBoardScreen;
import com.ck.proprios.intro.pojo.DashBoardInitialLoad;

import static androidx.navigation.Navigation.findNavController;

public class DashBoardFragment extends Fragment implements View.OnClickListener {

    NavController navController;
    LinearLayout btn_reminders, btn_rent, btn_appointments, btn_maintenance;
    private int fragment_status = -1;
    TextView tv_reminders, tv_rent, tv_appointments, tv_maintenance;
    View line_reminders, line_rent, line_appointments, line_maintenance;
    NavOptions.Builder navBuilder;

    public DashBoardFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dash_board, container, false);

        navController = findNavController(view.findViewById(R.id.fragment_container_dashboard));
        btn_reminders = view.findViewById(R.id.btn_reminders);
        btn_rent = view.findViewById(R.id.btn_rent);
        btn_appointments = view.findViewById(R.id.btn_appointments);
        btn_maintenance = view.findViewById(R.id.btn_maintenance);

        tv_reminders = view.findViewById(R.id.tv_reminders);
        tv_rent = view.findViewById(R.id.tv_rent);
        tv_appointments = view.findViewById(R.id.tv_appointments);
        tv_maintenance = view.findViewById(R.id.tv_maintenance);
        line_reminders = view.findViewById(R.id.line_reminders);
        line_rent = view.findViewById(R.id.line_rent);
        line_appointments = view.findViewById(R.id.line_appointments);
        line_maintenance = view.findViewById(R.id.line_maintenance);

        btn_reminders.setOnClickListener(this);
        btn_rent.setOnClickListener(this);
        btn_appointments.setOnClickListener(this);
        btn_maintenance.setOnClickListener(this);

        navBuilder = new NavOptions.Builder();
        return view;
    }

    public void setHeader() {
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel().toString().equals("fragment_reminders")) {
                    fragment_status = 0;
                    tv_reminders.setTextColor(getContext().getResources().getColor(R.color.color26));
                    tv_rent.setTextColor(getContext().getResources().getColor(R.color.color97));
                    tv_appointments.setTextColor(getContext().getResources().getColor(R.color.color97));
                    tv_maintenance.setTextColor(getContext().getResources().getColor(R.color.color97));
                    line_reminders.setVisibility(View.VISIBLE);
                    line_rent.setVisibility(View.GONE);
                    line_appointments.setVisibility(View.GONE);
                    line_maintenance.setVisibility(View.GONE);
                } else if (destination.getLabel().toString().equals("fragment_dash_board_rent")) {
                    fragment_status = 1;
                    tv_reminders.setTextColor(getContext().getResources().getColor(R.color.color97));
                    tv_rent.setTextColor(getContext().getResources().getColor(R.color.color26));
                    tv_appointments.setTextColor(getContext().getResources().getColor(R.color.color97));
                    tv_maintenance.setTextColor(getContext().getResources().getColor(R.color.color97));
                    line_reminders.setVisibility(View.GONE);
                    line_rent.setVisibility(View.VISIBLE);
                    line_appointments.setVisibility(View.GONE);
                    line_maintenance.setVisibility(View.GONE);
                } else if (destination.getLabel().toString().equals("fragment_dash_board_appointments")) {
                    fragment_status = 2;
                    tv_reminders.setTextColor(getContext().getResources().getColor(R.color.color97));
                    tv_rent.setTextColor(getContext().getResources().getColor(R.color.color97));
                    tv_appointments.setTextColor(getContext().getResources().getColor(R.color.color26));
                    tv_maintenance.setTextColor(getContext().getResources().getColor(R.color.color97));
                    line_reminders.setVisibility(View.GONE);
                    line_rent.setVisibility(View.GONE);
                    line_appointments.setVisibility(View.VISIBLE);
                    line_maintenance.setVisibility(View.GONE);
                } else if (destination.getLabel().toString().equals("fragment_dash_board_maintenance")) {
                    fragment_status = 3;
                    tv_reminders.setTextColor(getContext().getResources().getColor(R.color.color97));
                    tv_rent.setTextColor(getContext().getResources().getColor(R.color.color97));
                    tv_appointments.setTextColor(getContext().getResources().getColor(R.color.color97));
                    tv_maintenance.setTextColor(getContext().getResources().getColor(R.color.color26));
                    line_reminders.setVisibility(View.GONE);
                    line_rent.setVisibility(View.GONE);
                    line_appointments.setVisibility(View.GONE);
                    line_maintenance.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == btn_reminders) {
            if (fragment_status == 0) {
            } else {
                navBuilder.setEnterAnim(R.anim.slide_in_left).setExitAnim(R.anim.slide_out_right).setPopEnterAnim(R.anim.slide_in_left).setPopExitAnim(R.anim.slide_out_right);
                navController.navigate(R.id.action_global_reminders, null, navBuilder.build());
//                navController.navigate(R.id.action_global_reminders);
                setHeader();
            }
        } else if (v == btn_rent) {
            if (fragment_status == 1) {
            } else {
//                navBuilder.setEnterAnim(R.anim.slide_in_left).setExitAnim(R.anim.slide_out_right).setPopEnterAnim(R.anim.slide_in_left).setPopExitAnim(R.anim.slide_out_right);
//                navController.navigate(R.id.action_global_dashBoardRentFragment, null, navBuilder.build());
                navController.navigate(R.id.action_global_dashBoardRentFragment);
                setHeader();
            }
        } else if (v == btn_appointments) {
            if (fragment_status == 2) {
            } else {
                navController.navigate(R.id.action_global_dashBoardAppointmentsFragment);
                setHeader();
            }
        } else if (v == btn_maintenance) {
            if (fragment_status == 3) {
            } else {
                navController.navigate(R.id.action_global_dashBoardMaintenanceFragment);
                setHeader();
            }
        }
    }
}