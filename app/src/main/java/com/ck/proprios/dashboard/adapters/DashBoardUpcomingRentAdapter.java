package com.ck.proprios.dashboard.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ck.proprios.R;
import com.ck.proprios.dashboard.pojo.DashBoardRent;

import java.util.ArrayList;
import java.util.List;

public class DashBoardUpcomingRentAdapter extends RecyclerView.Adapter<DashBoardUpcomingRentAdapter.vHolder> {

    Context mContext;
    List<DashBoardRent.UpcomingRent> listUpcoming = new ArrayList<>();
    List<DashBoardRent.OutstandingRent> listOutgoing = new ArrayList<>();

    public DashBoardUpcomingRentAdapter(Context mContext, List<DashBoardRent.UpcomingRent>
            listUpcoming, List<DashBoardRent.OutstandingRent> listOutgoing) {
        this.mContext = mContext;
        this.listUpcoming = listUpcoming;
        this.listOutgoing = listOutgoing;
    }

    @NonNull
    @Override
    public vHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.dashboard_rent_item, parent, false);
        return new DashBoardUpcomingRentAdapter.vHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull vHolder holder, int position) {
        DashBoardRent.UpcomingRent upcomingRent;
        DashBoardRent.OutstandingRent outstandingRent;


        if (position == 0) {
            holder.tvUpcomingNew.setVisibility(View.VISIBLE);
        } else if (position == listUpcoming.size()) {
            holder.tvUpcomingNew.setVisibility(View.VISIBLE);
            holder.tvUpcomingNew.setText("Outstanding Rent");
        } else {
            holder.tvUpcomingNew.setVisibility(View.GONE);
        }


        if (position == listUpcoming.size() - 1) {
            holder.dashBoardRentSeparator.setVisibility(View.GONE);
        } else if (position == listUpcoming.size() + listOutgoing.size() - 1) {
            holder.dashBoardRentSeparator.setVisibility(View.GONE);
        } else {
            holder.dashBoardRentSeparator.setVisibility(View.VISIBLE);
        }

        if (position < listUpcoming.size()) {
            holder.rlRentName.setVisibility(View.VISIBLE);
            holder.rlRentDateYear.setVisibility(View.GONE);
            holder.tvRentDatePrice.setVisibility(View.VISIBLE);
            holder.tvRentStatusPrice.setVisibility(View.GONE);
            upcomingRent = listUpcoming.get(position);
            holder.tvRentHeading.setText(upcomingRent.getCustomerName());
            holder.tvRentSubHeading.setText(upcomingRent.getPropertyDetails());
            holder.tvRentDatePrice.setText(upcomingRent.getRentDate() + " - £" + upcomingRent.getPropertyRent());
        } else {
            holder.rlRentName.setVisibility(View.GONE);
            holder.rlRentDateYear.setVisibility(View.VISIBLE);
            holder.tvRentDatePrice.setVisibility(View.GONE);
            holder.tvRentStatusPrice.setVisibility(View.VISIBLE);
            outstandingRent = listOutgoing.get(position - listUpcoming.size());
            holder.tvRentHeading.setText(outstandingRent.getCustomerName());
            holder.tvRentSubHeading.setText(outstandingRent.getPropertyDetails());
            holder.tvRentDate.setText(outstandingRent.getRentDate());
            holder.tvRentYear.setText(outstandingRent.getRentYear());
            if (outstandingRent.getOrderStatus().matches("Partial")) {
                holder.tvRentStatusPrice.setText("£" + outstandingRent.getOrderTotal() + " - " + "OWED");
                holder.tvRentStatusPrice.setBackground(mContext.getResources().getDrawable(R.drawable.owed_bg));
                holder.tvRentStatusPrice.setTextColor(mContext.getResources().getColor(R.color.colorF3));
            } else {
                holder.tvRentStatusPrice.setText("£" + outstandingRent.getOrderTotal() + " - " + "UNPAID");
                holder.tvRentStatusPrice.setBackground(mContext.getResources().getDrawable(R.drawable.unpaid_bg));
                holder.tvRentStatusPrice.setTextColor(mContext.getResources().getColor(R.color.colorFA));
            }
        }
    }

    @Override
    public int getItemCount() {
        return listUpcoming.size() + listOutgoing.size();
    }

    public class vHolder extends RecyclerView.ViewHolder {

        TextView tvRentHeading, tvRentSubHeading, tvRentDate, tvRentName, tvUpcomingNew,
                tvRentDatePrice, tvRentYear, tvRentStatusPrice;
        View dashBoardRentSeparator;
        RelativeLayout rlRentName, rlRentDateYear;

        public vHolder(@NonNull View itemView) {
            super(itemView);
            rlRentName = itemView.findViewById(R.id.rlRentName);
            rlRentDateYear = itemView.findViewById(R.id.rlRentDateYear);
            tvRentStatusPrice = itemView.findViewById(R.id.tvRentStatusPrice);
            tvUpcomingNew = itemView.findViewById(R.id.tvUpcomingNew);
            tvRentDatePrice = itemView.findViewById(R.id.tvRentDatePrice);
            tvRentYear = itemView.findViewById(R.id.tvRentYear);
            dashBoardRentSeparator = itemView.findViewById(R.id.dashBoardRentSeparator);
            tvRentHeading = itemView.findViewById(R.id.tvRentHeading);
            tvRentSubHeading = itemView.findViewById(R.id.tvRentSubHeading);
            tvRentDate = itemView.findViewById(R.id.tvRentDate);
            tvRentName = itemView.findViewById(R.id.tvRentName);
        }
    }
}
