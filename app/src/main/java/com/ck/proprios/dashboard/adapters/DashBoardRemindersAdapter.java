package com.ck.proprios.dashboard.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ck.proprios.R;
import com.ck.proprios.intro.pojo.DashBoardInitialLoad;

import java.util.ArrayList;
import java.util.List;

public class DashBoardRemindersAdapter extends RecyclerView.Adapter<DashBoardRemindersAdapter.vHolder> {
    Context mContext;
    List<DashBoardInitialLoad.DashboardReminders> list = new ArrayList<>();

    public DashBoardRemindersAdapter(Context mContext, List<DashBoardInitialLoad.DashboardReminders> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @NonNull
    @Override
    public vHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.dashboard_reminder_item, parent, false);
        return new DashBoardRemindersAdapter.vHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull vHolder holder, int position) {
        DashBoardInitialLoad.DashboardReminders reminder = list.get(position);
        holder.btnMore.setVisibility(View.GONE);
        holder.tvReminderDate.setText(reminder.getReminderDateShort());
        holder.tvReminderTime.setText(reminder.getReminderTime());
        holder.tvReminderHeading.setText(reminder.getReminderName());
        holder.tvReminderSubHeading.setText(reminder.getPropertyDetails());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class vHolder extends RecyclerView.ViewHolder {

        TextView tvReminderDate, tvReminderTime, tvReminderHeading, tvReminderSubHeading;
        View viewDate;
        ImageView btnMore;

        public vHolder(@NonNull View itemView) {
            super(itemView);
            tvReminderDate = itemView.findViewById(R.id.tvReminderDate);
            tvReminderTime = itemView.findViewById(R.id.tvReminderTime);
            tvReminderHeading = itemView.findViewById(R.id.tvReminderHeading);
            tvReminderSubHeading = itemView.findViewById(R.id.tvReminderSubHeading);
            viewDate = itemView.findViewById(R.id.viewDate);
            btnMore = itemView.findViewById(R.id.btnMore);
        }
    }
}
