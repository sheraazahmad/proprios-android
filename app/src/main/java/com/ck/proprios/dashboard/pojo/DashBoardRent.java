package com.ck.proprios.dashboard.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DashBoardRent {
    @SerializedName("upcoming_rent")
    @Expose
    private List<UpcomingRent> upcomingRent = null;
    @SerializedName("outstanding_rent")
    @Expose
    private List<OutstandingRent> outstandingRent = null;

    public List<UpcomingRent> getUpcomingRent() {
        return upcomingRent;
    }

    public void setUpcomingRent(List<UpcomingRent> upcomingRent) {
        this.upcomingRent = upcomingRent;
    }

    public List<OutstandingRent> getOutstandingRent() {
        return outstandingRent;
    }

    public void setOutstandingRent(List<OutstandingRent> outstandingRent) {
        this.outstandingRent = outstandingRent;
    }

    public class OutstandingRent {

        @SerializedName("customer_name")
        @Expose
        private String customerName;
        @SerializedName("contract_id")
        @Expose
        private String contractId;
        @SerializedName("tenant_id")
        @Expose
        private String tenantId;
        @SerializedName("property_rent")
        @Expose
        private String propertyRent;
        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("order_total")
        @Expose
        private String orderTotal;
        @SerializedName("order_status")
        @Expose
        private String orderStatus;
        @SerializedName("rent_date")
        @Expose
        private String rentDate;
        @SerializedName("rent_year")
        @Expose
        private String rentYear;
        @SerializedName("property_details")
        @Expose
        private String propertyDetails;

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getContractId() {
            return contractId;
        }

        public void setContractId(String contractId) {
            this.contractId = contractId;
        }

        public String getTenantId() {
            return tenantId;
        }

        public void setTenantId(String tenantId) {
            this.tenantId = tenantId;
        }

        public String getPropertyRent() {
            return propertyRent;
        }

        public void setPropertyRent(String propertyRent) {
            this.propertyRent = propertyRent;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getOrderTotal() {
            return orderTotal;
        }

        public void setOrderTotal(String orderTotal) {
            this.orderTotal = orderTotal;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getRentDate() {
            return rentDate;
        }

        public void setRentDate(String rentDate) {
            this.rentDate = rentDate;
        }

        public String getRentYear() {
            return rentYear;
        }

        public void setRentYear(String rentYear) {
            this.rentYear = rentYear;
        }

        public String getPropertyDetails() {
            return propertyDetails;
        }

        public void setPropertyDetails(String propertyDetails) {
            this.propertyDetails = propertyDetails;
        }

    }

    public class UpcomingRent {

        @SerializedName("customer_name")
        @Expose
        private String customerName;
        @SerializedName("property_rent")
        @Expose
        private String propertyRent;
        @SerializedName("contract_id")
        @Expose
        private String contractId;
        @SerializedName("property_details")
        @Expose
        private String propertyDetails;
        @SerializedName("rent_date")
        @Expose
        private String rentDate;
        @SerializedName("rent_date_to_add")
        @Expose
        private String rentDateToAdd;

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getPropertyRent() {
            return propertyRent;
        }

        public void setPropertyRent(String propertyRent) {
            this.propertyRent = propertyRent;
        }

        public String getContractId() {
            return contractId;
        }

        public void setContractId(String contractId) {
            this.contractId = contractId;
        }

        public String getPropertyDetails() {
            return propertyDetails;
        }

        public void setPropertyDetails(String propertyDetails) {
            this.propertyDetails = propertyDetails;
        }

        public String getRentDate() {
            return rentDate;
        }

        public void setRentDate(String rentDate) {
            this.rentDate = rentDate;
        }

        public String getRentDateToAdd() {
            return rentDateToAdd;
        }

        public void setRentDateToAdd(String rentDateToAdd) {
            this.rentDateToAdd = rentDateToAdd;
        }

    }
}
