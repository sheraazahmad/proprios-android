package com.ck.proprios.dashboard.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashBoardAppointments {
    @SerializedName("appointment_id")
    @Expose
    private String appointmentId;
    @SerializedName("contract_id")
    @Expose
    private String contractId;
    @SerializedName("appointment_date")
    @Expose
    private String appointmentDate;
    @SerializedName("appointment_with")
    @Expose
    private String appointmentWith;
    @SerializedName("appointment_confirmed")
    @Expose
    private String appointmentConfirmed;
    @SerializedName("property_details")
    @Expose
    private String propertyDetails;
    @SerializedName("appointment_date_new")
    @Expose
    private String appointmentDateNew;
    @SerializedName("appointment_date_new_short")
    @Expose
    private String appointmentDateNewShort;
    @SerializedName("appointment_time")
    @Expose
    private String appointmentTime;
    @SerializedName("appointment_confirmed_text")
    @Expose
    private String appointmentConfirmedText;

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getAppointmentWith() {
        return appointmentWith;
    }

    public void setAppointmentWith(String appointmentWith) {
        this.appointmentWith = appointmentWith;
    }

    public String getAppointmentConfirmed() {
        return appointmentConfirmed;
    }

    public void setAppointmentConfirmed(String appointmentConfirmed) {
        this.appointmentConfirmed = appointmentConfirmed;
    }

    public String getPropertyDetails() {
        return propertyDetails;
    }

    public void setPropertyDetails(String propertyDetails) {
        this.propertyDetails = propertyDetails;
    }

    public String getAppointmentDateNew() {
        return appointmentDateNew;
    }

    public void setAppointmentDateNew(String appointmentDateNew) {
        this.appointmentDateNew = appointmentDateNew;
    }

    public String getAppointmentDateNewShort() {
        return appointmentDateNewShort;
    }

    public void setAppointmentDateNewShort(String appointmentDateNewShort) {
        this.appointmentDateNewShort = appointmentDateNewShort;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getAppointmentConfirmedText() {
        return appointmentConfirmedText;
    }

    public void setAppointmentConfirmedText(String appointmentConfirmedText) {
        this.appointmentConfirmedText = appointmentConfirmedText;
    }
}
