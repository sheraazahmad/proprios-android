package com.ck.proprios.dashboard.controller.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ck.proprios.R;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.dashboard.adapters.DashBoardRemindersAdapter;
import com.ck.proprios.helpers.Loader;
import com.ck.proprios.helpers.SharedPrefHelper;
import com.ck.proprios.intro.controller.FingerPrintScreen;
import com.ck.proprios.intro.controller.PassCodeScreen;
import com.ck.proprios.intro.pojo.DashBoardInitialLoad;
import com.ck.proprios.network.APIClient;
import com.ck.proprios.network.APIInterface;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashBoardRemindersFragments extends Fragment {

    DashBoardInitialLoad dashBoardInitialLoad;
    RecyclerView rvDashBoardReminders;
    DashBoardRemindersAdapter adapter;
    List<DashBoardInitialLoad.DashboardReminders> list = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;
    APIInterface apiInterface;
    Loader loader;
    CardView btnAdd;
    ImageView callUpIcon;
    TextView callUpText;


    public DashBoardRemindersFragments() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reminders, container, false);
        dashBoardInitialLoad = ((GlobalCLass) getContext().getApplicationContext()).getDashBoardInitialLoad();
//        Log.wtf("model", "" + dashBoardInitialLoad.getDashboardRemindersList().size());
        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        btnAdd = view.findViewById(R.id.btnAdd);
        apiInterface = APIClient.getAuthClient().create(APIInterface.class);
        loader = new Loader(getContext());
        callUpIcon = view.findViewById(R.id.callUpIcon);
        callUpText = view.findViewById(R.id.callUpText);
        list.clear();
//        list.addAll(dashBoardInitialLoad.getDashboardRemindersList());
        adapter = new DashBoardRemindersAdapter(getContext(), list);
        rvDashBoardReminders = view.findViewById(R.id.rvDashBoardReminders);
        rvDashBoardReminders.setHasFixedSize(true);
        rvDashBoardReminders.setLayoutManager(new LinearLayoutManager(getContext()));
        rvDashBoardReminders.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callAPI();
            }
        });
        return view;
    }

    public void myView() {
        if (list.size() > 0) {
            callUpIcon.setVisibility(View.GONE);
            callUpText.setVisibility(View.GONE);
            rvDashBoardReminders.setVisibility(View.VISIBLE);
        } else {
            callUpIcon.setVisibility(View.VISIBLE);
            callUpText.setVisibility(View.VISIBLE);
            rvDashBoardReminders.setVisibility(View.GONE);
        }
    }

    public void callAPI() {
        HashMap<String, String> map = new HashMap<>();
        map.put("request", "dashboard-initial-load");
        map.put("customer_id", "uR765FohTs_NrL0BMWOGvQ==");
        map.put("customer_type", "private-landlord");
        map.put("customer_api_key", "9e52c2ea3e734651");
        map.put("remote_address", "Not allowed by user");
        map.put("user_agent", "Mobile|iPhone 11 Pro|iOS 13.4.1|Safari XX");

        Log.wtf("dashBoardInitial", "Params: " + map);

        Call<DashBoardInitialLoad> call = apiInterface.dashBoardInitialLoad(map);
        call.enqueue(new Callback<DashBoardInitialLoad>() {
            @Override
            public void onResponse(Call<DashBoardInitialLoad> call, Response<DashBoardInitialLoad> response) {
                loader.hideLoader();
                DashBoardInitialLoad model = response.body();
                ((GlobalCLass) getContext().getApplicationContext()).setDashBoardInitialLoad(model);
                dashBoardInitialLoad = ((GlobalCLass) getContext().getApplicationContext()).getDashBoardInitialLoad();
                list.clear();
//                list.addAll(dashBoardInitialLoad.getDashboardRemindersList());
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                myView();
                Log.wtf("dashBoardInitial", "response: " + new Gson().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<DashBoardInitialLoad> call, Throwable t) {
                loader.hideLoader();
                Toast.makeText(getContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.wtf("dashBoardInitial", "error: " + t.getMessage());
            }
        });
    }
}