package com.ck.proprios.dashboard.controller;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.bumptech.glide.Glide;
import com.ck.proprios.R;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.SideMenu.LandLord.Adapter.SideMenuAdapter;
import com.ck.proprios.helpers.CustomCalendar;
import com.ck.proprios.helpers.SharedPrefHelper;
import com.ck.proprios.intro.controller.LoginScreen;
import com.google.android.material.navigation.NavigationView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DashBoardScreen extends AppCompatActivity implements View.OnClickListener {

    Button btnLogOut;
    public DrawerLayout nav_drawer;
    NavigationView navigation_view;
    NavController navController;
    RecyclerView rvSideMenu;
    SideMenuAdapter adapter;
    CardView headerCardView;
    public RelativeLayout btnMenu1, btnMenu2, navBarHeader, rlCustomerInitials, firstIconLayout,
            secondIconLayout, thirdIconLayout, headerTextBtnLayout;
    public LinearLayout sideMenuSettings, sideMenuSignOut, btnDashboard, btnMyPortfolio, btnTenants,
            btnMessages, btnProfile, footerNavBar;
    public TextView tvHeading, tvSideMenuSettings, tvSideMenuSignOut, tvUserName, tvUserType,
            tvCustomerInitials, headerTextBtnText;
    public ImageView icoSideMenuSettings, icoSideMenuSignOut, iconDashboard, iconMyPortfolio,
            iconTenants, iconMessages, iconProfile, firstIcon, secondIcon, thirdIcon, btnMenu1Icon;
    CircleImageView prof_img;
    List<Integer> iconsList = new ArrayList<>();
    List<String> textList = new ArrayList<>();
    public int fragments_status = -1;
    View notification_dot;
    CustomCalendar customCalendar;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_drawer);

        if (((GlobalCLass) getApplicationContext()).isSlider()) {
            ((GlobalCLass) getApplicationContext()).setSlider(false);
            Animatoo.animateSlideRight(DashBoardScreen.this);
        } else {
            Animatoo.animateSlideLeft(DashBoardScreen.this);
        }

        sideMenuList();

        headerCardView = findViewById(R.id.headerCardView);
        nav_drawer = findViewById(R.id.nav_drawer);
        navigation_view = findViewById(R.id.navigation_view);
        footerNavBar = findViewById(R.id.footerNavBar);
        navController = Navigation.findNavController(DashBoardScreen.this, R.id.fragment_container);
        customCalendar = new CustomCalendar();
        btnDashboard = findViewById(R.id.btnDashboard);
        btnMyPortfolio = findViewById(R.id.btnMyPortfolio);
        btnTenants = findViewById(R.id.btnTenants);
        btnMessages = findViewById(R.id.btnMessages);
        btnProfile = findViewById(R.id.btnProfile);

        iconDashboard = findViewById(R.id.iconDashboard);
        iconMyPortfolio = findViewById(R.id.iconMyPortfolio);
        iconTenants = findViewById(R.id.iconTenants);
        iconMessages = findViewById(R.id.iconMessage);
        iconProfile = findViewById(R.id.iconProfile);

        firstIconLayout = findViewById(R.id.firstIconLayout);
        secondIconLayout = findViewById(R.id.secondIconLayout);
        thirdIconLayout = findViewById(R.id.thirdIconLayout);
        headerTextBtnLayout = findViewById(R.id.headerTextBtnLayout);
        headerTextBtnText = findViewById(R.id.headerTextBtnText);

        firstIcon = findViewById(R.id.firstIcon);
        secondIcon = findViewById(R.id.secondIcon);
        thirdIcon = findViewById(R.id.thirdIcon);
        notification_dot = findViewById(R.id.notification_dot);

        btnLogOut = findViewById(R.id.btnLogOut);
        btnMenu1 = findViewById(R.id.btnMenu1);
        btnMenu2 = findViewById(R.id.btnMenu2);
        btnMenu1Icon = findViewById(R.id.btnMenu1Icon);
        tvHeading = findViewById(R.id.tvHeading);
        prof_img = findViewById(R.id.prof_img);
        rlCustomerInitials = findViewById(R.id.rlCustomerInitials);

        navBarHeader = navigation_view.findViewById(R.id.navBarHeader);
        tvUserName = navigation_view.findViewById(R.id.tvUserName);
        tvUserType = navigation_view.findViewById(R.id.tvUserType);
        tvCustomerInitials = navigation_view.findViewById(R.id.tvCustomerInitials);
        rvSideMenu = navigation_view.findViewById(R.id.rvSideMenu);
        sideMenuSettings = navigation_view.findViewById(R.id.sideMenuSettings);
        sideMenuSignOut = navigation_view.findViewById(R.id.sideMenuSignOut);
        tvSideMenuSettings = navigation_view.findViewById(R.id.tvSideMenuSettings);
        tvSideMenuSignOut = navigation_view.findViewById(R.id.tvSideMenuSignOut);
        icoSideMenuSettings = navigation_view.findViewById(R.id.icoSideMenuSettings);
        icoSideMenuSignOut = navigation_view.findViewById(R.id.icoSideMenuSignOut);

        getStatusBarHeight();
        Log.i("StatusBar Height", "StatusBar Height= " + getStatusBarHeight());

        adapter = new SideMenuAdapter(DashBoardScreen.this, iconsList, textList, navController);
        rvSideMenu.setHasFixedSize(true);
        rvSideMenu.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvSideMenu.setAdapter(adapter);
        btnLogOut.setOnClickListener(this);
        btnMenu1.setOnClickListener(this);
        sideMenuSettings.setOnClickListener(this);
        sideMenuSignOut.setOnClickListener(this);
        btnDashboard.setOnClickListener(this);
//        firstIconLayout.setOnClickListener(this);

        tvUserName.setText(((GlobalCLass) getApplicationContext()).getDashBoardInitialLoad()
                .getViewProfile().getCustomerName());
        tvUserType.setText(SharedPrefHelper.getInstance(getApplicationContext()).getCustomerType());
        tvCustomerInitials.setText(((GlobalCLass) getApplicationContext()).
                getDashBoardInitialLoad().getViewProfile().getCustomerInitials());

        URL url = null;
        try {
            url = new URL(((GlobalCLass) getApplicationContext()).
                    getDashBoardInitialLoad().getViewProfile().getCustomerProfileImage());
            Glide.with(DashBoardScreen.this)
                    .load(url)
                    .into(prof_img);
            rlCustomerInitials.setVisibility(View.GONE);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        setHeader();

    }

    private void setHeader() {
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull
                    NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel().toString().equals("fragment_dash_board")) {
                    tvHeading.setText("Welcome, " + ((GlobalCLass) getApplicationContext()).
                            getDashBoardInitialLoad().getViewProfile().getCustomerFirstName());
                    headerButtonsInvisible();
                    footerNavBar.setVisibility(View.VISIBLE);
                    btnMenu1.setVisibility(View.VISIBLE);
                    btnMenu1Icon.setImageResource(R.drawable.menu);
                    firstIconLayout.setVisibility(View.VISIBLE);
                    firstIcon.setImageResource(R.drawable.bell);
                    notification_dot.setVisibility(View.VISIBLE);
                    headerCardView.setCardElevation(0);
                } else if (destination.getLabel().toString().equals("fragment_reminder")) {
                    headerCardView.setCardElevation(10);
                    tvHeading.setText("Reminder");
                    headerButtonsInvisible();
                    footerNavBar.setVisibility(View.VISIBLE);
                    btnMenu1.setVisibility(View.VISIBLE);
                    btnMenu1Icon.setImageResource(R.drawable.menu);
                    firstIconLayout.setVisibility(View.VISIBLE);
                    firstIcon.setImageResource(R.drawable.plus);
                    secondIconLayout.setVisibility(View.VISIBLE);
                    secondIcon.setImageResource(R.drawable.search);

                } else if (destination.getLabel().toString().equals("fragment_side_menu_add_reminder")) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            headerCardView.setCardElevation(10);
                            tvHeading.setText("Add Reminder");
                            headerButtonsInvisible();
                            btnMenu1.setVisibility(View.VISIBLE);
                            btnMenu1Icon.setImageResource(R.drawable.back_arrow_new);
                            btnMenu2.setVisibility(View.VISIBLE);
                            headerTextBtnLayout.setVisibility(View.VISIBLE);
                        }
                    }, 100);
                } else if (destination.getLabel().toString().equals("fragment_side_menu_edit_reminder")) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            headerCardView.setCardElevation(10);
                            tvHeading.setText("Edit Reminder");
                            headerButtonsInvisible();
                            btnMenu1.setVisibility(View.VISIBLE);
                            btnMenu1Icon.setImageResource(R.drawable.back_arrow_new);
                            btnMenu2.setVisibility(View.VISIBLE);
                            headerTextBtnLayout.setVisibility(View.VISIBLE);
                        }
                    }, 100);
                }
            }
        });
    }

    public void headerButtonsInvisible() {
        footerNavBar.setVisibility(View.GONE);
        firstIconLayout.setVisibility(View.GONE);
        secondIconLayout.setVisibility(View.GONE);
        thirdIconLayout.setVisibility(View.GONE);
        btnMenu1.setVisibility(View.GONE);
        btnMenu2.setVisibility(View.GONE);
        headerTextBtnLayout.setVisibility(View.GONE);
        notification_dot.setVisibility(View.GONE);
    }

    private void sideMenuList() {
        iconsList.clear();
        iconsList.add(R.drawable.property_find);
        iconsList.add(R.drawable.my_portfolio);
        iconsList.add(R.drawable.tenants);
        iconsList.add(R.drawable.sm_maintainence);
        iconsList.add(R.drawable.bell);
        iconsList.add(R.drawable.sm_line_chart);

        textList.clear();
        textList.add("Search");
        textList.add("My Portfolio");
        textList.add("Manage Tenants");
        textList.add("My Tradesmen");
        textList.add("Reminders");
        textList.add("Financial Statement");
    }

    @Override
    public void onClick(View v) {
        if (v == btnLogOut) {
            SharedPrefHelper.getInstance(DashBoardScreen.this).logOut();
            ((GlobalCLass) getApplicationContext()).setSlider(true);
            Intent intent = new Intent(DashBoardScreen.this, LoginScreen.class);
            startActivity(intent);
            finishAffinity();
        } else if (v == btnMenu1) {
            nav_drawer.openDrawer(GravityCompat.START);
        } else if (v == sideMenuSettings) {
            adapter.index = -1;
            adapter.notifyDataSetChanged();
            icoSideMenuSettings.setImageResource(R.drawable.settings_fill);
            tvSideMenuSettings.setTextColor(getResources().getColor(R.color.color5A));
            icoSideMenuSignOut.setImageResource(R.drawable.signout);
            tvSideMenuSignOut.setTextColor(getResources().getColor(R.color.color26));

        } else if (v == sideMenuSignOut) {
            adapter.index = -1;
            adapter.notifyDataSetChanged();
            icoSideMenuSettings.setImageResource(R.drawable.settings);
            tvSideMenuSettings.setTextColor(getResources().getColor(R.color.color26));
            icoSideMenuSignOut.setImageResource(R.drawable.signout_fill);
            tvSideMenuSignOut.setTextColor(getResources().getColor(R.color.color5A));
            SharedPrefHelper.getInstance(DashBoardScreen.this).logOut();
            ((GlobalCLass) getApplicationContext()).setSlider(true);
            Intent intent = new Intent(DashBoardScreen.this, LoginScreen.class);
            startActivity(intent);
            finishAffinity();
        } else if (v == btnDashboard) {
            fragments_status = -1;
            adapter.index = -1;
            adapter.notifyDataSetChanged();
            iconDashboard.setImageResource(R.drawable.dashboard_fill);
            navController.navigate(R.id.action_global_dashBoardFragment);
        }
//        else if (v == firstIconLayout) {
//            Toast.makeText(this, "SHeeraz", Toast.LENGTH_SHORT).show();
//        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
        linearParams.setMargins(0, result, 0, 0);
        navBarHeader.setLayoutParams(linearParams);
        navBarHeader.requestLayout();
        return result;
    }

    public void changeDateStatusDash(int pos) {
        customCalendar.changeSlotStatus(pos);
    }

    public void changeColor(boolean status) {
        if (status) {
            headerTextBtnText.setTextColor(getResources().getColor(R.color.color5A));
        } else {
            headerTextBtnText.setTextColor(getResources().getColor(R.color.color99));
        }
    }
}