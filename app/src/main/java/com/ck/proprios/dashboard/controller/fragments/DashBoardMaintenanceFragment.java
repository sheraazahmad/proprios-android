package com.ck.proprios.dashboard.controller.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ck.proprios.R;
import com.ck.proprios.dashboard.adapters.DashBoardMaintenanceAdapter;
import com.ck.proprios.dashboard.pojo.DashBoardMaintenance;
import com.ck.proprios.dashboard.pojo.DashBoardRent;
import com.ck.proprios.helpers.Loader;
import com.ck.proprios.network.APIClient;
import com.ck.proprios.network.APIInterface;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashBoardMaintenanceFragment extends Fragment {

    SwipeRefreshLayout swipeRefreshLayout;
    ImageView callUpIcon;
    TextView callUpText;
    APIInterface apiInterface;
    Loader loader;
    RecyclerView rvDashBoardMaintenance;
    List<DashBoardMaintenance> dashBoardMaintenanceList = new ArrayList<>();
    DashBoardMaintenanceAdapter adapter;
    boolean refresh = false;

    public DashBoardMaintenanceFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dash_board_maintenance, container, false);

        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        callUpIcon = view.findViewById(R.id.callUpIcon);
        callUpText = view.findViewById(R.id.callUpText);
        apiInterface = APIClient.getAuthClient().create(APIInterface.class);
        loader = new Loader(getContext());
        adapter = new DashBoardMaintenanceAdapter(getContext(), dashBoardMaintenanceList);
        rvDashBoardMaintenance = view.findViewById(R.id.rvDashBoardMaintenance);
        rvDashBoardMaintenance.setHasFixedSize(true);
        rvDashBoardMaintenance.setLayoutManager(new LinearLayoutManager(getContext()));
        rvDashBoardMaintenance.setAdapter(adapter);
        callAPI();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh = true;
                callAPI();
                refresh = false;
            }
        });
        return view;
    }

    public void myView() {
        if (dashBoardMaintenanceList.size() > 0) {
            callUpIcon.setVisibility(View.GONE);
            callUpText.setVisibility(View.GONE);
            rvDashBoardMaintenance.setVisibility(View.VISIBLE);
        } else {
            callUpIcon.setVisibility(View.VISIBLE);
            callUpText.setVisibility(View.VISIBLE);
            rvDashBoardMaintenance.setVisibility(View.GONE);
        }
    }

    private void callAPI() {
        if (!refresh) {
            loader.showLoader();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("request", "dashboard-maintenance-issues");
        map.put("customer_id", "uR765FohTs_NrL0BMWOGvQ==");
        map.put("customer_type", "private-landlord");
        map.put("customer_api_key", "9e52c2ea3e734651");
        map.put("remote_address", "Not allowed by user");
        map.put("user_agent", "Mobile|iPhone 11 Pro|iOS 13.4.1|Safari XX");

        Log.wtf("dashBoardMaintenance", "Params: " + map);

        Call<List<DashBoardMaintenance>> call = apiInterface.dashboardMaintenance(map);
        call.enqueue(new Callback<List<DashBoardMaintenance>>() {
            @Override
            public void onResponse(Call<List<DashBoardMaintenance>> call, Response<List<DashBoardMaintenance>> response) {
                loader.hideLoader();
                List<DashBoardMaintenance> model = response.body();
                dashBoardMaintenanceList.clear();
                dashBoardMaintenanceList.addAll(model);
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                myView();
//                Log.wtf("dashBoardRent", "response: " + new Gson().toJson(response.body()));
                Log.wtf("dashBoardMaintenance", "response: " + dashBoardMaintenanceList.size());
            }

            @Override
            public void onFailure(Call<List<DashBoardMaintenance>> call, Throwable t) {
                loader.hideLoader();
                Toast.makeText(getContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.wtf("dashBoardMaintenance", "error: " + t.getMessage());
            }
        });
    }
}