package com.ck.proprios.dashboard.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashBoardMaintenance {
    @SerializedName("issue_id")
    @Expose
    private String issueId;
    @SerializedName("contract_id")
    @Expose
    private String contractId;
    @SerializedName("property_id")
    @Expose
    private String propertyId;
    @SerializedName("trader_id")
    @Expose
    private String traderId;
    @SerializedName("job_issue_id")
    @Expose
    private String jobIssueId;
    @SerializedName("issue_name")
    @Expose
    private String issueName;
    @SerializedName("issue_price")
    @Expose
    private Integer issuePrice;
    @SerializedName("issue_price_agreed")
    @Expose
    private String issuePriceAgreed;
    @SerializedName("issue_date_booked")
    @Expose
    private String issueDateBooked;
    @SerializedName("issue_date_booked_accepted")
    @Expose
    private String issueDateBookedAccepted;
    @SerializedName("issue_status")
    @Expose
    private Integer issueStatus;
    @SerializedName("property_details")
    @Expose
    private String propertyDetails;
    @SerializedName("issue_status_text")
    @Expose
    private String issueStatusText;
    @SerializedName("issue_status_colour")
    @Expose
    private Integer issueStatusColour;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("issue_date")
    @Expose
    private String issueDate;
    @SerializedName("issue_date_short")
    @Expose
    private String issueDateShort;
    @SerializedName("issue_time")
    @Expose
    private String issueTime;
    @SerializedName("date_viewed")
    @Expose
    private String dateViewed;
    @SerializedName("issue_invoice")
    @Expose
    private String issueInvoice;
    @SerializedName("invoice_extension")
    @Expose
    private String invoiceExtension;

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getTraderId() {
        return traderId;
    }

    public void setTraderId(String traderId) {
        this.traderId = traderId;
    }

    public String getJobIssueId() {
        return jobIssueId;
    }

    public void setJobIssueId(String jobIssueId) {
        this.jobIssueId = jobIssueId;
    }

    public String getIssueName() {
        return issueName;
    }

    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }

    public Integer getIssuePrice() {
        return issuePrice;
    }

    public void setIssuePrice(Integer issuePrice) {
        this.issuePrice = issuePrice;
    }

    public String getIssuePriceAgreed() {
        return issuePriceAgreed;
    }

    public void setIssuePriceAgreed(String issuePriceAgreed) {
        this.issuePriceAgreed = issuePriceAgreed;
    }

    public String getIssueDateBooked() {
        return issueDateBooked;
    }

    public void setIssueDateBooked(String issueDateBooked) {
        this.issueDateBooked = issueDateBooked;
    }

    public String getIssueDateBookedAccepted() {
        return issueDateBookedAccepted;
    }

    public void setIssueDateBookedAccepted(String issueDateBookedAccepted) {
        this.issueDateBookedAccepted = issueDateBookedAccepted;
    }

    public Integer getIssueStatus() {
        return issueStatus;
    }

    public void setIssueStatus(Integer issueStatus) {
        this.issueStatus = issueStatus;
    }

    public String getPropertyDetails() {
        return propertyDetails;
    }

    public void setPropertyDetails(String propertyDetails) {
        this.propertyDetails = propertyDetails;
    }

    public String getIssueStatusText() {
        return issueStatusText;
    }

    public void setIssueStatusText(String issueStatusText) {
        this.issueStatusText = issueStatusText;
    }

    public Integer getIssueStatusColour() {
        return issueStatusColour;
    }

    public void setIssueStatusColour(Integer issueStatusColour) {
        this.issueStatusColour = issueStatusColour;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueDateShort() {
        return issueDateShort;
    }

    public void setIssueDateShort(String issueDateShort) {
        this.issueDateShort = issueDateShort;
    }

    public String getIssueTime() {
        return issueTime;
    }

    public void setIssueTime(String issueTime) {
        this.issueTime = issueTime;
    }

    public String getDateViewed() {
        return dateViewed;
    }

    public void setDateViewed(String dateViewed) {
        this.dateViewed = dateViewed;
    }

    public String getIssueInvoice() {
        return issueInvoice;
    }

    public void setIssueInvoice(String issueInvoice) {
        this.issueInvoice = issueInvoice;
    }

    public String getInvoiceExtension() {
        return invoiceExtension;
    }

    public void setInvoiceExtension(String invoiceExtension) {
        this.invoiceExtension = invoiceExtension;
    }
}
