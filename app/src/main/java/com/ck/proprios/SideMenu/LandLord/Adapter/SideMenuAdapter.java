package com.ck.proprios.SideMenu.LandLord.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;

import com.ck.proprios.R;
import com.ck.proprios.dashboard.controller.DashBoardScreen;

import java.util.ArrayList;
import java.util.List;

public class SideMenuAdapter extends RecyclerView.Adapter<SideMenuAdapter.vHolder> {

    Context mContext;
    List<Integer> icon = new ArrayList<>();
    List<String> text = new ArrayList<>();
    public int index = -1;
    NavController navController;

    public SideMenuAdapter(Context mContext, List<Integer> icon, List<String> text,
                           NavController navController) {
        this.mContext = mContext;
        this.icon = icon;
        this.text = text;
        this.navController = navController;
    }

    @NonNull
    @Override
    public vHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.side_menu_item, parent, false);
        return new SideMenuAdapter.vHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final vHolder holder, final int position) {
        holder.sideMenuIcon.setImageResource(icon.get(position));
        holder.sideMenuText.setText(text.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index = position;
                notifyDataSetChanged();
                ((DashBoardScreen) mContext).iconDashboard.setImageResource(R.drawable.dashboard);
                ((DashBoardScreen) mContext).icoSideMenuSettings.setImageResource(R.drawable.settings);
                ((DashBoardScreen) mContext).tvSideMenuSettings.setTextColor(mContext.getResources().getColor(R.color.color26));
                ((DashBoardScreen) mContext).icoSideMenuSignOut.setImageResource(R.drawable.signout);
                ((DashBoardScreen) mContext).tvSideMenuSignOut.setTextColor(mContext.getResources().getColor(R.color.color26));
            }
        });
        if (index == position) {
            changeColor(position, holder);
        } else {
            for (int i = 0; i < icon.size(); i++) {
                holder.sideMenuIcon.setImageResource(icon.get(position));
                holder.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color26));
            }
        }
    }

    @Override
    public int getItemCount() {
        return text.size();
    }

    public class vHolder extends RecyclerView.ViewHolder {

        ImageView sideMenuIcon;
        TextView sideMenuText;

        public vHolder(@NonNull View itemView) {
            super(itemView);
            sideMenuIcon = itemView.findViewById(R.id.sideMenuIcon);
            sideMenuText = itemView.findViewById(R.id.sideMenuText);
        }
    }

    public void changeColor(int indexThis, vHolder holderThis) {
        if (indexThis == 0) {
            for (int i = 0; i < icon.size(); i++) {
                holderThis.sideMenuIcon.setImageResource(icon.get(i));
                holderThis.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color26));
            }
            holderThis.sideMenuIcon.setImageResource(R.drawable.propert_find_fill);
            holderThis.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (indexThis == 1) {
            for (int i = 0; i < icon.size(); i++) {
                holderThis.sideMenuIcon.setImageResource(icon.get(i));
                holderThis.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color26));
            }
            holderThis.sideMenuIcon.setImageResource(R.drawable.my_portfolio_fill);
            holderThis.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (indexThis == 2) {
            for (int i = 0; i < icon.size(); i++) {
                holderThis.sideMenuIcon.setImageResource(icon.get(i));
                holderThis.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color26));
            }
            holderThis.sideMenuIcon.setImageResource(R.drawable.user_fill);
            holderThis.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (indexThis == 3) {
            for (int i = 0; i < icon.size(); i++) {
                holderThis.sideMenuIcon.setImageResource(icon.get(i));
                holderThis.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color26));
            }
            holderThis.sideMenuIcon.setImageResource(R.drawable.maintainence_fill);
            holderThis.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (indexThis == 4) {
            for (int i = 0; i < icon.size(); i++) {
                holderThis.sideMenuIcon.setImageResource(icon.get(i));
                holderThis.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color26));
            }
            holderThis.sideMenuIcon.setImageResource(R.drawable.bell_fill);
            holderThis.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color5A));
            ((DashBoardScreen) mContext).nav_drawer.closeDrawer(GravityCompat.START);
            if (((DashBoardScreen) mContext).fragments_status != 4) {
                ((DashBoardScreen) mContext).fragments_status = 4;
                navController.navigate(R.id.action_global_reminderFragment);
            }
        } else if (indexThis == 5) {
            for (int i = 0; i < icon.size(); i++) {
                holderThis.sideMenuIcon.setImageResource(icon.get(i));
                holderThis.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color26));
            }
            holderThis.sideMenuIcon.setImageResource(R.drawable.line_chart_fill);
            holderThis.sideMenuText.setTextColor(mContext.getResources().getColor(R.color.color5A));
        }
    }
}
