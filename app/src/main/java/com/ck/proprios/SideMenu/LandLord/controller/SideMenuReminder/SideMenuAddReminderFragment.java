package com.ck.proprios.SideMenu.LandLord.controller.SideMenuReminder;

import android.content.Context;
import android.os.Bundle;

import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ck.proprios.R;
import com.ck.proprios.SideMenu.LandLord.pojo.SideMenuAddReminder;
import com.ck.proprios.SideMenu.LandLord.pojo.SideMenuReminderResponse;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.dashboard.controller.DashBoardScreen;
import com.ck.proprios.helpers.BottomSheetButton;
import com.ck.proprios.helpers.CustomCalendar;
import com.ck.proprios.helpers.Loader;
import com.ck.proprios.intro.pojo.DashBoardInitialLoad;
import com.ck.proprios.network.APIClient;
import com.ck.proprios.network.APIInterface;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.navigation.Navigation.findNavController;

public class SideMenuAddReminderFragment extends Fragment implements View.OnClickListener {

    NavController navController;
    APIInterface apiInterface;
    Loader loader;
    LinearLayout side_menu_add_reminder_layout;
    TextInputLayout edtReminderName, edtNotes;
    RelativeLayout rlDateTime, rlTimeToRemind, rlProperty, rlAutoRenew;
    TextView tvDateTimeLabel, tvDateTimeValue, tvTimeToRemindLabel, tvTimeToRemindValue,
            tvPropertyLabel, tvPropertyValue, tvAutoRenewLabel, tvAutoRenewValue;
    BottomSheetButton bottomSheetButton;
    CustomCalendar customCalendar;
    List<DashBoardInitialLoad.ReminderWhen> listReminder = new ArrayList<>();
    List<DashBoardInitialLoad.PropertyPortfolio> listProperty = new ArrayList<>();
    List<String> whenToRemindList = new ArrayList<>();
    List<String> propertyList = new ArrayList<>();
    List<String> autoRenewList = new ArrayList<>();
    int positionWhenToRemind = -1, positionProperty = -1, positionAutoRenew = -1;


    public SideMenuAddReminderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_side_menu_add_reminder, container, false);
        navController = Navigation.findNavController(getActivity(), R.id.fragment_container);
        apiInterface = APIClient.getAuthClient().create(APIInterface.class);
        loader = new Loader(getContext());
        bottomSheetButton = new BottomSheetButton();
        customCalendar = new CustomCalendar();

        ((DashBoardScreen) getActivity()).headerTextBtnText.setTextColor(getContext().
                getResources().getColor(R.color.color99));
        ((DashBoardScreen) getActivity()).headerTextBtnLayout.setEnabled(false);

        getReminderWhenList();
        getPropertyList();
        getAutoRenewList();

        ((GlobalCLass) getContext().getApplicationContext()).setSideMenuAddReminderFragment(this);
        side_menu_add_reminder_layout = view.findViewById(R.id.side_menu_add_reminder_layout);
        edtReminderName = view.findViewById(R.id.edtReminderName);
        edtNotes = view.findViewById(R.id.edtNotes);
        rlDateTime = view.findViewById(R.id.rlDateTime);
        rlTimeToRemind = view.findViewById(R.id.rlTimeToRemind);
        rlProperty = view.findViewById(R.id.rlProperty);
        rlAutoRenew = view.findViewById(R.id.rlAutoRenew);

        tvDateTimeLabel = view.findViewById(R.id.tvDateTimeLabel);
        tvDateTimeValue = view.findViewById(R.id.tvDateTimeValue);
        tvTimeToRemindLabel = view.findViewById(R.id.tvTimeToRemindLabel);
        tvTimeToRemindValue = view.findViewById(R.id.tvTimeToRemindValue);
        tvPropertyLabel = view.findViewById(R.id.tvPropertyLabel);
        tvPropertyValue = view.findViewById(R.id.tvPropertyValue);
        tvAutoRenewLabel = view.findViewById(R.id.tvAutoRenewLabel);
        tvAutoRenewValue = view.findViewById(R.id.tvAutoRenewValue);

        ((DashBoardScreen) this.getActivity()).btnMenu1.setOnClickListener(this);
        ((DashBoardScreen) this.getActivity()).btnMenu2.setOnClickListener(this);
        ((DashBoardScreen) this.getActivity()).headerTextBtnLayout.setOnClickListener(this);
        rlDateTime.setOnClickListener(this);
        rlTimeToRemind.setOnClickListener(this);
        rlProperty.setOnClickListener(this);
        rlAutoRenew.setOnClickListener(this);
        side_menu_add_reminder_layout.setOnClickListener(this);

        hideKeyBoard();

        edtReminderName.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                changeColor();
            }
        });

        edtNotes.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                changeColor();
            }
        });

        return view;
    }

    private void getAutoRenewList() {
        autoRenewList.clear();
        autoRenewList.add("Yes");
        autoRenewList.add("No");
    }

    private void getReminderWhenList() {
        listReminder.clear();
        listReminder.addAll(((GlobalCLass) getContext().getApplicationContext()).getDashBoardInitialLoad().getReminderWhen());
        whenToRemindList.clear();
        for (int i = 0; i < listReminder.size(); i++) {
            whenToRemindList.add(listReminder.get(i).getOptionValue());
        }
    }

    private void getPropertyList() {
        listProperty.clear();
        listProperty.addAll(((GlobalCLass) getContext().getApplicationContext()).getDashBoardInitialLoad().getPropertyPortfolio());
        propertyList.clear();
        propertyList.add("All");
        for (int i = 0; i < listProperty.size(); i++) {
            propertyList.add(listProperty.get(i).getPropertyAddress1());
        }
    }

    @Override
    public void onClick(View v) {
        if (v == ((DashBoardScreen) this.getActivity()).btnMenu1) {
            navController.navigate(R.id.action_global_reminderFragment);
        } else if (v == ((DashBoardScreen) this.getActivity()).btnMenu2) {
            ((DashBoardScreen) this.getActivity()).nav_drawer.openDrawer(GravityCompat.START);
        } else if (v == rlDateTime) {
            customCalendar.calendarPopUp(getContext(), tvDateTimeLabel, tvDateTimeValue, navController, "");
        } else if (v == rlTimeToRemind) {
            positionWhenToRemind = bottomSheetButton.listTimeToRemindPopUp(getContext(),
                    tvTimeToRemindLabel, tvTimeToRemindValue, whenToRemindList,
                    positionWhenToRemind, navController);
            Log.wtf("positionNew", "" + positionWhenToRemind);
        } else if (v == rlProperty) {
            positionProperty = bottomSheetButton.listPropertyPopUp(getContext(), tvPropertyLabel, tvPropertyValue, propertyList,
                    positionWhenToRemind,navController);
            Log.wtf("positionNew1", "" + positionProperty);
        } else if (v == rlAutoRenew) {
            positionAutoRenew = bottomSheetButton.listAutoRenewPopUp(getContext(), tvAutoRenewLabel,
                    tvAutoRenewValue, autoRenewList, positionAutoRenew, navController);
            Log.wtf("positionNew2", "" + positionAutoRenew);
        } else if (v == ((DashBoardScreen) this.getActivity()).headerTextBtnLayout) {
            addReminderApi();
        } else if (v == side_menu_add_reminder_layout) {
            hideKeyBoard();
        }
    }

    public void addReminderApi() {
        String reminder_name = edtReminderName.getEditText().getText().toString().trim();
        String reminder_datetime = tvDateTimeValue.getText().toString().trim();
        String reminder_notes = edtNotes.getEditText().getText().toString().trim();
        Log.wtf("add_reminder", "Name: " + reminder_name + " Date: " + reminder_datetime
                + " Time to Remind: " + listReminder.get(positionWhenToRemind).getOptionKey() + " Property:"
                + listProperty.get(positionProperty).getPropertyId() + " Auto Renew: " + tvAutoRenewValue.getText()
                + " Notes: " + reminder_notes
        );
        loader.showLoader();
        HashMap<String, String> map = new HashMap<>();
        map.put("request", "add-a-reminder");
        map.put("customer_id", "uR765FohTs_NrL0BMWOGvQ==");
        map.put("customer_type", "private-landlord");
        map.put("customer_api_key", "9e52c2ea3e734651");
        map.put("remote_address", "Not allowed by user");
        map.put("reminder_id", "new");
        map.put("reminder_name", reminder_name);
        map.put("reminder_datetime", reminder_datetime);
        map.put("reminder_when", listReminder.get(positionWhenToRemind).getOptionKey());
        map.put("reminder_notes", reminder_notes);
        if (positionProperty == -1) {
            map.put("property_id", "");
        } else {
            map.put("property_id", listProperty.get(positionProperty).getPropertyId());
        }
        map.put("auto_renew", tvAutoRenewValue.getText().toString().trim());

        Log.wtf("sideMenuAddReminder", "Params: " + map);

        Call<SideMenuAddReminder> call = apiInterface.sideMenuAddReminder(map);
        call.enqueue(new Callback<SideMenuAddReminder>() {
            @Override
            public void onResponse(Call<SideMenuAddReminder> call, Response<SideMenuAddReminder> response) {
                loader.hideLoader();
                SideMenuAddReminder model = response.body();
                if (model.getSuccess() != null) {
                    navController.navigate(R.id.action_global_reminderFragment);
                }
                Log.wtf("sideMenuAddReminder", "response: " + model.getSuccess());
            }

            @Override
            public void onFailure(Call<SideMenuAddReminder> call, Throwable t) {
                loader.hideLoader();
                Toast.makeText(getContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.wtf("sideMenuAddReminder", "error: " + t.getMessage());
            }
        });
    }

    public void changeColor() {
        if (edtReminderName.getEditText().getText().toString().trim().length() > 0 &&
                tvDateTimeValue.getText().toString().trim().length() > 0 &&
                tvTimeToRemindValue.getText().toString().trim().length() > 0 &&
                tvAutoRenewValue.getText().toString().trim().length() > 0 &&
                edtNotes.getEditText().getText().toString().trim().length() > 0) {
            ((DashBoardScreen) getActivity()).headerTextBtnText.setTextColor(getContext().
                    getResources().getColor(R.color.color5A));
            ((DashBoardScreen) getActivity()).headerTextBtnLayout.setEnabled(true);
        } else {
            ((DashBoardScreen) getActivity()).headerTextBtnText.setTextColor(getContext().
                    getResources().getColor(R.color.color99));
            ((DashBoardScreen) getActivity()).headerTextBtnLayout.setEnabled(false);
        }
    }

    public void hideKeyBoard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            edtReminderName.clearFocus();
            edtNotes.clearFocus();
        }
    }
}