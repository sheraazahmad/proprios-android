package com.ck.proprios.SideMenu.LandLord.controller.financial_statement;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ck.proprios.R;

public class FinancialStatementFragment extends Fragment {

    public FinancialStatementFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_financial_statement, container, false);
    }
}