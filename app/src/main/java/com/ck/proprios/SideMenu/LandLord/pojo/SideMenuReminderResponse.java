package com.ck.proprios.SideMenu.LandLord.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SideMenuReminderResponse {
    @SerializedName("reminder_id")
    @Expose
    private String reminderId;
    @SerializedName("decoded_reminder_id")
    @Expose
    private String decodedReminderId;
    @SerializedName("reminder_name")
    @Expose
    private String reminderName;
    @SerializedName("reminder_datetime")
    @Expose
    private String reminderDatetime;
    @SerializedName("reminder_date_status")
    @Expose
    private String reminderDateStatus;
    @SerializedName("reminder_date")
    @Expose
    private String reminderDate;
    @SerializedName("reminder_date_short")
    @Expose
    private String reminderDateShort;
    @SerializedName("reminder_time")
    @Expose
    private String reminderTime;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("property_details")
    @Expose
    private String propertyDetails;

    public String getReminderId() {
        return reminderId;
    }

    public void setReminderId(String reminderId) {
        this.reminderId = reminderId;
    }

    public String getDecodedReminderId() {
        return decodedReminderId;
    }

    public void setDecodedReminderId(String decodedReminderId) {
        this.decodedReminderId = decodedReminderId;
    }

    public String getReminderName() {
        return reminderName;
    }

    public void setReminderName(String reminderName) {
        this.reminderName = reminderName;
    }

    public String getReminderDatetime() {
        return reminderDatetime;
    }

    public void setReminderDatetime(String reminderDatetime) {
        this.reminderDatetime = reminderDatetime;
    }

    public String getReminderDateStatus() {
        return reminderDateStatus;
    }

    public void setReminderDateStatus(String reminderDateStatus) {
        this.reminderDateStatus = reminderDateStatus;
    }

    public String getReminderDate() {
        return reminderDate;
    }

    public void setReminderDate(String reminderDate) {
        this.reminderDate = reminderDate;
    }

    public String getReminderDateShort() {
        return reminderDateShort;
    }

    public void setReminderDateShort(String reminderDateShort) {
        this.reminderDateShort = reminderDateShort;
    }

    public String getReminderTime() {
        return reminderTime;
    }

    public void setReminderTime(String reminderTime) {
        this.reminderTime = reminderTime;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getPropertyDetails() {
        return propertyDetails;
    }

    public void setPropertyDetails(String propertyDetails) {
        this.propertyDetails = propertyDetails;
    }
}
