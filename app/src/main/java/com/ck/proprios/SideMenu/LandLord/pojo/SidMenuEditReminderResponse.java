package com.ck.proprios.SideMenu.LandLord.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SidMenuEditReminderResponse {
    @SerializedName("reminder_id")
    @Expose
    private String reminderId;
    @SerializedName("property_id")
    @Expose
    private String propertyId;
    @SerializedName("reminder_name")
    @Expose
    private String reminderName;
    @SerializedName("reminder_datetime")
    @Expose
    private String reminderDatetime;
    @SerializedName("reminder_notes")
    @Expose
    private String reminderNotes;
    @SerializedName("reminder_when")
    @Expose
    private String reminderWhen;
    @SerializedName("auto_renew")
    @Expose
    private String autoRenew;
    @SerializedName("reminder_only_date")
    @Expose
    private String reminderOnlyDate;
    @SerializedName("reminder_only_time")
    @Expose
    private String reminderOnlyTime;

    public String getReminderId() {
        return reminderId;
    }

    public void setReminderId(String reminderId) {
        this.reminderId = reminderId;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getReminderName() {
        return reminderName;
    }

    public void setReminderName(String reminderName) {
        this.reminderName = reminderName;
    }

    public String getReminderDatetime() {
        return reminderDatetime;
    }

    public void setReminderDatetime(String reminderDatetime) {
        this.reminderDatetime = reminderDatetime;
    }

    public String getReminderNotes() {
        return reminderNotes;
    }

    public void setReminderNotes(String reminderNotes) {
        this.reminderNotes = reminderNotes;
    }

    public String getReminderWhen() {
        return reminderWhen;
    }

    public void setReminderWhen(String reminderWhen) {
        this.reminderWhen = reminderWhen;
    }

    public String getAutoRenew() {
        return autoRenew;
    }

    public void setAutoRenew(String autoRenew) {
        this.autoRenew = autoRenew;
    }

    public String getReminderOnlyDate() {
        return reminderOnlyDate;
    }

    public void setReminderOnlyDate(String reminderOnlyDate) {
        this.reminderOnlyDate = reminderOnlyDate;
    }

    public String getReminderOnlyTime() {
        return reminderOnlyTime;
    }

    public void setReminderOnlyTime(String reminderOnlyTime) {
        this.reminderOnlyTime = reminderOnlyTime;
    }
}
