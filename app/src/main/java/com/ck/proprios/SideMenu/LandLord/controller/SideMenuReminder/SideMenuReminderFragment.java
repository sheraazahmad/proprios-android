package com.ck.proprios.SideMenu.LandLord.controller.SideMenuReminder;

import android.os.Bundle;

import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ck.proprios.R;
import com.ck.proprios.SideMenu.LandLord.Adapter.SideMenuReminderAdapter;
import com.ck.proprios.SideMenu.LandLord.pojo.SideMenuDeleteReminder;
import com.ck.proprios.SideMenu.LandLord.pojo.SideMenuReminderResponse;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.dashboard.controller.DashBoardScreen;
import com.ck.proprios.dashboard.pojo.DashBoardAppointments;
import com.ck.proprios.helpers.BottomSheetButton;
import com.ck.proprios.helpers.Loader;
import com.ck.proprios.intro.controller.RegisterScreen;
import com.ck.proprios.network.APIClient;
import com.ck.proprios.network.APIInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.navigation.Navigation.findNavController;

public class SideMenuReminderFragment extends Fragment implements View.OnClickListener {

    NavController navController;
    APIInterface apiInterface;
    Loader loader;
    boolean refresh = false;
    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView rvSideMenuReminder;
    SideMenuReminderAdapter adapter;
    List<SideMenuReminderResponse> list = new ArrayList<>();
    BottomSheetButton bottomSheetButton;
    List<String> labelsList = new ArrayList<>();
    List<Integer> iconsList = new ArrayList<>();
    static String reminder_id = null;

    public SideMenuReminderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reminder, container, false);

        ((GlobalCLass) getContext().getApplicationContext()).setSideMenuReminderFragment(this);
        navController = Navigation.findNavController(getActivity(), R.id.fragment_container);
        apiInterface = APIClient.getAuthClient().create(APIInterface.class);
        loader = new Loader(getContext());
        bottomSheetButton = new BottomSheetButton();
        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        rvSideMenuReminder = view.findViewById(R.id.rvSideMenuReminder);

        labelsList.clear();
        iconsList.clear();
        labelsList.add("Edit Reminder");
        labelsList.add("Delete");

        iconsList.add(R.drawable.edit_pen);
        iconsList.add(R.drawable.delete);

//        list.clear();
        adapter = new SideMenuReminderAdapter(getContext(), list, SideMenuReminderFragment.this);
        rvSideMenuReminder.setHasFixedSize(true);
        rvSideMenuReminder.setLayoutManager(new LinearLayoutManager(getContext()));
        rvSideMenuReminder.setAdapter(adapter);
        callAPI();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh = true;
                callAPI();
                refresh = false;
            }
        });
        ((DashBoardScreen) this.getActivity()).firstIconLayout.setOnClickListener(this);
        ((DashBoardScreen) this.getActivity()).btnMenu1.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        if (v == ((DashBoardScreen) this.getActivity()).firstIconLayout) {
            navController.navigate(R.id.action_global_sideMenuAddReminderFragment);
        } else if (v == ((DashBoardScreen) this.getActivity()).btnMenu1) {
            ((DashBoardScreen) this.getActivity()).nav_drawer.openDrawer(GravityCompat.START);
        }
    }

    public void openBottomSheet(String reminder_id) {
        this.reminder_id = reminder_id;
        bottomSheetButton.openBottomSheetPopUp(getContext(), labelsList, iconsList, 1);
    }

    private void callAPI() {
        if (refresh) {
        } else {
            loader.showLoader();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("request", "reminders");
        map.put("customer_id", "uR765FohTs_NrL0BMWOGvQ==");
        map.put("customer_type", "private-landlord");
        map.put("customer_api_key", "9e52c2ea3e734651");
        map.put("remote_address", "Not allowed by user");
        map.put("user_agent", "Mobile|iPhone 11 Pro|iOS 13.4.1|Safari XX");

        Log.wtf("sideMenuReminder", "Params: " + map);

        Call<List<SideMenuReminderResponse>> call = apiInterface.sideMenuReminder(map);
        call.enqueue(new Callback<List<SideMenuReminderResponse>>() {
            @Override
            public void onResponse(Call<List<SideMenuReminderResponse>> call, Response<List<SideMenuReminderResponse>> response) {
                loader.hideLoader();
                List<SideMenuReminderResponse> model = response.body();
                list.clear();
                list.addAll(model);
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
//                myView();
//                Log.wtf("dashBoardRent", "response: " + new Gson().toJson(response.body()));
                Log.wtf("sideMenuReminder", "response: " + list.size());
            }

            @Override
            public void onFailure(Call<List<SideMenuReminderResponse>> call, Throwable t) {
                loader.hideLoader();
                Toast.makeText(getContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.wtf("sideMenuReminder", "error: " + t.getMessage());
            }
        });
    }

    public void deleteAPI() {
        loader.showLoader();
        HashMap<String, String> map = new HashMap<>();
        map.put("request", "delete-a-reminder");
        map.put("customer_id", "uR765FohTs_NrL0BMWOGvQ==");
        map.put("customer_type", "private-landlord");
        map.put("customer_api_key", "9e52c2ea3e734651");
        map.put("remote_address", "Not allowed by user");
        map.put("reminder_id", reminder_id);
        map.put("user_agent", "Mobile|iPhone 11 Pro|iOS 13.4.1|Safari XX");

        Log.wtf("sideMenuDeleteReminder", "Params: " + map);

        Call<SideMenuDeleteReminder> call = apiInterface.sideMenuDeleteReminder(map);
        call.enqueue(new Callback<SideMenuDeleteReminder>() {
            @Override
            public void onResponse(Call<SideMenuDeleteReminder> call, Response<SideMenuDeleteReminder> response) {
                loader.hideLoader();
                SideMenuDeleteReminder model = response.body();
                callAPI();
                Log.wtf("sideMenuDeleteReminder", "response: " + list.size());
            }

            @Override
            public void onFailure(Call<SideMenuDeleteReminder> call, Throwable t) {
                loader.hideLoader();
                Toast.makeText(getContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.wtf("sideMenuDeleteReminder", "error: " + t.getMessage());
            }
        });
    }

    public void editReminder() {
        navController.navigate(R.id.action_global_sideMenuEditReminderFragment);
    }
}