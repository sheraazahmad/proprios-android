package com.ck.proprios.SideMenu.LandLord.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ck.proprios.R;
import com.ck.proprios.SideMenu.LandLord.controller.SideMenuReminder.SideMenuReminderFragment;
import com.ck.proprios.SideMenu.LandLord.pojo.SideMenuReminderResponse;

import java.util.ArrayList;
import java.util.List;

public class SideMenuReminderAdapter extends RecyclerView.Adapter<SideMenuReminderAdapter.vHolder> {

    Context mContext;
    List<SideMenuReminderResponse> list = new ArrayList<>();
    SideMenuReminderFragment mFragment;

    public SideMenuReminderAdapter(Context mContext, List<SideMenuReminderResponse> list,
                                   SideMenuReminderFragment mFragment) {
        this.mContext = mContext;
        this.list = list;
        this.mFragment = mFragment;
    }

    @NonNull
    @Override
    public vHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.dashboard_reminder_item, parent, false);
        return new SideMenuReminderAdapter.vHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull vHolder holder, int position) {
        final SideMenuReminderResponse response = list.get(position);
        holder.tvReminderDate.setText(response.getReminderDateShort());
        holder.tvReminderTime.setText(response.getReminderTime());
        holder.tvReminderHeading.setText(response.getReminderName());
        if (response.getPropertyDetails().length() > 0) {
            holder.tvReminderSubHeading.setVisibility(View.VISIBLE);
            holder.tvReminderSubHeading.setText(response.getPropertyDetails());
        } else {
            holder.tvReminderSubHeading.setVisibility(View.GONE);
        }

        if (response.getReminderDateStatus().matches("complete")) {
            holder.viewDate.setBackgroundColor(mContext.getResources().getColor(R.color.colorF3));
        } else {
            holder.viewDate.setBackgroundColor(mContext.getResources().getColor(R.color.color69));
        }

        holder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragment.openBottomSheet(response.getReminderId());
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.wtf("reminder_id", "" + response.getReminderId());
                mFragment.openBottomSheet(response.getReminderId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class vHolder extends RecyclerView.ViewHolder {
        TextView tvReminderDate, tvReminderTime, tvReminderHeading, tvReminderSubHeading;
        View viewDate;
        ImageView btnMore;

        public vHolder(@NonNull View itemView) {
            super(itemView);
            tvReminderDate = itemView.findViewById(R.id.tvReminderDate);
            tvReminderTime = itemView.findViewById(R.id.tvReminderTime);
            tvReminderHeading = itemView.findViewById(R.id.tvReminderHeading);
            tvReminderSubHeading = itemView.findViewById(R.id.tvReminderSubHeading);
            viewDate = itemView.findViewById(R.id.viewDate);
            btnMore = itemView.findViewById(R.id.btnMore);
        }
    }
}
