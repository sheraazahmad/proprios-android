package com.ck.proprios.application;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.ck.proprios.SideMenu.LandLord.controller.SideMenuReminder.SideMenuAddReminderFragment;
import com.ck.proprios.SideMenu.LandLord.controller.SideMenuReminder.SideMenuEditReminderFragment;
import com.ck.proprios.SideMenu.LandLord.controller.SideMenuReminder.SideMenuReminderFragment;
import com.ck.proprios.helpers.Loader;
import com.ck.proprios.helpers.SharedPrefHelper;
import com.ck.proprios.intro.controller.FingerPrintScreen;
import com.ck.proprios.intro.controller.PassCodeScreen;
import com.ck.proprios.intro.controller.SplashScreen;
import com.ck.proprios.intro.pojo.DashBoardInitialLoad;
import com.ck.proprios.intro.pojo.LoginResponse;
import com.ck.proprios.network.APIClient;
import com.ck.proprios.network.APIInterface;
import com.google.gson.Gson;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GlobalCLass extends Application {
    public boolean slider = false;
    public boolean noSlide = false;
    APIInterface apiInterface;
    Loader loader;

    public DashBoardInitialLoad getDashBoardInitialLoad() {
        return dashBoardInitialLoad;
    }

    public void setDashBoardInitialLoad(DashBoardInitialLoad dashBoardInitialLoad) {
        this.dashBoardInitialLoad = dashBoardInitialLoad;
    }

    DashBoardInitialLoad dashBoardInitialLoad;


    public boolean isNoSlide() {
        return noSlide;
    }

    public void setNoSlide(boolean noSlide) {
        this.noSlide = noSlide;
    }

    public boolean isSlider() {
        return slider;
    }

    public void setSlider(boolean slider) {
        this.slider = slider;
    }

    public void dashBoardInitial(final Context context) {
        apiInterface = APIClient.getAuthClient().create(APIInterface.class);
        loader = new Loader(context);
        loader.showLoader();
        HashMap<String, String> map = new HashMap<>();
        map.put("request", "dashboard-initial-load");
        map.put("customer_id", "uR765FohTs_NrL0BMWOGvQ==");
        map.put("customer_type", "private-landlord");
        map.put("customer_api_key", "9e52c2ea3e734651");
        map.put("remote_address", "Not allowed by user");
        map.put("user_agent", "Mobile|iPhone 11 Pro|iOS 13.4.1|Safari XX");

        Log.wtf("dashBoardInitial", "Params: " + map);

        Call<DashBoardInitialLoad> call = apiInterface.dashBoardInitialLoad(map);
        call.enqueue(new Callback<DashBoardInitialLoad>() {
            @Override
            public void onResponse(Call<DashBoardInitialLoad> call, Response<DashBoardInitialLoad> response) {
                loader.hideLoader();
                DashBoardInitialLoad model = response.body();
                setDashBoardInitialLoad(model);
                if (SharedPrefHelper.getInstance(context).getLoginStatus()) {
                    if (SharedPrefHelper.getInstance(context).getFingerPrintStatus()) {
                        Intent intent = new Intent(context, FingerPrintScreen.class);
                        context.startActivity(intent);
                    } else {
                        ((GlobalCLass) getApplicationContext()).setSlider(true);
                        Intent intent = new Intent(context, PassCodeScreen.class);
                        context.startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(context, FingerPrintScreen.class);
                    context.startActivity(intent);
                }
                Log.wtf("dashBoardInitial", "response: " + new Gson().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<DashBoardInitialLoad> call, Throwable t) {
                loader.hideLoader();
                Toast.makeText(context, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.wtf("dashBoardInitial", "error: " + t.getMessage());
            }
        });
    }


    LoginResponse loginResponse;

    public void setLoginData(LoginResponse loginData) {
        this.loginResponse = loginData;
    }

    public LoginResponse getLoginResponse() {
        return loginResponse;
    }

    public SideMenuAddReminderFragment getSideMenuAddReminderFragment() {
        return sideMenuAddReminderFragment;
    }

    public void setSideMenuAddReminderFragment(SideMenuAddReminderFragment sideMenuAddReminderFragment) {
        this.sideMenuAddReminderFragment = sideMenuAddReminderFragment;
    }

    public SideMenuAddReminderFragment sideMenuAddReminderFragment;

    public SideMenuReminderFragment getSideMenuReminderFragment() {
        return sideMenuReminderFragment;
    }

    public void setSideMenuReminderFragment(SideMenuReminderFragment sideMenuReminderFragment) {
        this.sideMenuReminderFragment = sideMenuReminderFragment;
    }

    public SideMenuReminderFragment sideMenuReminderFragment;

    public SideMenuEditReminderFragment sideMenuEditReminderFragment;

    public SideMenuEditReminderFragment getSideMenuEditReminderFragment() {
        return sideMenuEditReminderFragment;
    }

    public void setSideMenuEditReminderFragment(SideMenuEditReminderFragment sideMenuEditReminderFragment) {
        this.sideMenuEditReminderFragment = sideMenuEditReminderFragment;
    }
}
