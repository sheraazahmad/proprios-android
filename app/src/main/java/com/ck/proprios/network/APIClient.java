package com.ck.proprios.network;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static Retrofit retrofit = null;

    public static Retrofit getAuthClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();

        clientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Content-type", "application/json")
                        .addHeader("cache-control", "no-cache")
                        .addHeader("maulana-auth-sheikh-mehmet", "Bismillah*_3EOkkeLrkAr_*Maulana-Sheikh-Nazim&*shrfZl4LaAQ1C8Hg_X3R6dP5b")
                        .build();
                return chain.proceed(newRequest);
            }
        });
        clientBuilder.addInterceptor(interceptor);
        clientBuilder.connectTimeout(10, TimeUnit.SECONDS);
        clientBuilder.writeTimeout(30, TimeUnit.SECONDS);
        clientBuilder.readTimeout(1, TimeUnit.MINUTES);

        retrofit = new Retrofit.Builder()
//                .baseUrl("https://www.v72.appcrates.net/myrunsteak/public/")
                .baseUrl("https://api-webservices.proprios.com/?")
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientBuilder.build())
                .build();

        return retrofit;
    }
}
