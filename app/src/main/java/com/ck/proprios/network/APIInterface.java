package com.ck.proprios.network;

import com.ck.proprios.SideMenu.LandLord.pojo.SidMenuEditReminderResponse;
import com.ck.proprios.SideMenu.LandLord.pojo.SideMenuAddReminder;
import com.ck.proprios.SideMenu.LandLord.pojo.SideMenuDeleteReminder;
import com.ck.proprios.SideMenu.LandLord.pojo.SideMenuReminderResponse;
import com.ck.proprios.SideMenu.LandLord.pojo.SideMenuUpdateReminderResponse;
import com.ck.proprios.dashboard.pojo.DashBoardAppointments;
import com.ck.proprios.dashboard.pojo.DashBoardMaintenance;
import com.ck.proprios.dashboard.pojo.DashBoardRent;
import com.ck.proprios.intro.pojo.DashBoardInitialLoad;
import com.ck.proprios.intro.pojo.LoginResponse;
import com.ck.proprios.intro.pojo.RegisterResponse;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface APIInterface {
    @GET("https://api-webservices.proprios.com/?")
    Call<DashBoardInitialLoad> dashBoardInitialLoad(@QueryMap Map<String, String> params);

    @GET("https://api-webservices.proprios.com/?")
    Call<LoginResponse> login(@QueryMap Map<String, String> params);

    @POST("https://api-webservices.proprios.com/?")
    Call<RegisterResponse> register(@QueryMap Map<String, String> params);

    @GET("https://api-webservices.proprios.com/?")
    Call<DashBoardRent> dashboardRent(@QueryMap Map<String, String> params);

    @GET("https://api-webservices.proprios.com/?")
    Call<List<DashBoardMaintenance>> dashboardMaintenance(@QueryMap Map<String, String> params);

    @GET("https://api-webservices.proprios.com/?")
    Call<List<DashBoardAppointments>> dashboardAppointments(@QueryMap Map<String, String> params);

    @GET("https://api-webservices.proprios.com/?")
    Call<List<SideMenuReminderResponse>> sideMenuReminder(@QueryMap Map<String, String> params);

    @POST("https://api-webservices.proprios.com/?")
    Call<SideMenuAddReminder> sideMenuAddReminder(@QueryMap Map<String, String> params);

    @POST("https://api-webservices.proprios.com/?")
    Call<SideMenuDeleteReminder> sideMenuDeleteReminder(@QueryMap Map<String, String> params);

    @GET("https://api-webservices.proprios.com/?")
    Call<SidMenuEditReminderResponse> sideMenuEditReminder(@QueryMap Map<String, String> params);

    @POST("https://api-webservices.proprios.com/?")
    Call<SideMenuUpdateReminderResponse> sideMenuUpdateReminder(@QueryMap Map<String, String> params);
}
