package com.ck.proprios.intro.controller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.ck.proprios.R;
import com.ck.proprios.dashboard.controller.DashBoardScreen;
import com.ck.proprios.helpers.SharedPrefHelper;

import java.util.concurrent.Executor;

public class FingerPrintScreen extends AppCompatActivity implements View.OnClickListener {

    TextView btnTouchId, btnSkipNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finger_print_screen);
        Animatoo.animateSlideLeft(FingerPrintScreen.this);

        btnTouchId = findViewById(R.id.btnTouchId);
        btnSkipNow = findViewById(R.id.btnSkipNow);

        btnTouchId.setOnClickListener(this);
        btnSkipNow.setOnClickListener(this);

        if (SharedPrefHelper.getInstance(this).getLoginStatus()) {
            btnTouchId.setVisibility(View.GONE);
            btnSkipNow.setVisibility(View.GONE);
            fingerPrintScanner();
        }
    }

    public void fingerPrintScanner() {
        BiometricManager biometricManager = androidx.biometric.BiometricManager.from(getApplicationContext());
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
//                Toast.makeText(this, "You can use", Toast.LENGTH_SHORT).show();
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                Toast.makeText(this, "No hardware available", Toast.LENGTH_SHORT).show();
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                Toast.makeText(this, "Temporary Unavailable", Toast.LENGTH_SHORT).show();
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                Toast.makeText(this, "No Enrolled", Toast.LENGTH_SHORT).show();
                break;
        }

        Executor executor = ContextCompat.getMainExecutor(getApplicationContext());
        BiometricPrompt biometricPrompt = new BiometricPrompt(FingerPrintScreen.this, executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                    Intent intent = new Intent(FingerPrintScreen.this, PassCodeScreen.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                if (SharedPrefHelper.getInstance(FingerPrintScreen.this).getLoginStatus()) {
                    Intent intent = new Intent(FingerPrintScreen.this, DashBoardScreen.class);
                    startActivity(intent);
                    finishAffinity();
                } else {
                    SharedPrefHelper.getInstance(FingerPrintScreen.this).setFingerPrintStatus(true);
                    Intent intent = new Intent(FingerPrintScreen.this, PassCodeScreen.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
            }
        });

        BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Login")
                .setDescription("User your fingerprint")
                .setNegativeButtonText("Cancel")
                .setConfirmationRequired(false)
                .build();
//
        biometricPrompt.authenticate(promptInfo);
    }

    @Override
    public void onClick(View v) {
        if (v == btnTouchId) {
            fingerPrintScanner();
        } else if (v == btnSkipNow) {
            Intent intent = new Intent(FingerPrintScreen.this, PassCodeScreen.class);
            startActivity(intent);
        }
    }
}