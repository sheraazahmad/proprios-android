package com.ck.proprios.intro.controller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.ck.proprios.R;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.helpers.BottomSheetButton;
import com.ck.proprios.helpers.Loader;
import com.ck.proprios.network.APIClient;
import com.ck.proprios.network.APIInterface;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterScreen extends AppCompatActivity implements View.OnClickListener {

    TextView tvWhoYouAreLabel, tvWhoYouAreValue, tvLogin, tv8Character, tv1Uppercase,
            tv1Lowercase, tv1Number, tv1Special;
    CardView btnRegister;
    RelativeLayout btnBack, rlWhoYouAre;
    BottomSheetButton bottomSheetButton;
    String email = null;
    String password = null;
    TextInputLayout edtName, edtMobile, edtEmail, edtPassword;
    APIInterface apiInterface;
    Loader loader;
    LinearLayout terms;
    List<String> labelsList = new ArrayList<>();
    List<Integer> iconsList = new ArrayList<>();
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_screen);
        if (((GlobalCLass) getApplicationContext()).noSlide) {
            ((GlobalCLass) getApplicationContext()).setNoSlide(false);
        } else {
            if (((GlobalCLass) getApplicationContext()).isSlider()) {
                ((GlobalCLass) getApplicationContext()).setSlider(false);
                Animatoo.animateSlideRight(RegisterScreen.this);
            } else {
                Animatoo.animateSlideLeft(RegisterScreen.this);
            }
        }
        tvLogin = findViewById(R.id.tvLogin);
//        btnBack = findViewById(R.id.btnBack);
        btnRegister = findViewById(R.id.btnRegister);
        edtName = findViewById(R.id.edtName);
        edtMobile = findViewById(R.id.edtMobile);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        rlWhoYouAre = findViewById(R.id.rlWhoYouAre);
        tvWhoYouAreValue = findViewById(R.id.tvWhoYouAreValue);
        tvWhoYouAreLabel = findViewById(R.id.tvWhoYouAreLabel);
        tv8Character = findViewById(R.id.tv8Character);
        tv1Uppercase = findViewById(R.id.tv1Uppercase);
        tv1Lowercase = findViewById(R.id.tv1Lowercase);
        tv1Number = findViewById(R.id.tv1Number);
        tv1Special = findViewById(R.id.tv1Special);
        terms = findViewById(R.id.terms);
        labelsList.clear();
        iconsList.clear();

        labelsList.add("Terms & Conditions");
        labelsList.add("Privacy Policy");

        iconsList.add(R.drawable.terms);
        iconsList.add(R.drawable.privacypolicy);

        apiInterface = APIClient.getAuthClient().create(APIInterface.class);
        loader = new Loader(RegisterScreen.this);
        bottomSheetButton = new BottomSheetButton();

        tvLogin.setOnClickListener(this);
//        btnBack.setOnClickListener(this);
        rlWhoYouAre.setOnClickListener(this);
        terms.setOnClickListener(this);
        btnRegister.setOnClickListener(this);

        edtPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isNumber(s)) {
                    tv1Number.setTextColor(getResources().getColor(R.color.colorGreen));
                } else {
                    tv1Number.setTextColor(getResources().getColor(R.color.colorC6));
                }
                if (isSpecial(s)) {
                    tv1Special.setTextColor(getResources().getColor(R.color.colorGreen));
                } else {
                    tv1Special.setTextColor(getResources().getColor(R.color.colorC6));
                }
                if (isUpperCase(s)) {
                    tv1Uppercase.setTextColor(getResources().getColor(R.color.colorGreen));
                } else {
                    tv1Uppercase.setTextColor(getResources().getColor(R.color.colorC6));
                }
                if (isLowerCase(s)) {
                    tv1Lowercase.setTextColor(getResources().getColor(R.color.colorGreen));
                } else {
                    tv1Lowercase.setTextColor(getResources().getColor(R.color.colorC6));
                }
                if (edtPassword.getEditText().getText().toString().length() > 7) {
                    tv8Character.setTextColor(getResources().getColor(R.color.colorGreen));
                } else {
                    tv8Character.setTextColor(getResources().getColor(R.color.colorC6));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private boolean isNumber(CharSequence s) {
        if (s.toString().contains("1") || s.toString().contains("2") || s.toString().contains("3")
                || s.toString().contains("4") || s.toString().contains("5") || s.toString().contains("6")
                || s.toString().contains("7") || s.toString().contains("8") || s.toString().contains("9")
                || s.toString().contains("0")) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isSpecial(CharSequence s) {
        if (s.toString().contains("!") || s.toString().contains("£") || s.toString().contains("$")
                || s.toString().contains("%") || s.toString().contains("^") || s.toString().contains("6")
                || s.toString().contains("&") || s.toString().contains("*") || s.toString().contains("9")
                || s.toString().contains("(") || s.toString().contains(")") || s.toString().contains("_")
                || s.toString().contains("-") || s.toString().contains("@") || s.toString().contains("~")
                || s.toString().contains("#") || s.toString().contains("*") || s.toString().contains("+")
                || s.toString().contains("/") || s.toString().contains("?") || s.toString().contains("=")
                || s.toString().contains("{") || s.toString().contains("}") || s.toString().contains("[")
                || s.toString().contains("]") || s.toString().contains(":") || s.toString().contains(";")
                || s.toString().contains(",") || s.toString().contains(".")) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isUpperCase(CharSequence s) {
        if (s.toString().contains("A") || s.toString().contains("B") || s.toString().contains("C")
                || s.toString().contains("D") || s.toString().contains("E") || s.toString().contains("F")
                || s.toString().contains("G") || s.toString().contains("H") || s.toString().contains("I")
                || s.toString().contains("J") || s.toString().contains("K") || s.toString().contains("L")
                || s.toString().contains("M") || s.toString().contains("N") || s.toString().contains("O")
                || s.toString().contains("P") || s.toString().contains("Q") || s.toString().contains("R")
                || s.toString().contains("S") || s.toString().contains("T") || s.toString().contains("U")
                || s.toString().contains("V") || s.toString().contains("W") || s.toString().contains("X")
                || s.toString().contains("Y") || s.toString().contains("Z")) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isLowerCase(CharSequence s) {
        if (s.toString().contains("a") || s.toString().contains("b") || s.toString().contains("c")
                || s.toString().contains("d") || s.toString().contains("e") || s.toString().contains("f")
                || s.toString().contains("g") || s.toString().contains("h") || s.toString().contains("i")
                || s.toString().contains("j") || s.toString().contains("k") || s.toString().contains("l")
                || s.toString().contains("m") || s.toString().contains("n") || s.toString().contains("o")
                || s.toString().contains("p") || s.toString().contains("q") || s.toString().contains("r")
                || s.toString().contains("s") || s.toString().contains("t") || s.toString().contains("u")
                || s.toString().contains("v") || s.toString().contains("w") || s.toString().contains("x")
                || s.toString().contains("y") || s.toString().contains("z")) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View v) {
//        if (v == btnBack) {
//            ((GlobalCLass) getApplicationContext()).setSlider(true);
//            Intent intent = new Intent(RegisterScreen.this, WelcomeScreen.class);
//            startActivity(intent);
//        } else
        if (v == rlWhoYouAre) {
            bottomSheetButton.openBottomSheetList(RegisterScreen.this, tvWhoYouAreLabel, tvWhoYouAreValue);
        } else if (v == tvLogin) {
            ((GlobalCLass) getApplicationContext()).setNoSlide(true);
            Intent intent = new Intent(RegisterScreen.this, LoginScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivityForResult(intent, 0);
            overridePendingTransition(0, 0);
            finishAffinity();
        } else if (v == terms) {
            bottomSheetButton.openBottomSheetPopUp(RegisterScreen.this, labelsList, iconsList, 0);
        } else if (v == btnRegister) {
            String name = edtName.getEditText().getText().toString().trim();
            String mobile = edtMobile.getEditText().getText().toString().trim();
            String email = edtEmail.getEditText().getText().toString().trim();
            String password = edtPassword.getEditText().getText().toString().trim();
            if (tvWhoYouAreValue.getVisibility() == View.GONE) {
                Toast.makeText(this, "Fill Complete Form", Toast.LENGTH_SHORT).show();
            } else {
                if (name.length() > 0) {
                    if (mobile.length() > 0) {
                        if (email.matches(emailPattern) && email.length() > 0) {
                            if (password.matches(PASSWORD_PATTERN)) {
                                Intent intent = new Intent(RegisterScreen.this, TipsOfferScreen.class);
                                intent.putExtra("whoYouAre", "private-landlord");
                                intent.putExtra("name", name);
                                intent.putExtra("mobile", mobile);
                                intent.putExtra("email", email);
                                intent.putExtra("password", password);
                                startActivity(intent);
                            } else {
                                Toast.makeText(this, "Invalid Password Format", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(RegisterScreen.this, "Invalid Email Format", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "Mobile Number Required", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Name Required", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        ((GlobalCLass) getApplicationContext()).setSlider(true);
//        Intent intent = new Intent(RegisterScreen.this, WelcomeScreen.class);
//        startActivity(intent);
    }
}