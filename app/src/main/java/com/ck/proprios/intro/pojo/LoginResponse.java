package com.ck.proprios.intro.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_gender")
    @Expose
    private String customerGender;
    @SerializedName("customer_age_group")
    @Expose
    private String customerAgeGroup;
    @SerializedName("customer_email")
    @Expose
    private String customerEmail;
    @SerializedName("customer_address1")
    @Expose
    private String customerAddress1;
    @SerializedName("customer_country")
    @Expose
    private String customerCountry;
    @SerializedName("customer_mobile_number")
    @Expose
    private String customerMobileNumber;
    @SerializedName("customer_type")
    @Expose
    private String customerType;
    @SerializedName("customer_status")
    @Expose
    private String customerStatus;
    @SerializedName("parent_customer_id")
    @Expose
    private String parentCustomerId;
    @SerializedName("agreed_terms_datetime")
    @Expose
    private String agreedTermsDatetime;
    @SerializedName("customer_share_data_datetime")
    @Expose
    private String customerShareDataDatetime;
    @SerializedName("customer_api_key")
    @Expose
    private String customerApiKey;
    @SerializedName("completed_property")
    @Expose
    private String completedProperty;
    @SerializedName("completed_buyer")
    @Expose
    private String completedBuyer;
    @SerializedName("request_password_reset")
    @Expose
    private String requestPasswordReset;
    @SerializedName("is_valid_mobile_number")
    @Expose
    private String isValidMobileNumber;
    @SerializedName("mobile_confirmation_code")
    @Expose
    private String mobileConfirmationCode;
    @SerializedName("customer_profile_image")
    @Expose
    private String customerProfileImage;
    @SerializedName("dashboard_configuration")
    @Expose
    private String dashboardConfiguration;
    @SerializedName("language_name")
    @Expose
    private String languageName;
    @SerializedName("currency_name")
    @Expose
    private String currencyName;
    @SerializedName("currency_hex_code")
    @Expose
    private String currencyHexCode;
    @SerializedName("iso_currency_format")
    @Expose
    private String isoCurrencyFormat;
    @SerializedName("dial_code")
    @Expose
    private String dialCode;
    @SerializedName("mobile_phone_id")
    @Expose
    private String mobilePhoneId;
    @SerializedName("tour_completed")
    @Expose
    private String tourCompleted;
    @SerializedName("is_admin")
    @Expose
    private String isAdmin;
    @SerializedName("has_tfa")
    @Expose
    private String hasTfa;
    @SerializedName("is_sidemenu_closed")
    @Expose
    private String isSidemenuClosed;
    @SerializedName("staff_permission_type")
    @Expose
    private String staffPermissionType;
    @SerializedName("is_uk")
    @Expose
    private String isUk;
    @SerializedName("customer_is_managed")
    @Expose
    private String customerIsManaged;
    @SerializedName("agreed_terms")
    @Expose
    private Integer agreedTerms;
    @SerializedName("agreed_share")
    @Expose
    private Integer agreedShare;
    @SerializedName("customer_initials")
    @Expose
    private String customerInitials;
    @SerializedName("customer_type_description")
    @Expose
    private String customerTypeDescription;
    @SerializedName("customer_first_name")
    @Expose
    private String customerFirstName;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("session_id")
    @Expose
    private String sessionId;

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("wrong_attempt")
    @Expose
    private String wrongAttempt;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getWrongAttempt() {
        return wrongAttempt;
    }

    public void setWrongAttempt(String wrongAttempt) {
        this.wrongAttempt = wrongAttempt;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerGender() {
        return customerGender;
    }

    public void setCustomerGender(String customerGender) {
        this.customerGender = customerGender;
    }

    public String getCustomerAgeGroup() {
        return customerAgeGroup;
    }

    public void setCustomerAgeGroup(String customerAgeGroup) {
        this.customerAgeGroup = customerAgeGroup;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerAddress1() {
        return customerAddress1;
    }

    public void setCustomerAddress1(String customerAddress1) {
        this.customerAddress1 = customerAddress1;
    }

    public String getCustomerCountry() {
        return customerCountry;
    }

    public void setCustomerCountry(String customerCountry) {
        this.customerCountry = customerCountry;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getParentCustomerId() {
        return parentCustomerId;
    }

    public void setParentCustomerId(String parentCustomerId) {
        this.parentCustomerId = parentCustomerId;
    }

    public String getAgreedTermsDatetime() {
        return agreedTermsDatetime;
    }

    public void setAgreedTermsDatetime(String agreedTermsDatetime) {
        this.agreedTermsDatetime = agreedTermsDatetime;
    }

    public String getCustomerShareDataDatetime() {
        return customerShareDataDatetime;
    }

    public void setCustomerShareDataDatetime(String customerShareDataDatetime) {
        this.customerShareDataDatetime = customerShareDataDatetime;
    }

    public String getCustomerApiKey() {
        return customerApiKey;
    }

    public void setCustomerApiKey(String customerApiKey) {
        this.customerApiKey = customerApiKey;
    }

    public String getCompletedProperty() {
        return completedProperty;
    }

    public void setCompletedProperty(String completedProperty) {
        this.completedProperty = completedProperty;
    }

    public String getCompletedBuyer() {
        return completedBuyer;
    }

    public void setCompletedBuyer(String completedBuyer) {
        this.completedBuyer = completedBuyer;
    }

    public String getRequestPasswordReset() {
        return requestPasswordReset;
    }

    public void setRequestPasswordReset(String requestPasswordReset) {
        this.requestPasswordReset = requestPasswordReset;
    }

    public String getIsValidMobileNumber() {
        return isValidMobileNumber;
    }

    public void setIsValidMobileNumber(String isValidMobileNumber) {
        this.isValidMobileNumber = isValidMobileNumber;
    }

    public String getMobileConfirmationCode() {
        return mobileConfirmationCode;
    }

    public void setMobileConfirmationCode(String mobileConfirmationCode) {
        this.mobileConfirmationCode = mobileConfirmationCode;
    }

    public String getCustomerProfileImage() {
        return customerProfileImage;
    }

    public void setCustomerProfileImage(String customerProfileImage) {
        this.customerProfileImage = customerProfileImage;
    }

    public String getDashboardConfiguration() {
        return dashboardConfiguration;
    }

    public void setDashboardConfiguration(String dashboardConfiguration) {
        this.dashboardConfiguration = dashboardConfiguration;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencyHexCode() {
        return currencyHexCode;
    }

    public void setCurrencyHexCode(String currencyHexCode) {
        this.currencyHexCode = currencyHexCode;
    }

    public String getIsoCurrencyFormat() {
        return isoCurrencyFormat;
    }

    public void setIsoCurrencyFormat(String isoCurrencyFormat) {
        this.isoCurrencyFormat = isoCurrencyFormat;
    }

    public String getDialCode() {
        return dialCode;
    }

    public void setDialCode(String dialCode) {
        this.dialCode = dialCode;
    }

    public String getMobilePhoneId() {
        return mobilePhoneId;
    }

    public void setMobilePhoneId(String mobilePhoneId) {
        this.mobilePhoneId = mobilePhoneId;
    }

    public String getTourCompleted() {
        return tourCompleted;
    }

    public void setTourCompleted(String tourCompleted) {
        this.tourCompleted = tourCompleted;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getHasTfa() {
        return hasTfa;
    }

    public void setHasTfa(String hasTfa) {
        this.hasTfa = hasTfa;
    }

    public String getIsSidemenuClosed() {
        return isSidemenuClosed;
    }

    public void setIsSidemenuClosed(String isSidemenuClosed) {
        this.isSidemenuClosed = isSidemenuClosed;
    }

    public String getStaffPermissionType() {
        return staffPermissionType;
    }

    public void setStaffPermissionType(String staffPermissionType) {
        this.staffPermissionType = staffPermissionType;
    }

    public String getIsUk() {
        return isUk;
    }

    public void setIsUk(String isUk) {
        this.isUk = isUk;
    }

    public String getCustomerIsManaged() {
        return customerIsManaged;
    }

    public void setCustomerIsManaged(String customerIsManaged) {
        this.customerIsManaged = customerIsManaged;
    }

    public Integer getAgreedTerms() {
        return agreedTerms;
    }

    public void setAgreedTerms(Integer agreedTerms) {
        this.agreedTerms = agreedTerms;
    }

    public Integer getAgreedShare() {
        return agreedShare;
    }

    public void setAgreedShare(Integer agreedShare) {
        this.agreedShare = agreedShare;
    }

    public String getCustomerInitials() {
        return customerInitials;
    }

    public void setCustomerInitials(String customerInitials) {
        this.customerInitials = customerInitials;
    }

    public String getCustomerTypeDescription() {
        return customerTypeDescription;
    }

    public void setCustomerTypeDescription(String customerTypeDescription) {
        this.customerTypeDescription = customerTypeDescription;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

}
