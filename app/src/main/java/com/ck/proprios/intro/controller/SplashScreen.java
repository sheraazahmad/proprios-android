package com.ck.proprios.intro.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.ck.proprios.R;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.helpers.Loader;
import com.ck.proprios.helpers.SharedPrefHelper;
import com.ck.proprios.intro.pojo.DashBoardInitialLoad;
import com.ck.proprios.network.APIClient;
import com.ck.proprios.network.APIInterface;
import com.google.gson.Gson;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {

    APIInterface apiInterface;
    Loader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        apiInterface = APIClient.getAuthClient().create(APIInterface.class);
        loader = new Loader(SplashScreen.this);
        Log.wtf("abc", "" + SharedPrefHelper.getInstance(this).getLoginStatus());
        if (SharedPrefHelper.getInstance(SplashScreen.this).getLoginStatus()) {
            ((GlobalCLass) getApplicationContext()).dashBoardInitial(this);
        } else {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashScreen.this, WelcomeScreen.class);
                    startActivity(intent);
                    finishAffinity();
                }
            }, 1000);
        }
    }

    public void dashBoardInitial() {
        loader.showLoader();
        HashMap<String, String> map = new HashMap<>();
        map.put("request", "dashboard-initial-load");
        map.put("customer_id", "uR765FohTs_NrL0BMWOGvQ==");
        map.put("customer_type", "private-landlord");
        map.put("customer_api_key", "9e52c2ea3e734651");
        map.put("remote_address", "Not allowed by user");
        map.put("user_agent", "Mobile|iPhone 11 Pro|iOS 13.4.1|Safari XX");

        Log.wtf("dashBoardInitial", "Params: " + map);

        Call<DashBoardInitialLoad> call = apiInterface.dashBoardInitialLoad(map);
        call.enqueue(new Callback<DashBoardInitialLoad>() {
            @Override
            public void onResponse(Call<DashBoardInitialLoad> call, Response<DashBoardInitialLoad> response) {
                loader.hideLoader();
                Log.wtf("dashBoardInitial", "response: " + new Gson().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<DashBoardInitialLoad> call, Throwable t) {
                Log.wtf("dashBoardInitial", "error: " + t.getMessage());
            }
        });
    }
}