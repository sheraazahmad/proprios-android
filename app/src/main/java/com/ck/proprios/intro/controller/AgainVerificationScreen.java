package com.ck.proprios.intro.controller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.ck.proprios.R;
import com.ck.proprios.application.GlobalCLass;
import com.google.android.material.textfield.TextInputLayout;

public class AgainVerificationScreen extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout btnBack;
    TextInputLayout edtCode;
    CardView btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.again_verification_screen);

        if (((GlobalCLass) getApplicationContext()).isSlider()) {
            ((GlobalCLass) getApplicationContext()).setSlider(false);
            Animatoo.animateSlideRight(AgainVerificationScreen.this);
        } else {
            Animatoo.animateSlideLeft(AgainVerificationScreen.this);
        }

        btnBack = findViewById(R.id.btnBack);
        btnSubmit = findViewById(R.id.btnSubmit);

        btnBack.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnBack) {
            ((GlobalCLass) getApplicationContext()).setSlider(true);
            Intent intent = new Intent(AgainVerificationScreen.this, VerificationScreen.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ((GlobalCLass) getApplicationContext()).setSlider(true);
        Intent intent = new Intent(AgainVerificationScreen.this, VerificationScreen.class);
        startActivity(intent);
    }
}