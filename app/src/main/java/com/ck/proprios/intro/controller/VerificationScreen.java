package com.ck.proprios.intro.controller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.ck.proprios.R;
import com.ck.proprios.application.GlobalCLass;
import com.google.android.material.textfield.TextInputLayout;

public class VerificationScreen extends AppCompatActivity implements View.OnClickListener {

    TextView mobileNumber, tvNotReceive;
    TextInputLayout edtCode;
    CardView btnSubmit;
    String mobile_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification_screen);

        if (((GlobalCLass) getApplicationContext()).isSlider()) {
            ((GlobalCLass) getApplicationContext()).setSlider(false);
            Animatoo.animateSlideRight(VerificationScreen.this);
        } else {
            Animatoo.animateSlideLeft(VerificationScreen.this);
        }

        mobile_number = getIntent().getStringExtra("mobile_number");

        mobileNumber = findViewById(R.id.mobileNumber);
        tvNotReceive = findViewById(R.id.tvNotReceive);
        edtCode = findViewById(R.id.edtCode);
        btnSubmit = findViewById(R.id.btnSubmit);

        mobileNumber.setText(mobileNumber.getText().toString() + " " + mobile_number);

        btnSubmit.setOnClickListener(this);
        tvNotReceive.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == tvNotReceive) {
            Intent intent = new Intent(VerificationScreen.this, AgainVerificationScreen.class);
            startActivity(intent);
        } else if (v == btnSubmit) {

        }
    }
}