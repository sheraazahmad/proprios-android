package com.ck.proprios.intro.controller;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.ck.proprios.R;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.helpers.Loader;
import com.ck.proprios.intro.pojo.LoginResponse;
import com.ck.proprios.intro.pojo.RegisterResponse;
import com.ck.proprios.network.APIClient;
import com.ck.proprios.network.APIInterface;
import com.google.gson.Gson;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TipsOfferScreen extends AppCompatActivity implements View.OnClickListener {
    String whoYouAre, name, mobile, email, password;
    String customer_share_data = null;
    RelativeLayout btnBack;
    TextView tvYes, tvNo;
    CardView btnContinue;
    APIInterface apiInterface;
    Loader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tips_offer_screen);

        Animatoo.animateSlideLeft(TipsOfferScreen.this);
        apiInterface = APIClient.getAuthClient().create(APIInterface.class);
        loader = new Loader(TipsOfferScreen.this);

        whoYouAre = getIntent().getStringExtra("whoYouAre");
        name = getIntent().getStringExtra("name");
        mobile = getIntent().getStringExtra("mobile");
        email = getIntent().getStringExtra("email");
        password = getIntent().getStringExtra("password");

        btnBack = findViewById(R.id.btnBack);
        tvYes = findViewById(R.id.tvYes);
        tvNo = findViewById(R.id.tvNo);
        btnContinue = findViewById(R.id.btnContinue);

        btnBack.setOnClickListener(this);
        tvYes.setOnClickListener(this);
        tvNo.setOnClickListener(this);
        btnContinue.setOnClickListener(this);

        Log.wtf("Data", "whoYouAre: " + whoYouAre + " name: " + name + " mobile: " + mobile + " email: "
                + email + " password: " + password);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        if (v == btnBack) {
            ((GlobalCLass) getApplicationContext()).setSlider(true);
            onBackPressed();
        } else if (v == btnContinue) {
            if (customer_share_data.length() > 0) {
                registerApi();
            }
        } else if (v == tvYes) {
            tvYes.setTextColor(getResources().getColor(R.color.color5A));
            tvYes.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.color5ALight));
            customer_share_data = "yes";
            tvNo.setTextColor(getResources().getColor(R.color.colorC1));
            tvNo.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorF8));
        } else if (v == tvNo) {
            tvNo.setTextColor(getResources().getColor(R.color.color5A));
            tvNo.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.color5ALight));
            customer_share_data = "no";
            tvYes.setTextColor(getResources().getColor(R.color.colorC1));
            tvYes.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorF8));
        }
    }

    private void registerApi() {
        loader.showLoader();
        HashMap<String, String> map = new HashMap<>();
        map.put("request", "register");
        map.put("customer_type", "private-landlord");
        map.put("customer_name", name);
        map.put("customer_email", email);
        map.put("customer_mobile_number", mobile);
        map.put("customer_password", password);
        map.put("customer_share_data", customer_share_data);
        map.put("mobile_phone_id", "");
        map.put("remote_address", "Not allowed by user");
        map.put("user_agent", "Mobile|iPhone 11 Pro|iOS 13.4.1|Safari XX");

        Log.wtf("registerAPI", "Params: " + map);

        Call<RegisterResponse> call = apiInterface.register(map);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                RegisterResponse registerResponse = response.body();
                loader.hideLoader();
//                if (registerResponse.getError() != null) {
//                    Toast.makeText(TipsOfferScreen.this, "" + registerResponse.getError(), Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(TipsOfferScreen.this, "" + registerResponse.getSuccess(), Toast.LENGTH_SHORT).show();
////                    SharedPrefHelper.getInstance(LoginScreen.this).setLoginStatus(true);
////                    ((GlobalCLass) getApplicationContext()).dashBoardInitial(TipsOfferScreen.this);
//                }
                Toast.makeText(TipsOfferScreen.this, "" + registerResponse.getSuccess(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(TipsOfferScreen.this, VerificationScreen.class);
                intent.putExtra("mobile_number", mobile);
                startActivity(intent);
                Log.wtf("registerApi", "response: " + new Gson().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                loader.hideLoader();
                Log.wtf("registerApi", "error: " + t.getMessage());
            }
        });
    }
}