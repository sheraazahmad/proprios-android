package com.ck.proprios.intro.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DashBoardInitialLoad {
    @SerializedName("property_portfolio")
    @Expose
    private List<PropertyPortfolio> propertyPortfolio = null;
    @SerializedName("dashboard_summary")
    @Expose
    private DashboardSummary dashboardSummary;

    //    public List<DashboardReminders> getDashboardRemindersList() {
//        return dashboardRemindersList;
//    }
//
//    public void setDashboardRemindersList(List<DashboardReminders> dashboardRemindersList) {
//        this.dashboardRemindersList = dashboardRemindersList;
//    }
//
//    @SerializedName("dashboard_reminders")
//    @Expose
//    private List<DashboardReminders> dashboardRemindersList;
    @SerializedName("dashboard_reminders")
    @Expose
    private DashboardReminders dashboardReminders;
    @SerializedName("reminder_when")
    @Expose
    private List<ReminderWhen> reminderWhen = null;
    @SerializedName("landlord_schemes")
    @Expose
    private List<LandlordScheme> landlordSchemes = null;
    @SerializedName("view_profile")
    @Expose
    private ViewProfile viewProfile;
    @SerializedName("user_notifications")
    @Expose
    private UserNotifications userNotifications;
    @SerializedName("country_regions")
    @Expose
    private CountryRegions countryRegions;
    @SerializedName("list_customer_types")
    @Expose
    private List<ListCustomerType> listCustomerTypes = null;

    public List<PropertyPortfolio> getPropertyPortfolio() {
        return propertyPortfolio;
    }

    public void setPropertyPortfolio(List<PropertyPortfolio> propertyPortfolio) {
        this.propertyPortfolio = propertyPortfolio;
    }

    public DashboardSummary getDashboardSummary() {
        return dashboardSummary;
    }

    public void setDashboardSummary(DashboardSummary dashboardSummary) {
        this.dashboardSummary = dashboardSummary;
    }

    public DashboardReminders getDashboardReminders() {
        return dashboardReminders;
    }

    public void setDashboardReminders(DashboardReminders dashboardReminders) {
        this.dashboardReminders = dashboardReminders;
    }

    public List<ReminderWhen> getReminderWhen() {
        return reminderWhen;
    }

    public void setReminderWhen(List<ReminderWhen> reminderWhen) {
        this.reminderWhen = reminderWhen;
    }

    public List<LandlordScheme> getLandlordSchemes() {
        return landlordSchemes;
    }

    public void setLandlordSchemes(List<LandlordScheme> landlordSchemes) {
        this.landlordSchemes = landlordSchemes;
    }

    public ViewProfile getViewProfile() {
        return viewProfile;
    }

    public void setViewProfile(ViewProfile viewProfile) {
        this.viewProfile = viewProfile;
    }

    public UserNotifications getUserNotifications() {
        return userNotifications;
    }

    public void setUserNotifications(UserNotifications userNotifications) {
        this.userNotifications = userNotifications;
    }

    public CountryRegions getCountryRegions() {
        return countryRegions;
    }

    public void setCountryRegions(CountryRegions countryRegions) {
        this.countryRegions = countryRegions;
    }

    public List<ListCustomerType> getListCustomerTypes() {
        return listCustomerTypes;
    }

    public void setListCustomerTypes(List<ListCustomerType> listCustomerTypes) {
        this.listCustomerTypes = listCustomerTypes;
    }

    public class PropertyPortfolio {

        @SerializedName("property_id")
        @Expose
        private String propertyId;
        @SerializedName("decoded_property_id")
        @Expose
        private String decodedPropertyId;
        @SerializedName("property_address1")
        @Expose
        private String propertyAddress1;
        @SerializedName("property_postcode")
        @Expose
        private String propertyPostcode;

        public String getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(String propertyId) {
            this.propertyId = propertyId;
        }

        public String getDecodedPropertyId() {
            return decodedPropertyId;
        }

        public void setDecodedPropertyId(String decodedPropertyId) {
            this.decodedPropertyId = decodedPropertyId;
        }

        public String getPropertyAddress1() {
            return propertyAddress1;
        }

        public void setPropertyAddress1(String propertyAddress1) {
            this.propertyAddress1 = propertyAddress1;
        }

        public String getPropertyPostcode() {
            return propertyPostcode;
        }

        public void setPropertyPostcode(String propertyPostcode) {
            this.propertyPostcode = propertyPostcode;
        }

    }

    public class DashboardSummary {

        @SerializedName("count_properties")
        @Expose
        private String countProperties;
        @SerializedName("count_occupied")
        @Expose
        private String countOccupied;
        @SerializedName("count_unoccupied")
        @Expose
        private String countUnoccupied;

        public String getCountProperties() {
            return countProperties;
        }

        public void setCountProperties(String countProperties) {
            this.countProperties = countProperties;
        }

        public String getCountOccupied() {
            return countOccupied;
        }

        public void setCountOccupied(String countOccupied) {
            this.countOccupied = countOccupied;
        }

        public String getCountUnoccupied() {
            return countUnoccupied;
        }

        public void setCountUnoccupied(String countUnoccupied) {
            this.countUnoccupied = countUnoccupied;
        }

    }

    public class DashboardReminders {

        //        @SerializedName("error")
//        @Expose
//        private String error;
//
//        public String getError() {
//            return error;
//        }
//
//        public void setError(String error) {
//            this.error = error;
//        }
        @SerializedName("reminder_id")
        @Expose
        private String reminderId;
        @SerializedName("decoded_reminder_id")
        @Expose
        private String decodedReminderId;
        @SerializedName("reminder_name")
        @Expose
        private String reminderName;
        @SerializedName("reminder_datetime")
        @Expose
        private String reminderDatetime;
        @SerializedName("reminder_date_status")
        @Expose
        private String reminderDateStatus;
        @SerializedName("reminder_date")
        @Expose
        private String reminderDate;
        @SerializedName("reminder_date_short")
        @Expose
        private String reminderDateShort;
        @SerializedName("reminder_time")
        @Expose
        private String reminderTime;
        @SerializedName("date_created")
        @Expose
        private String dateCreated;
        @SerializedName("property_details")
        @Expose
        private String propertyDetails;

        public String getReminderId() {
            return reminderId;
        }

        public void setReminderId(String reminderId) {
            this.reminderId = reminderId;
        }

        public String getDecodedReminderId() {
            return decodedReminderId;
        }

        public void setDecodedReminderId(String decodedReminderId) {
            this.decodedReminderId = decodedReminderId;
        }

        public String getReminderName() {
            return reminderName;
        }

        public void setReminderName(String reminderName) {
            this.reminderName = reminderName;
        }

        public String getReminderDatetime() {
            return reminderDatetime;
        }

        public void setReminderDatetime(String reminderDatetime) {
            this.reminderDatetime = reminderDatetime;
        }

        public String getReminderDateStatus() {
            return reminderDateStatus;
        }

        public void setReminderDateStatus(String reminderDateStatus) {
            this.reminderDateStatus = reminderDateStatus;
        }

        public String getReminderDate() {
            return reminderDate;
        }

        public void setReminderDate(String reminderDate) {
            this.reminderDate = reminderDate;
        }

        public String getReminderDateShort() {
            return reminderDateShort;
        }

        public void setReminderDateShort(String reminderDateShort) {
            this.reminderDateShort = reminderDateShort;
        }

        public String getReminderTime() {
            return reminderTime;
        }

        public void setReminderTime(String reminderTime) {
            this.reminderTime = reminderTime;
        }

        public String getDateCreated() {
            return dateCreated;
        }

        public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
        }

        public String getPropertyDetails() {
            return propertyDetails;
        }

        public void setPropertyDetails(String propertyDetails) {
            this.propertyDetails = propertyDetails;
        }


    }

    public class ReminderWhen {

        @SerializedName("option_key")
        @Expose
        private String optionKey;
        @SerializedName("option_value")
        @Expose
        private String optionValue;

        public String getOptionKey() {
            return optionKey;
        }

        public void setOptionKey(String optionKey) {
            this.optionKey = optionKey;
        }

        public String getOptionValue() {
            return optionValue;
        }

        public void setOptionValue(String optionValue) {
            this.optionValue = optionValue;
        }

    }

    public class LandlordScheme {

        @SerializedName("option_key")
        @Expose
        private String optionKey;
        @SerializedName("option_value")
        @Expose
        private String optionValue;

        public String getOptionKey() {
            return optionKey;
        }

        public void setOptionKey(String optionKey) {
            this.optionKey = optionKey;
        }

        public String getOptionValue() {
            return optionValue;
        }

        public void setOptionValue(String optionValue) {
            this.optionValue = optionValue;
        }

    }

    public class ViewProfile {

        @SerializedName("customer_id")
        @Expose
        private String customerId;
        @SerializedName("customer_company")
        @Expose
        private String customerCompany;
        @SerializedName("customer_title")
        @Expose
        private String customerTitle;
        @SerializedName("customer_name")
        @Expose
        private String customerName;
        @SerializedName("customer_email")
        @Expose
        private String customerEmail;
        @SerializedName("customer_password")
        @Expose
        private String customerPassword;
        @SerializedName("customer_address1")
        @Expose
        private String customerAddress1;
        @SerializedName("customer_address2")
        @Expose
        private String customerAddress2;
        @SerializedName("customer_town")
        @Expose
        private String customerTown;
        @SerializedName("customer_county")
        @Expose
        private String customerCounty;
        @SerializedName("customer_postcode")
        @Expose
        private String customerPostcode;
        @SerializedName("customer_country")
        @Expose
        private String customerCountry;
        @SerializedName("customer_telephone")
        @Expose
        private String customerTelephone;
        @SerializedName("customer_mobile_number")
        @Expose
        private String customerMobileNumber;
        @SerializedName("customer_website")
        @Expose
        private String customerWebsite;
        @SerializedName("company_logo")
        @Expose
        private String companyLogo;
        @SerializedName("customer_company_type")
        @Expose
        private String customerCompanyType;
        @SerializedName("is_registered_scheme")
        @Expose
        private String isRegisteredScheme;
        @SerializedName("registered_scheme_name")
        @Expose
        private String registeredSchemeName;
        @SerializedName("customer_account_name")
        @Expose
        private String customerAccountName;
        @SerializedName("customer_bank_sort_code")
        @Expose
        private String customerBankSortCode;
        @SerializedName("customer_bank_account_number")
        @Expose
        private String customerBankAccountNumber;
        @SerializedName("customer_register_date")
        @Expose
        private String customerRegisterDate;
        @SerializedName("customer_last_login")
        @Expose
        private String customerLastLogin;
        @SerializedName("customer_amount_of_months_free")
        @Expose
        private String customerAmountOfMonthsFree;
        @SerializedName("customer_free_for_life")
        @Expose
        private String customerFreeForLife;
        @SerializedName("customer_tenant_list_free_months")
        @Expose
        private String customerTenantListFreeMonths;
        @SerializedName("agreed_terms_datetime")
        @Expose
        private String agreedTermsDatetime;
        @SerializedName("customer_share_data")
        @Expose
        private String customerShareData;
        @SerializedName("customer_share_data_datetime")
        @Expose
        private String customerShareDataDatetime;
        @SerializedName("customer_share_data_remote_address")
        @Expose
        private String customerShareDataRemoteAddress;
        @SerializedName("is_valid_mobile_number")
        @Expose
        private String isValidMobileNumber;
        @SerializedName("mobile_confirmation_code")
        @Expose
        private String mobileConfirmationCode;
        @SerializedName("customer_profile_image")
        @Expose
        private String customerProfileImage;
        @SerializedName("dashboard_configuration")
        @Expose
        private String dashboardConfiguration;
        @SerializedName("language_name")
        @Expose
        private String languageName;
        @SerializedName("currency_name")
        @Expose
        private String currencyName;
        @SerializedName("currency_hex_code")
        @Expose
        private String currencyHexCode;
        @SerializedName("iso_currency_format")
        @Expose
        private String isoCurrencyFormat;
        @SerializedName("dial_code")
        @Expose
        private String dialCode;
        @SerializedName("tour_completed")
        @Expose
        private String tourCompleted;
        @SerializedName("staff_permission_type")
        @Expose
        private String staffPermissionType;
        @SerializedName("selected_schemes")
        @Expose
        private List<String> selectedSchemes = null;
        @SerializedName("agreed_terms")
        @Expose
        private Integer agreedTerms;
        @SerializedName("agreed_share")
        @Expose
        private Integer agreedShare;
        @SerializedName("company_logo_path")
        @Expose
        private String companyLogoPath;
        @SerializedName("customer_initials")
        @Expose
        private String customerInitials;
        @SerializedName("customer_first_name")
        @Expose
        private String customerFirstName;

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getCustomerCompany() {
            return customerCompany;
        }

        public void setCustomerCompany(String customerCompany) {
            this.customerCompany = customerCompany;
        }

        public String getCustomerTitle() {
            return customerTitle;
        }

        public void setCustomerTitle(String customerTitle) {
            this.customerTitle = customerTitle;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getCustomerEmail() {
            return customerEmail;
        }

        public void setCustomerEmail(String customerEmail) {
            this.customerEmail = customerEmail;
        }

        public String getCustomerPassword() {
            return customerPassword;
        }

        public void setCustomerPassword(String customerPassword) {
            this.customerPassword = customerPassword;
        }

        public String getCustomerAddress1() {
            return customerAddress1;
        }

        public void setCustomerAddress1(String customerAddress1) {
            this.customerAddress1 = customerAddress1;
        }

        public String getCustomerAddress2() {
            return customerAddress2;
        }

        public void setCustomerAddress2(String customerAddress2) {
            this.customerAddress2 = customerAddress2;
        }

        public String getCustomerTown() {
            return customerTown;
        }

        public void setCustomerTown(String customerTown) {
            this.customerTown = customerTown;
        }

        public String getCustomerCounty() {
            return customerCounty;
        }

        public void setCustomerCounty(String customerCounty) {
            this.customerCounty = customerCounty;
        }

        public String getCustomerPostcode() {
            return customerPostcode;
        }

        public void setCustomerPostcode(String customerPostcode) {
            this.customerPostcode = customerPostcode;
        }

        public String getCustomerCountry() {
            return customerCountry;
        }

        public void setCustomerCountry(String customerCountry) {
            this.customerCountry = customerCountry;
        }

        public String getCustomerTelephone() {
            return customerTelephone;
        }

        public void setCustomerTelephone(String customerTelephone) {
            this.customerTelephone = customerTelephone;
        }

        public String getCustomerMobileNumber() {
            return customerMobileNumber;
        }

        public void setCustomerMobileNumber(String customerMobileNumber) {
            this.customerMobileNumber = customerMobileNumber;
        }

        public String getCustomerWebsite() {
            return customerWebsite;
        }

        public void setCustomerWebsite(String customerWebsite) {
            this.customerWebsite = customerWebsite;
        }

        public String getCompanyLogo() {
            return companyLogo;
        }

        public void setCompanyLogo(String companyLogo) {
            this.companyLogo = companyLogo;
        }

        public String getCustomerCompanyType() {
            return customerCompanyType;
        }

        public void setCustomerCompanyType(String customerCompanyType) {
            this.customerCompanyType = customerCompanyType;
        }

        public String getIsRegisteredScheme() {
            return isRegisteredScheme;
        }

        public void setIsRegisteredScheme(String isRegisteredScheme) {
            this.isRegisteredScheme = isRegisteredScheme;
        }

        public String getRegisteredSchemeName() {
            return registeredSchemeName;
        }

        public void setRegisteredSchemeName(String registeredSchemeName) {
            this.registeredSchemeName = registeredSchemeName;
        }

        public String getCustomerAccountName() {
            return customerAccountName;
        }

        public void setCustomerAccountName(String customerAccountName) {
            this.customerAccountName = customerAccountName;
        }

        public String getCustomerBankSortCode() {
            return customerBankSortCode;
        }

        public void setCustomerBankSortCode(String customerBankSortCode) {
            this.customerBankSortCode = customerBankSortCode;
        }

        public String getCustomerBankAccountNumber() {
            return customerBankAccountNumber;
        }

        public void setCustomerBankAccountNumber(String customerBankAccountNumber) {
            this.customerBankAccountNumber = customerBankAccountNumber;
        }

        public String getCustomerRegisterDate() {
            return customerRegisterDate;
        }

        public void setCustomerRegisterDate(String customerRegisterDate) {
            this.customerRegisterDate = customerRegisterDate;
        }

        public String getCustomerLastLogin() {
            return customerLastLogin;
        }

        public void setCustomerLastLogin(String customerLastLogin) {
            this.customerLastLogin = customerLastLogin;
        }

        public String getCustomerAmountOfMonthsFree() {
            return customerAmountOfMonthsFree;
        }

        public void setCustomerAmountOfMonthsFree(String customerAmountOfMonthsFree) {
            this.customerAmountOfMonthsFree = customerAmountOfMonthsFree;
        }

        public String getCustomerFreeForLife() {
            return customerFreeForLife;
        }

        public void setCustomerFreeForLife(String customerFreeForLife) {
            this.customerFreeForLife = customerFreeForLife;
        }

        public String getCustomerTenantListFreeMonths() {
            return customerTenantListFreeMonths;
        }

        public void setCustomerTenantListFreeMonths(String customerTenantListFreeMonths) {
            this.customerTenantListFreeMonths = customerTenantListFreeMonths;
        }

        public String getAgreedTermsDatetime() {
            return agreedTermsDatetime;
        }

        public void setAgreedTermsDatetime(String agreedTermsDatetime) {
            this.agreedTermsDatetime = agreedTermsDatetime;
        }

        public String getCustomerShareData() {
            return customerShareData;
        }

        public void setCustomerShareData(String customerShareData) {
            this.customerShareData = customerShareData;
        }

        public String getCustomerShareDataDatetime() {
            return customerShareDataDatetime;
        }

        public void setCustomerShareDataDatetime(String customerShareDataDatetime) {
            this.customerShareDataDatetime = customerShareDataDatetime;
        }

        public String getCustomerShareDataRemoteAddress() {
            return customerShareDataRemoteAddress;
        }

        public void setCustomerShareDataRemoteAddress(String customerShareDataRemoteAddress) {
            this.customerShareDataRemoteAddress = customerShareDataRemoteAddress;
        }

        public String getIsValidMobileNumber() {
            return isValidMobileNumber;
        }

        public void setIsValidMobileNumber(String isValidMobileNumber) {
            this.isValidMobileNumber = isValidMobileNumber;
        }

        public String getMobileConfirmationCode() {
            return mobileConfirmationCode;
        }

        public void setMobileConfirmationCode(String mobileConfirmationCode) {
            this.mobileConfirmationCode = mobileConfirmationCode;
        }

        public String getCustomerProfileImage() {
            return customerProfileImage;
        }

        public void setCustomerProfileImage(String customerProfileImage) {
            this.customerProfileImage = customerProfileImage;
        }

        public String getDashboardConfiguration() {
            return dashboardConfiguration;
        }

        public void setDashboardConfiguration(String dashboardConfiguration) {
            this.dashboardConfiguration = dashboardConfiguration;
        }

        public String getLanguageName() {
            return languageName;
        }

        public void setLanguageName(String languageName) {
            this.languageName = languageName;
        }

        public String getCurrencyName() {
            return currencyName;
        }

        public void setCurrencyName(String currencyName) {
            this.currencyName = currencyName;
        }

        public String getCurrencyHexCode() {
            return currencyHexCode;
        }

        public void setCurrencyHexCode(String currencyHexCode) {
            this.currencyHexCode = currencyHexCode;
        }

        public String getIsoCurrencyFormat() {
            return isoCurrencyFormat;
        }

        public void setIsoCurrencyFormat(String isoCurrencyFormat) {
            this.isoCurrencyFormat = isoCurrencyFormat;
        }

        public String getDialCode() {
            return dialCode;
        }

        public void setDialCode(String dialCode) {
            this.dialCode = dialCode;
        }

        public String getTourCompleted() {
            return tourCompleted;
        }

        public void setTourCompleted(String tourCompleted) {
            this.tourCompleted = tourCompleted;
        }

        public String getStaffPermissionType() {
            return staffPermissionType;
        }

        public void setStaffPermissionType(String staffPermissionType) {
            this.staffPermissionType = staffPermissionType;
        }

        public List<String> getSelectedSchemes() {
            return selectedSchemes;
        }

        public void setSelectedSchemes(List<String> selectedSchemes) {
            this.selectedSchemes = selectedSchemes;
        }

        public Integer getAgreedTerms() {
            return agreedTerms;
        }

        public void setAgreedTerms(Integer agreedTerms) {
            this.agreedTerms = agreedTerms;
        }

        public Integer getAgreedShare() {
            return agreedShare;
        }

        public void setAgreedShare(Integer agreedShare) {
            this.agreedShare = agreedShare;
        }

        public String getCompanyLogoPath() {
            return companyLogoPath;
        }

        public void setCompanyLogoPath(String companyLogoPath) {
            this.companyLogoPath = companyLogoPath;
        }

        public String getCustomerInitials() {
            return customerInitials;
        }

        public void setCustomerInitials(String customerInitials) {
            this.customerInitials = customerInitials;
        }

        public String getCustomerFirstName() {
            return customerFirstName;
        }

        public void setCustomerFirstName(String customerFirstName) {
            this.customerFirstName = customerFirstName;
        }

    }

    public class UserNotifications {

        @SerializedName("notifications")
        @Expose
        private List<Notification> notifications = null;
        @SerializedName("unread_notifications")
        @Expose
        private Integer unreadNotifications;

        public List<Notification> getNotifications() {
            return notifications;
        }

        public void setNotifications(List<Notification> notifications) {
            this.notifications = notifications;
        }

        public Integer getUnreadNotifications() {
            return unreadNotifications;
        }

        public void setUnreadNotifications(Integer unreadNotifications) {
            this.unreadNotifications = unreadNotifications;
        }

    }

    public class Notification {

        @SerializedName("notification_id")
        @Expose
        private String notificationId;
        @SerializedName("customer_id")
        @Expose
        private String customerId;
        @SerializedName("customer_type")
        @Expose
        private String customerType;
        @SerializedName("customer_role")
        @Expose
        private String customerRole;
        @SerializedName("main_id")
        @Expose
        private String mainId;
        @SerializedName("sub_id")
        @Expose
        private String subId;
        @SerializedName("landing_url")
        @Expose
        private String landingUrl;
        @SerializedName("action_type")
        @Expose
        private Object actionType;
        @SerializedName("icon_name")
        @Expose
        private String iconName;
        @SerializedName("notification_content")
        @Expose
        private String notificationContent;
        @SerializedName("notification_read")
        @Expose
        private String notificationRead;
        @SerializedName("is_notified")
        @Expose
        private String isNotified;
        @SerializedName("date_created")
        @Expose
        private String dateCreated;

        public String getNotificationId() {
            return notificationId;
        }

        public void setNotificationId(String notificationId) {
            this.notificationId = notificationId;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getCustomerType() {
            return customerType;
        }

        public void setCustomerType(String customerType) {
            this.customerType = customerType;
        }

        public String getCustomerRole() {
            return customerRole;
        }

        public void setCustomerRole(String customerRole) {
            this.customerRole = customerRole;
        }

        public String getMainId() {
            return mainId;
        }

        public void setMainId(String mainId) {
            this.mainId = mainId;
        }

        public String getSubId() {
            return subId;
        }

        public void setSubId(String subId) {
            this.subId = subId;
        }

        public String getLandingUrl() {
            return landingUrl;
        }

        public void setLandingUrl(String landingUrl) {
            this.landingUrl = landingUrl;
        }

        public Object getActionType() {
            return actionType;
        }

        public void setActionType(Object actionType) {
            this.actionType = actionType;
        }

        public String getIconName() {
            return iconName;
        }

        public void setIconName(String iconName) {
            this.iconName = iconName;
        }

        public String getNotificationContent() {
            return notificationContent;
        }

        public void setNotificationContent(String notificationContent) {
            this.notificationContent = notificationContent;
        }

        public String getNotificationRead() {
            return notificationRead;
        }

        public void setNotificationRead(String notificationRead) {
            this.notificationRead = notificationRead;
        }

        public String getIsNotified() {
            return isNotified;
        }

        public void setIsNotified(String isNotified) {
            this.isNotified = isNotified;
        }

        public String getDateCreated() {
            return dateCreated;
        }

        public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
        }

    }

    public class CountryRegions {

        @SerializedName("England")
        @Expose
        private List<England> england = null;
        @SerializedName("Scotland")
        @Expose
        private List<Scotland> scotland = null;
        @SerializedName("Northern Ireland")
        @Expose
        private List<NorthernIreland> northernIreland = null;
        @SerializedName("Wales")
        @Expose
        private List<Wale> wales = null;
        @SerializedName("UK Offshore Dependencies")
        @Expose
        private List<UKOffshoreDependency> uKOffshoreDependencies = null;

        public List<England> getEngland() {
            return england;
        }

        public void setEngland(List<England> england) {
            this.england = england;
        }

        public List<Scotland> getScotland() {
            return scotland;
        }

        public void setScotland(List<Scotland> scotland) {
            this.scotland = scotland;
        }

        public List<NorthernIreland> getNorthernIreland() {
            return northernIreland;
        }

        public void setNorthernIreland(List<NorthernIreland> northernIreland) {
            this.northernIreland = northernIreland;
        }

        public List<Wale> getWales() {
            return wales;
        }

        public void setWales(List<Wale> wales) {
            this.wales = wales;
        }

        public List<UKOffshoreDependency> getUKOffshoreDependencies() {
            return uKOffshoreDependencies;
        }

        public void setUKOffshoreDependencies(List<UKOffshoreDependency> uKOffshoreDependencies) {
            this.uKOffshoreDependencies = uKOffshoreDependencies;
        }

    }

    public class England {

        @SerializedName("region_name")
        @Expose
        private String regionName;

        public String getRegionName() {
            return regionName;
        }

        public void setRegionName(String regionName) {
            this.regionName = regionName;
        }

    }

    public class Scotland {

        @SerializedName("region_name")
        @Expose
        private String regionName;

        public String getRegionName() {
            return regionName;
        }

        public void setRegionName(String regionName) {
            this.regionName = regionName;
        }

    }

    public class NorthernIreland {

        @SerializedName("region_name")
        @Expose
        private String regionName;

        public String getRegionName() {
            return regionName;
        }

        public void setRegionName(String regionName) {
            this.regionName = regionName;
        }

    }

    public class Wale {

        @SerializedName("region_name")
        @Expose
        private String regionName;

        public String getRegionName() {
            return regionName;
        }

        public void setRegionName(String regionName) {
            this.regionName = regionName;
        }

    }

    public class UKOffshoreDependency {

        @SerializedName("region_name")
        @Expose
        private String regionName;

        public String getRegionName() {
            return regionName;
        }

        public void setRegionName(String regionName) {
            this.regionName = regionName;
        }

    }

    public class ListCustomerType {

        @SerializedName("customer_type_key")
        @Expose
        private String customerTypeKey;
        @SerializedName("customer_type_value")
        @Expose
        private String customerTypeValue;

        public String getCustomerTypeKey() {
            return customerTypeKey;
        }

        public void setCustomerTypeKey(String customerTypeKey) {
            this.customerTypeKey = customerTypeKey;
        }

        public String getCustomerTypeValue() {
            return customerTypeValue;
        }

        public void setCustomerTypeValue(String customerTypeValue) {
            this.customerTypeValue = customerTypeValue;
        }

    }
}
