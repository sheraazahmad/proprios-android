package com.ck.proprios.intro.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.ck.proprios.R;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.dashboard.controller.DashBoardScreen;
import com.ck.proprios.helpers.SharedPrefHelper;

import java.util.ArrayList;
import java.util.List;

public class PassCodeScreen extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout[] digits = new RelativeLayout[12];
    LinearLayout[] circles = new LinearLayout[5];
    int count = 0;
    int attempt = 0;
    String passCode = "";
    String passCodeReType = "";
    int[] code = new int[5];
    List<Integer> codeList = new ArrayList<>();
    TextView tvHeading, tvForget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pass_code_screen);
        codeList.clear();
        if (((GlobalCLass) getApplicationContext()).isSlider()) {
            ((GlobalCLass) getApplicationContext()).setSlider(false);
            Animatoo.animateSlideLeft(PassCodeScreen.this);
        } else {
        }

        tvHeading = findViewById(R.id.tvHeading);
        tvForget = findViewById(R.id.tvForget);
        findView();
        onClickFun();

        if (SharedPrefHelper.getInstance(PassCodeScreen.this).getLoginStatus()) {
//            digits[10].setVisibility(View.VISIBLE);
            digits[10].setEnabled(true);
            tvForget.setTextColor(getResources().getColor(R.color.color26));
            tvHeading.setText("Enter your 5-digit paascode to log in");
        } else {
            digits[10].setEnabled(false);
            tvForget.setTextColor(getResources().getColor(R.color.colorDE));
//            digits[10].setVisibility(View.INVISIBLE);
        }
    }

    private void onClickFun() {
        digits[0].setOnClickListener(this);
        digits[1].setOnClickListener(this);
        digits[2].setOnClickListener(this);
        digits[3].setOnClickListener(this);
        digits[4].setOnClickListener(this);
        digits[5].setOnClickListener(this);
        digits[6].setOnClickListener(this);
        digits[7].setOnClickListener(this);
        digits[8].setOnClickListener(this);
        digits[9].setOnClickListener(this);
        digits[10].setOnClickListener(this);
        digits[11].setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == digits[0]) {
            insertValue(0);
        } else if (v == digits[1]) {
            insertValue(1);
        } else if (v == digits[2]) {
            insertValue(2);
        } else if (v == digits[3]) {
            insertValue(3);
        } else if (v == digits[4]) {
            insertValue(4);
        } else if (v == digits[5]) {
            insertValue(5);
        } else if (v == digits[6]) {
            insertValue(6);
        } else if (v == digits[7]) {
            insertValue(7);
        } else if (v == digits[8]) {
            insertValue(8);
        } else if (v == digits[9]) {
            insertValue(9);
        } else if (v == digits[10]) {
            forgetClick();
        } else if (v == digits[11]) {
            backBtn();
        }
    }

    private void forgetClick() {
        SharedPrefHelper.getInstance(PassCodeScreen.this).logOut();
        ((GlobalCLass) getApplicationContext()).setSlider(true);
        Intent intent = new Intent(PassCodeScreen.this, LoginScreen.class);
        startActivity(intent);
    }

    private void backBtn() {
        if (count > 0) {
            codeList.remove(count - 1);
            circles[count - 1].setBackground(getResources().getDrawable(R.drawable.passcode_circle));
            count--;
        }
    }

    private void insertValue(int val) {
        if (count < 5) {
            codeList.add(val);
            code[count] = val;
            circles[count].setBackground(getResources().getDrawable(R.drawable.ic_pass_code_circle_fill));
            count++;
//            Toast.makeText(this, "Count = " + count, Toast.LENGTH_SHORT).show();
        }
        if (count == 5) {
//            Toast.makeText(this, "CodeList" + codeList.size(), Toast.LENGTH_SHORT).show();
            if (SharedPrefHelper.getInstance(PassCodeScreen.this).getLoginStatus()) {
                Log.wtf("attempt", "" + attempt);
                if (attempt == 2) {
                    SharedPrefHelper.getInstance(PassCodeScreen.this).logOut();
                    ((GlobalCLass) getApplicationContext()).setSlider(true);
                    Intent intent = new Intent(PassCodeScreen.this, LoginScreen.class);
                    startActivity(intent);
                } else {
                    passCode = "";
                    for (int i = 0; i < codeList.size(); i++) {
                        passCode = passCode + codeList.get(i);
                    }
                    if (passCode.matches(SharedPrefHelper.getInstance(PassCodeScreen.this).getPassCodeValue())) {
                        Intent intent = new Intent(PassCodeScreen.this, DashBoardScreen.class);
                        startActivity(intent);
                        finishAffinity();
                    } else {
                        attempt++;
                        Toast.makeText(this, "Wrong Password", Toast.LENGTH_SHORT).show();
                        for (int i = 0; i < codeList.size(); i++) {
                            circles[i].setBackground(getResources().getDrawable(R.drawable.passcode_circle));
                        }
//                        tvHeading.setText("Please Re-Type Password");
                        codeList.clear();
                        count = 0;
                    }
                }
            } else {
                if (attempt > 0) {
                    passCodeReType = "";
                    for (int i = 0; i < codeList.size(); i++) {
                        passCodeReType = passCodeReType + codeList.get(i);
                    }
                    Log.wtf("password1", passCodeReType + " size: " + codeList.size());
                    if (passCode.matches(passCodeReType)) {
                        SharedPrefHelper.getInstance(PassCodeScreen.this).setLoginStatus(true);
                        String customerType = ((GlobalCLass) getApplicationContext()).
                                getLoginResponse().getCustomerTypeDescription();
                        SharedPrefHelper.getInstance(PassCodeScreen.this).setCustomerType(customerType);
                        SharedPrefHelper.getInstance(PassCodeScreen.this).setPassCodeValue(passCode);
                        Intent intent = new Intent(PassCodeScreen.this, DashBoardScreen.class);
                        startActivity(intent);
                        finishAffinity();
                    } else {
                        Toast.makeText(this, "Password Mismatch", Toast.LENGTH_SHORT).show();
                        for (int i = 0; i < codeList.size(); i++) {
                            circles[i].setBackground(getResources().getDrawable(R.drawable.passcode_circle));
                        }
//                        tvHeading.setText("Please Re-Type Password");
                        codeList.clear();
                        count = 0;
                    }
                } else {
                    attempt++;
                    for (int i = 0; i < codeList.size(); i++) {
//                    passCode = passCode + code[i];
                        passCode = passCode + codeList.get(i);
                        circles[i].setBackground(getResources().getDrawable(R.drawable.passcode_circle));
                        count = 0;
                    }
                    tvHeading.setText("Please Re-Type Password");
                    codeList.clear();
                    Toast.makeText(this, "New Code" + passCode, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void findView() {
        digits[0] = findViewById(R.id.btnDigitZero);
        digits[1] = findViewById(R.id.btnDigitOne);
        digits[2] = findViewById(R.id.btnDigitTwo);
        digits[3] = findViewById(R.id.btnDigitThree);
        digits[4] = findViewById(R.id.btnDigitFour);
        digits[5] = findViewById(R.id.btnDigitFive);
        digits[6] = findViewById(R.id.btnDigitSix);
        digits[7] = findViewById(R.id.btnDigitSeven);
        digits[8] = findViewById(R.id.btnDigitEight);
        digits[9] = findViewById(R.id.btnDigitNine);
        digits[10] = findViewById(R.id.btnForgot);
        digits[11] = findViewById(R.id.btnBack);

        circles[0] = findViewById(R.id.passCodeOne);
        circles[1] = findViewById(R.id.passCodeTwo);
        circles[2] = findViewById(R.id.passCodeThree);
        circles[3] = findViewById(R.id.passCodeFour);
        circles[4] = findViewById(R.id.passCodeFive);
    }
}