package com.ck.proprios.intro.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("session_id")
    @Expose
    private String sessionId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_type")
    @Expose
    private String customerType;
    @SerializedName("customer_type_description")
    @Expose
    private String customerTypeDescription;
    @SerializedName("agreed_terms")
    @Expose
    private Integer agreedTerms;
    @SerializedName("agreed_share")
    @Expose
    private Integer agreedShare;
    @SerializedName("customer_api_key")
    @Expose
    private String customerApiKey;
    @SerializedName("customer_mobile_number")
    @Expose
    private String customerMobileNumber;
    @SerializedName("is_valid_mobile_number")
    @Expose
    private String isValidMobileNumber;
    @SerializedName("mobile_confirmation_code")
    @Expose
    private String mobileConfirmationCode;
    @SerializedName("customer_initials")
    @Expose
    private String customerInitials;
    @SerializedName("customer_first_name")
    @Expose
    private String customerFirstName;
    @SerializedName("customer_is_managed")
    @Expose
    private String customerIsManaged;
    @SerializedName("is_admin")
    @Expose
    private String isAdmin;
    @SerializedName("customer_country")
    @Expose
    private String customerCountry;
    @SerializedName("language_name")
    @Expose
    private String languageName;
    @SerializedName("currency_name")
    @Expose
    private String currencyName;
    @SerializedName("currency_hex_code")
    @Expose
    private String currencyHexCode;
    @SerializedName("iso_currency_format")
    @Expose
    private String isoCurrencyFormat;
    @SerializedName("dial_code")
    @Expose
    private String dialCode;
    @SerializedName("is_uk")
    @Expose
    private String isUk;
    @SerializedName("completed_property")
    @Expose
    private Integer completedProperty;
    @SerializedName("completed_buyer")
    @Expose
    private Integer completedBuyer;
    @SerializedName("tour_completed")
    @Expose
    private Integer tourCompleted;
    @SerializedName("has_tfa")
    @Expose
    private Integer hasTfa;
    @SerializedName("is_sidemenu_closed")
    @Expose
    private Integer isSidemenuClosed;
    @SerializedName("customer_gender")
    @Expose
    private String customerGender;
    @SerializedName("customer_age_group")
    @Expose
    private String customerAgeGroup;

//    @SerializedName("error")
//    @Expose
//    private Error error;
//
//    public Error getError() {
//        return error;
//    }
//
//    public void setError(Error error) {
//        this.error = error;
//    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerTypeDescription() {
        return customerTypeDescription;
    }

    public void setCustomerTypeDescription(String customerTypeDescription) {
        this.customerTypeDescription = customerTypeDescription;
    }

    public Integer getAgreedTerms() {
        return agreedTerms;
    }

    public void setAgreedTerms(Integer agreedTerms) {
        this.agreedTerms = agreedTerms;
    }

    public Integer getAgreedShare() {
        return agreedShare;
    }

    public void setAgreedShare(Integer agreedShare) {
        this.agreedShare = agreedShare;
    }

    public String getCustomerApiKey() {
        return customerApiKey;
    }

    public void setCustomerApiKey(String customerApiKey) {
        this.customerApiKey = customerApiKey;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getIsValidMobileNumber() {
        return isValidMobileNumber;
    }

    public void setIsValidMobileNumber(String isValidMobileNumber) {
        this.isValidMobileNumber = isValidMobileNumber;
    }

    public String getMobileConfirmationCode() {
        return mobileConfirmationCode;
    }

    public void setMobileConfirmationCode(String mobileConfirmationCode) {
        this.mobileConfirmationCode = mobileConfirmationCode;
    }

    public String getCustomerInitials() {
        return customerInitials;
    }

    public void setCustomerInitials(String customerInitials) {
        this.customerInitials = customerInitials;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerIsManaged() {
        return customerIsManaged;
    }

    public void setCustomerIsManaged(String customerIsManaged) {
        this.customerIsManaged = customerIsManaged;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getCustomerCountry() {
        return customerCountry;
    }

    public void setCustomerCountry(String customerCountry) {
        this.customerCountry = customerCountry;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencyHexCode() {
        return currencyHexCode;
    }

    public void setCurrencyHexCode(String currencyHexCode) {
        this.currencyHexCode = currencyHexCode;
    }

    public String getIsoCurrencyFormat() {
        return isoCurrencyFormat;
    }

    public void setIsoCurrencyFormat(String isoCurrencyFormat) {
        this.isoCurrencyFormat = isoCurrencyFormat;
    }

    public String getDialCode() {
        return dialCode;
    }

    public void setDialCode(String dialCode) {
        this.dialCode = dialCode;
    }

    public String getIsUk() {
        return isUk;
    }

    public void setIsUk(String isUk) {
        this.isUk = isUk;
    }

    public Integer getCompletedProperty() {
        return completedProperty;
    }

    public void setCompletedProperty(Integer completedProperty) {
        this.completedProperty = completedProperty;
    }

    public Integer getCompletedBuyer() {
        return completedBuyer;
    }

    public void setCompletedBuyer(Integer completedBuyer) {
        this.completedBuyer = completedBuyer;
    }

    public Integer getTourCompleted() {
        return tourCompleted;
    }

    public void setTourCompleted(Integer tourCompleted) {
        this.tourCompleted = tourCompleted;
    }

    public Integer getHasTfa() {
        return hasTfa;
    }

    public void setHasTfa(Integer hasTfa) {
        this.hasTfa = hasTfa;
    }

    public Integer getIsSidemenuClosed() {
        return isSidemenuClosed;
    }

    public void setIsSidemenuClosed(Integer isSidemenuClosed) {
        this.isSidemenuClosed = isSidemenuClosed;
    }

    public String getCustomerGender() {
        return customerGender;
    }

    public void setCustomerGender(String customerGender) {
        this.customerGender = customerGender;
    }

    public String getCustomerAgeGroup() {
        return customerAgeGroup;
    }

    public void setCustomerAgeGroup(String customerAgeGroup) {
        this.customerAgeGroup = customerAgeGroup;
    }
}
