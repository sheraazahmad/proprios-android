package com.ck.proprios.intro.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.ck.proprios.R;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.helpers.SharedPrefHelper;

public class WelcomeScreen extends AppCompatActivity implements View.OnClickListener {

    TextView tvLogin, tvJoin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_screen);
        if (((GlobalCLass) getApplicationContext()).isSlider()) {
            ((GlobalCLass) getApplicationContext()).setSlider(false);
            Animatoo.animateSlideRight(WelcomeScreen.this);
        } else {
            Animatoo.animateSlideLeft(WelcomeScreen.this);
        }

        tvLogin = findViewById(R.id.tvLogin);
        tvJoin = findViewById(R.id.tvJoin);

        tvLogin.setOnClickListener(this);
        tvJoin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == tvLogin) {
            Intent intent = new Intent(WelcomeScreen.this, LoginScreen.class);
            startActivity(intent);
            finishAffinity();
//            SharedPrefHelper.getInstance(WelcomeScreen.this).setLoginStatus(true);
//            Toast.makeText(this, "" + SharedPrefHelper.getInstance(WelcomeScreen.this).getLoginStatus(), Toast.LENGTH_SHORT).show();
        } else if (v == tvJoin) {
            Intent intent = new Intent(WelcomeScreen.this, RegisterScreen.class);
            startActivity(intent);
            finishAffinity();
        }
    }
}