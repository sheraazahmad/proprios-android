package com.ck.proprios.intro.controller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.ck.proprios.R;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.helpers.BottomSheetButton;
import com.ck.proprios.helpers.Loader;
import com.ck.proprios.helpers.SharedPrefHelper;
import com.ck.proprios.intro.pojo.LoginResponse;
import com.ck.proprios.network.APIClient;
import com.ck.proprios.network.APIInterface;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener {

    TextView tvWhoYouAreLabel, tvWhoYouAreValue, tvRegister;
    CardView btnLogin;
    RelativeLayout btnBack, rlWhoYouAre;
    BottomSheetButton bottomSheetButton;
    String email = null;
    String password = null;
    TextInputLayout edtEmail, edtPassword;
    APIInterface apiInterface;
    Loader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        if (((GlobalCLass) getApplicationContext()).noSlide) {
            ((GlobalCLass) getApplicationContext()).setNoSlide(false);
        } else {
            if (((GlobalCLass) getApplicationContext()).isSlider()) {
                ((GlobalCLass) getApplicationContext()).setSlider(false);
                Animatoo.animateSlideRight(LoginScreen.this);
            } else {
                Animatoo.animateSlideLeft(LoginScreen.this);
            }
        }
        apiInterface = APIClient.getAuthClient().create(APIInterface.class);
        loader = new Loader(LoginScreen.this);

//        btnBack = findViewById(R.id.btnBack);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        rlWhoYouAre = findViewById(R.id.rlWhoYouAre);
        tvRegister = findViewById(R.id.tvRegister);
        tvWhoYouAreLabel = findViewById(R.id.tvWhoYouAreLabel);
        tvWhoYouAreValue = findViewById(R.id.tvWhoYouAreValue);
        btnLogin = findViewById(R.id.btnLogin);
        bottomSheetButton = new BottomSheetButton();

//        btnBack.setOnClickListener(this);
        rlWhoYouAre.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
//        if (v == btnBack) {
//            ((GlobalCLass) getApplicationContext()).setSlider(true);
//            Intent intent = new Intent(LoginScreen.this, WelcomeScreen.class);
//            startActivity(intent);
//        } else
        if (v == rlWhoYouAre) {
            bottomSheetButton.openBottomSheetList(LoginScreen.this, tvWhoYouAreLabel, tvWhoYouAreValue);
        } else if (v == btnLogin) {
            email = edtEmail.getEditText().getText().toString();
            password = edtPassword.getEditText().getText().toString();
            if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password) || tvWhoYouAreValue.getVisibility() == View.GONE) {
                Toast.makeText(LoginScreen.this, "Please Fill Complete Form", Toast.LENGTH_SHORT).show();
            } else {
                loginAPI();
            }
        } else if (v == tvRegister) {
            ((GlobalCLass) getApplicationContext()).setNoSlide(true);
            Intent intent = new Intent(LoginScreen.this, RegisterScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivityForResult(intent, 0);
            overridePendingTransition(0, 0);
            finishAffinity();
        }
    }

    public void loginAPI() {
        loader.showLoader();
        HashMap<String, String> map = new HashMap<>();
        map.put("request", "login");
        map.put("customer_email", email);
        map.put("customer_type", "private-landlord");
        map.put("customer_password", password);
        map.put("remote_address", "Not allowed by user");
        map.put("user_agent", "Mobile|iPhone 11 Pro|iOS 13.4.1|Safari XX");

        Log.wtf("loginAPI", "Params: " + map);

        Call<LoginResponse> call = apiInterface.login(map);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();
                if (loginResponse.getError() != null) {
                    loader.hideLoader();
                    Toast.makeText(LoginScreen.this, "" + loginResponse.getError(), Toast.LENGTH_SHORT).show();
                } else {
//                    SharedPrefHelper.getInstance(LoginScreen.this).setLoginStatus(true);
                    ((GlobalCLass) getApplicationContext()).setLoginData(loginResponse);
                    ((GlobalCLass) getApplicationContext()).dashBoardInitial(LoginScreen.this);
                }
                Log.wtf("loginAPI", "response: " + new Gson().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loader.hideLoader();
                Log.wtf("loginAPI", "error: " + t.getMessage());
            }
        });
    }

}