package com.ck.proprios.helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;

import com.ck.proprios.R;
import com.ck.proprios.SideMenu.LandLord.controller.SideMenuReminder.SideMenuReminderFragment;
import com.ck.proprios.application.GlobalCLass;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

public class BottomSheetListAdapter extends RecyclerView.Adapter<BottomSheetListAdapter.vHolder> {

    Context mContext;
    List<String> labels = new ArrayList<>();
    List<Integer> icons = new ArrayList<>();
    int fragment_number;
    BottomSheetDialog dialog;
    NavController navController;
    ;

    public BottomSheetListAdapter(Context mContext, List<String> labels, List<Integer> icons,
                                  int fragment_number, BottomSheetDialog dialog) {
        this.mContext = mContext;
        this.labels = labels;
        this.icons = icons;
        this.fragment_number = fragment_number;
        this.dialog = dialog;
    }

    @NonNull
    @Override
    public vHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.bottom_sheet_item, parent, false);
        return new BottomSheetListAdapter.vHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull vHolder holder, final int position) {
        holder.tvBottomSheetListItem.setText(labels.get(position));
        holder.iconBottomSheetListItem.setImageResource(icons.get(position));
        if (fragment_number == 1) {
            if (position == 1) {
                holder.tvBottomSheetListItem.setTextColor(mContext.getResources().getColor(R.color.colorD0));
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragment_number == 1) {
                    sideMenuReminderBtn(position);
                } else {

                }
            }
        });
    }

    private void sideMenuReminderBtn(int position) {
        if (position == 0) {
            dialog.dismiss();
            ((GlobalCLass) mContext.getApplicationContext()).getSideMenuReminderFragment().editReminder();
        } else {
            ((GlobalCLass) mContext.getApplicationContext()).getSideMenuReminderFragment().deleteAPI();
            dialog.dismiss();
        }
    }

    @Override
    public int getItemCount() {
        return labels.size();
    }

    public class vHolder extends RecyclerView.ViewHolder {

        TextView tvBottomSheetListItem;
        ImageView iconBottomSheetListItem;

        public vHolder(@NonNull View itemView) {
            super(itemView);
            tvBottomSheetListItem = itemView.findViewById(R.id.tvBottomSheetListItem);
            iconBottomSheetListItem = itemView.findViewById(R.id.iconBottomSheetListItem);
        }
    }
}
