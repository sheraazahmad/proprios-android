package com.ck.proprios.helpers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TimeSlotModel {
    public String time;
    public boolean status;
    public List<TimeSlotModel> list;
    public List<TimeSlotModel> fullList;

    public TimeSlotModel() {
    }

    public TimeSlotModel(String time, boolean status) {
        this.time = time;
        this.status = status;
    }

    public List<TimeSlotModel> getList() {
        if (list == null) {
            list = new ArrayList<>();
            Date currentTime = Calendar.
                    getInstance().getTime();
            int hour = currentTime.getHours();
            int minutes;
            if (currentTime.getMinutes() < 30) {
                minutes = 30;
            } else {
                minutes = 60;
                hour = hour + 1;
            }
            for (int i = 0; i < 48; i++) {
                if (i != 0 && minutes != 30) {
                    hour = hour + 1;
                }
                hour = hour % 24;
                if (minutes == 60) {
                    minutes = 00;
                    list.add(new TimeSlotModel(String.format("%02d", hour) + ":" + String.format("%02d", minutes), false));
                    minutes = 60;
                } else {
                    list.add(new TimeSlotModel(String.format("%02d", hour) + ":" + String.format("%02d", minutes), false));
                }
                minutes = minutes + 30;
                if (minutes == 90) {
                    minutes = 30;
                }
                if (hour == 00) {
                    break;
                }
            }
            return list;
        } else {
            return list;
        }
    }

    public List<TimeSlotModel> getFullList() {
        if (fullList == null) {
            fullList = new ArrayList<>();
            Date currentTime = Calendar.
                    getInstance().getTime();
            int hour = currentTime.getHours();
            int minutes;
            if (currentTime.getMinutes() < 30) {
                minutes = 30;
            } else {
                minutes = 60;
                hour = hour + 1;
            }
            for (int i = 0; i < 48; i++) {
                if (i != 0 && minutes != 30) {
                    hour = hour + 1;
                }
                hour = hour % 24;
                if (minutes == 60) {
                    minutes = 00;
                    fullList.add(new TimeSlotModel(String.format("%02d", hour) + ":" + String.format("%02d", minutes), false));
                    minutes = 60;
                } else {
                    fullList.add(new TimeSlotModel(String.format("%02d", hour) + ":" + String.format("%02d", minutes), false));
                }
                minutes = minutes + 30;
                if (minutes == 90) {
                    minutes = 30;
                }
            }
            return fullList;
        } else {
            return fullList;
        }
    }

    public List<TimeSlotModel> getFullListNew() {
        if (fullList == null) {
            fullList = new ArrayList<>();
            fullList.add(new TimeSlotModel("00:00", false));
            fullList.add(new TimeSlotModel("00:30", false));
            fullList.add(new TimeSlotModel("01:00", false));
            fullList.add(new TimeSlotModel("01:30", false));
            fullList.add(new TimeSlotModel("02:00", false));
            fullList.add(new TimeSlotModel("02:30", false));
            fullList.add(new TimeSlotModel("03:00", false));
            fullList.add(new TimeSlotModel("03:30", false));
            fullList.add(new TimeSlotModel("04:00", false));
            fullList.add(new TimeSlotModel("04:30", false));
            fullList.add(new TimeSlotModel("05:50", false));
            fullList.add(new TimeSlotModel("06:00", false));
            fullList.add(new TimeSlotModel("06:30", false));
            fullList.add(new TimeSlotModel("07:00", false));
            fullList.add(new TimeSlotModel("07:30", false));
            fullList.add(new TimeSlotModel("08:00", false));
            fullList.add(new TimeSlotModel("08:30", false));
            fullList.add(new TimeSlotModel("09:00", false));
            fullList.add(new TimeSlotModel("09:30", false));
            fullList.add(new TimeSlotModel("10:00", false));
            fullList.add(new TimeSlotModel("10:30", false));
            fullList.add(new TimeSlotModel("11:00", false));
            fullList.add(new TimeSlotModel("11:30", false));
            fullList.add(new TimeSlotModel("12:00", false));
            fullList.add(new TimeSlotModel("12:30", false));
            fullList.add(new TimeSlotModel("12:30", false));
            fullList.add(new TimeSlotModel("13:00", false));
            fullList.add(new TimeSlotModel("13:30", false));
            fullList.add(new TimeSlotModel("14:00", false));
            fullList.add(new TimeSlotModel("14:30", false));
            fullList.add(new TimeSlotModel("15:00", false));
            fullList.add(new TimeSlotModel("15:30", false));
            fullList.add(new TimeSlotModel("16:00", false));
            fullList.add(new TimeSlotModel("16:30", false));
            fullList.add(new TimeSlotModel("17:00", false));
            fullList.add(new TimeSlotModel("17:30", false));
            fullList.add(new TimeSlotModel("18:00", false));
            fullList.add(new TimeSlotModel("18:30", false));
            fullList.add(new TimeSlotModel("19:00", false));
            fullList.add(new TimeSlotModel("19:30", false));
            fullList.add(new TimeSlotModel("20:00", false));
            fullList.add(new TimeSlotModel("20:30", false));
            fullList.add(new TimeSlotModel("21:00", false));
            fullList.add(new TimeSlotModel("21:30", false));
            fullList.add(new TimeSlotModel("22:00", false));
            fullList.add(new TimeSlotModel("22:30", false));
            fullList.add(new TimeSlotModel("23:00", false));
            fullList.add(new TimeSlotModel("23:30", false));
            fullList.add(new TimeSlotModel("24:00", false));
            return fullList;
        } else {
            return fullList;
        }
    }
}
