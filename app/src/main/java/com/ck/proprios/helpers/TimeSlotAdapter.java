package com.ck.proprios.helpers;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ck.proprios.R;
import com.ck.proprios.dashboard.controller.DashBoardScreen;

import java.util.List;

public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.VHolder> {
    Context mContext;
    List<TimeSlotModel> list;
    public int index = -1;
    boolean isFullList;

    public TimeSlotAdapter(Context mContext, List<TimeSlotModel> list, boolean isFullList) {
        this.mContext = mContext;
        this.list = list;
        this.isFullList = isFullList;
    }

    @NonNull
    @Override
    public VHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.time_slot_item, parent, false);
        return new TimeSlotAdapter.VHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VHolder holder, final int position) {
        final TimeSlotModel model = list.get(position);
        holder.time.setText(model.time);
        holder.timeSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DashBoardScreen) mContext).changeDateStatusDash(position);
                changeStatus(position);
                if (isFullList) {
                    model.getFullListNew().get(position).status = true;
                    Log.wtf("statusChangeFull", "->" + model.getFullListNew().get(position).status);
                } else {
                    model.getList().get(position).status = true;
                    Log.wtf("statusChange", "->" + model.getList().get(position).status);
                }
                index = position;

                notifyDataSetChanged();

            }
        });
        if (model.status) {
            holder.timeSlot.setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            holder.time.setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else {
            holder.timeSlot.setBackground(mContext.getResources().getDrawable(R.drawable.time_slot_bg_unselect));
            holder.time.setTextColor(mContext.getResources().getColor(R.color.colorC1));
        }
    }

    private void changeStatus(int position) {
        for (int i = 0; i < list.size(); i++) {
            if (position == i) {
                list.get(i).status = true;
            } else {
                list.get(i).status = false;
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {

        RelativeLayout timeSlot;
        TextView time;

        public VHolder(@NonNull View itemView) {
            super(itemView);
            timeSlot = itemView.findViewById(R.id.timeSlotItem);
            time = itemView.findViewById(R.id.txtTime);
        }
    }
}
