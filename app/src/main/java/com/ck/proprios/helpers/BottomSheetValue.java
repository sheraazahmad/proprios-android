package com.ck.proprios.helpers;

import java.util.ArrayList;

public class BottomSheetValue {
    private int id;
    private String name;

    private static ArrayList<BottomSheetValue> bottomSheetValueArrayList;

    public BottomSheetValue(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static void initValue() {
        bottomSheetValueArrayList = new ArrayList<>();
        BottomSheetValue owner = new BottomSheetValue(0, "Owner/Buyer");
        bottomSheetValueArrayList.add(owner);
        BottomSheetValue tenant = new BottomSheetValue(1, "Tenant");
        bottomSheetValueArrayList.add(tenant);
        BottomSheetValue tradeperson = new BottomSheetValue(2, "Tradeperson");
        bottomSheetValueArrayList.add(tradeperson);
    }

    public static ArrayList<BottomSheetValue> getBottomSheetValueArrayList() {
        return bottomSheetValueArrayList;
    }

    public static String[] valueNames() {
        String[] names = new String[bottomSheetValueArrayList.size()];
        for (int i = 0; i < bottomSheetValueArrayList.size(); i++) {
            names[i] = bottomSheetValueArrayList.get(i).name;
        }
        return names;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
