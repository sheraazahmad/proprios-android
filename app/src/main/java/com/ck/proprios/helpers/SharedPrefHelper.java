package com.ck.proprios.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SharedPrefHelper {
    private static SharedPrefHelper _this;
    Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public static SharedPrefHelper getInstance(Context context) {
        if (_this == null) {
            _this = new SharedPrefHelper();
            _this.preferences = context.getSharedPreferences("loginPref", Context.MODE_PRIVATE);
            _this.editor = _this.preferences.edit();
        }
        _this.context = context;
        return _this;
    }

    public boolean getLoginStatus() {
        return preferences.getBoolean("loginStatus", false);
    }

    public void setLoginStatus(boolean loginStatus) {
        editor.putBoolean("loginStatus", loginStatus);
        editor.apply();
    }

    public boolean getFingerPrintStatus() {
        return preferences.getBoolean("fingerPrintStatus", false);
    }

    public void setFingerPrintStatus(boolean fingerPrint) {
        editor.putBoolean("fingerPrintStatus", fingerPrint);
        editor.apply();
    }

    public boolean getPassCodeStatus() {
        return preferences.getBoolean("passCode", false);
    }

    public void setPassCodeStatus(boolean passCode) {
        editor.putBoolean("passCode", passCode);
        editor.apply();
    }

    public String getPassCodeValue() {
        return preferences.getString("passCodeValue", null);
    }

    public void setPassCodeValue(String passCodeValue) {
        editor.putString("passCodeValue", passCodeValue);
        editor.apply();
    }

    public String getUserName() {
        return preferences.getString("userName", null);
    }

    public String getPassword() {
        return preferences.getString("password", null);
    }

    public String getCustomerType() {
        return preferences.getString("customerType", null);
    }

    public void setCustomerType(String customerType) {
        editor.putString("customerType", customerType);
        editor.apply();
    }

    public void setUserNameAndPassword(String userName, String password) {
        editor.putString("userName", userName);
        editor.putString("password", password);
        editor.apply();
    }

    public void logOut() {
        editor.clear();
        editor.apply();
    }

}
