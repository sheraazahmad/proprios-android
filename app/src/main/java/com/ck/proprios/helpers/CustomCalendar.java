package com.ck.proprios.helpers;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ck.proprios.R;
import com.ck.proprios.SideMenu.LandLord.controller.SideMenuReminder.SideMenuAddReminderFragment;
import com.ck.proprios.SideMenu.LandLord.pojo.SideMenuAddReminder;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.dashboard.controller.DashBoardScreen;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CustomCalendar extends AppCompatActivity implements View.OnClickListener {

    BottomSheetDialog dialog;
    Context mContext;
    RelativeLayout btnCross, btnDone, selectDay, yearMinus, yearPlus;
    TextView label, setValue, txtYear;
    String selectedDate = "", todayDate = "", finalDate = "";
    boolean isDateSelected = false;
    Calendar calendarTemp = Calendar.getInstance();
    View btnMonths, btnMonthsMinus, btnMonthsPlus;
    LinearLayout selectMonths;
    TextView[] months = new TextView[12];
    int day, month, year, todayDay, todayMonth, todayYear;
    CalendarView calendarView;
    final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    ImageView iconYearMinus;

    RecyclerView timeRecycleView;
    List<TimeSlotModel> list = new ArrayList<>();
    TimeSlotModel model = new TimeSlotModel();
    TimeSlotAdapter adapter;
    static int selectedTimeSlotTemp = -1, selectedTimeSlot = -1;
    NavController navController;
    int fragmentStatus = -1;
    String editDateTimeValue = null;

    public String calendarPopUp(Context context, final TextView label, final TextView setValue,
                                NavController navController, String editDateTimeValue) {
        mContext = context;
        this.label = label;
        this.setValue = setValue;
        this.navController = navController;
        this.editDateTimeValue = editDateTimeValue;
        dialog = new BottomSheetDialog(mContext);
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view1 = layoutInflater.inflate(R.layout.calendar_pop_up, null);
        dialog.setContentView(view1);

        if (editDateTimeValue.equalsIgnoreCase("")) {

        } else {
            String dateTimeSplit[] = editDateTimeValue.split(" ");
            isDateSelected = true;
            selectedDate = dateTimeSplit[0];
            Log.wtf("editDateTime", "" + dateTimeSplit[0] + "Size: " + dateTimeSplit.length);
        }
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel().toString().equals("fragment_side_menu_add_reminder")) {
                    fragmentStatus = 0;
                }
            }
        });
        calendarView = view1.findViewById(R.id.calendarView);
        timeRecycleView = view1.findViewById(R.id.timeRecycleView);
        btnMonths = view1.findViewById(R.id.btnMonths);
        selectDay = view1.findViewById(R.id.selectDay);
        selectMonths = view1.findViewById(R.id.selectMonths);
        txtYear = view1.findViewById(R.id.txtYear);
        yearMinus = view1.findViewById(R.id.yearMinus);
        iconYearMinus = view1.findViewById(R.id.iconYearMinus);
        yearPlus = view1.findViewById(R.id.yearPlus);
        btnMonthsPlus = view1.findViewById(R.id.btnMonthsPlus);
        btnMonthsMinus = view1.findViewById(R.id.btnMonthsMinus);
        btnCross = view1.findViewById(R.id.btnCross);
        btnDone = view1.findViewById(R.id.btnDone);
        months[0] = view1.findViewById(R.id.january);
        months[1] = view1.findViewById(R.id.february);
        months[2] = view1.findViewById(R.id.march);
        months[3] = view1.findViewById(R.id.april);
        months[4] = view1.findViewById(R.id.may);
        months[5] = view1.findViewById(R.id.june);
        months[6] = view1.findViewById(R.id.july);
        months[7] = view1.findViewById(R.id.august);
        months[8] = view1.findViewById(R.id.september);
        months[9] = view1.findViewById(R.id.october);
        months[10] = view1.findViewById(R.id.november);
        months[11] = view1.findViewById(R.id.december);

        for (int i = 0; i < model.getList().size(); i++) {
            model.getList().get(i).status = false;
        }
        for (int i = 0; i < model.getFullListNew().size(); i++) {
            model.getFullListNew().get(i).status = false;
        }

        list = model.getList();
        adapter = new TimeSlotAdapter(mContext, list, false);
        timeRecycleView.setHasFixedSize(true);
        timeRecycleView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        if (selectedTimeSlot > -1) {
            Log.wtf("SelectedTime2", "" + selectedTimeSlot);
            changeSlotList();
            for (int i = 0; i < list.size(); i++) {
                if (i == selectedTimeSlot) {
                    list.get(i).status = true;
                } else {
                    list.get(i).status = false;
                }
            }
        }
        timeRecycleView.setAdapter(adapter);
        timeRecycleView.scrollToPosition(selectedTimeSlot);
        dialog.setCancelable(false);
        dialog.show();

        //***************** Today's Date *************

        todayDate = new SimpleDateFormat("dd/MM/YYYY", Locale.getDefault()).format(new Date());
        Log.wtf("DateNew4", todayDate);
        String todayDateParts[] = todayDate.split("/");
        todayDay = Integer.parseInt(todayDateParts[0]);
        todayMonth = Integer.parseInt(todayDateParts[1]);
        todayYear = Integer.parseInt(todayDateParts[2]);
        calendarView.setMinDate(getMilliTime(todayDate));

        //***************** Today's Date *************

        yearMinus.setVisibility(View.INVISIBLE);
        yearMinus.setEnabled(false);

        if (isDateSelected) {
            calendarView.setDate(getMilliTime(selectedDate), true, true);
            int yearThis = Integer.parseInt(txtYear.getText().toString());
            if (yearThis > todayYear) {
                yearMinus.setVisibility(View.VISIBLE);
                yearMinus.setEnabled(true);
            }
        }

        selectedDate = sdf.format(new Date(calendarView.getDate()));
        Log.wtf("DateNew", selectedDate);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                calendarTemp.set(Calendar.YEAR, year);
                calendarTemp.set(Calendar.MONTH, month);
                calendarTemp.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                selectedDate = sdf.format(calendarTemp.getTime());
                calendarView.setDate(getMilliTime(selectedDate));
                Log.wtf("DateNew1", selectedDate + " Day: " + dayOfMonth + " Month: " + month + " Year: " + year);
            }
        });

        btnCross.setOnClickListener(this);
        btnDone.setOnClickListener(this);
        btnMonths.setOnClickListener(this);
        months[0].setOnClickListener(this);
        months[1].setOnClickListener(this);
        months[2].setOnClickListener(this);
        months[3].setOnClickListener(this);
        months[4].setOnClickListener(this);
        months[5].setOnClickListener(this);
        months[6].setOnClickListener(this);
        months[7].setOnClickListener(this);
        months[8].setOnClickListener(this);
        months[9].setOnClickListener(this);
        months[10].setOnClickListener(this);
        months[11].setOnClickListener(this);
        yearMinus.setOnClickListener(this);
        yearPlus.setOnClickListener(this);
        btnMonthsPlus.setOnClickListener(this);
        btnMonthsMinus.setOnClickListener(this);
        return setValue.getText().toString();
    }

    @Override
    public void onClick(View v) {
        if (v == btnCross) {
            selectedDate = finalDate;
            selectedTimeSlotTemp = selectedTimeSlot;
            dialog.dismiss();
        } else if (v == btnDone) {
            if (selectedTimeSlotTemp == -1) {
                Toast.makeText(mContext, "Please Select Time First", Toast.LENGTH_SHORT).show();
            } else {
                isDateSelected = true;
                dialog.dismiss();
                slideUp(label);
                setValue.setVisibility(View.VISIBLE);
                selectedTimeSlot = selectedTimeSlotTemp;
                finalDate = selectedDate;
                Log.wtf("SelectedTime1", "" + selectedTimeSlot);
                setValue.setText(finalDate + " - " + list.get(selectedTimeSlot).time);
                if (fragmentStatus == 0) {
                    ((GlobalCLass) mContext.getApplicationContext()).getSideMenuAddReminderFragment().changeColor();
                } else {
                    ((GlobalCLass) mContext.getApplicationContext()).getSideMenuEditReminderFragment().changeColor();
                }
            }
        } else if (v == btnMonths) {
            selectDay.setVisibility(View.GONE);
            btnDone.setVisibility(View.GONE);
            selectMonths.setVisibility(View.VISIBLE);
            monthBackground(month);
        } else if (v == months[0]) {
            changeMonth(1);
        } else if (v == months[1]) {
            changeMonth(2);
        } else if (v == months[2]) {
            changeMonth(3);
        } else if (v == months[3]) {
            changeMonth(4);
        } else if (v == months[4]) {
            changeMonth(5);
        } else if (v == months[5]) {
            changeMonth(6);
        } else if (v == months[6]) {
            changeMonth(7);
        } else if (v == months[7]) {
            changeMonth(8);
        } else if (v == months[8]) {
            changeMonth(9);
        } else if (v == months[9]) {
            changeMonth(10);
        } else if (v == months[10]) {
            changeMonth(11);
        } else if (v == months[11]) {
            changeMonth(12);
        } else if (v == yearMinus) {
            int yearThis = Integer.parseInt(txtYear.getText().toString());
            yearThis--;
            year = yearThis;
            txtYear.setText("" + yearThis);
            monthBackground(month);
            if (yearThis == todayYear) {
                yearMinus.setVisibility(View.INVISIBLE);
                yearMinus.setEnabled(false);
                if (month < todayMonth) {
                    monthBackground(todayMonth);
                }
            }
        } else if (v == yearPlus) {
            int yearThis = Integer.parseInt(txtYear.getText().toString());
            yearThis++;
            year = yearThis;
            txtYear.setText("" + yearThis);
            monthBackground(month);
            if (yearThis > todayYear) {
                yearMinus.setVisibility(View.VISIBLE);
                yearMinus.setEnabled(true);
            }
        } else if (v == btnMonthsPlus) {
            if (month == 12) {
                year = year + 1;
            }
            if (year > todayYear) {
                yearMinus.setVisibility(View.VISIBLE);
                yearMinus.setEnabled(true);
            }
            month = month + 1;
            monthPlusMinusBtn();
            changeSlotList();
        } else if (v == btnMonthsMinus) {
            if (month == 1) {
                year = year - 1;
            }
            if (year == todayYear) {
                yearMinus.setVisibility(View.INVISIBLE);
                yearMinus.setEnabled(false);
            }
            month = month - 1;
            monthPlusMinusBtn();
            changeSlotList();
        }
    }

    public long getMilliTime(String date) {
        String dateParts[] = date.split("/");
        day = Integer.parseInt(dateParts[0]);
        month = Integer.parseInt(dateParts[1]);
        year = Integer.parseInt(dateParts[2]);
        Calendar calender = Calendar.getInstance();
        calender.set(Calendar.YEAR, year);
        calender.set(Calendar.MONTH, month - 1);
        calender.set(Calendar.DAY_OF_MONTH, day);
        txtYear.setText("" + year);
        Log.wtf("NewDate", "" + day + " : " + month + " : " + year + " today => " + todayDay + " : " + todayMonth + " : " + todayYear);
        changeSlotList();
        return calender.getTimeInMillis();
    }

    public void slideDown(TextView label) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(label, "translationY", 0f);
        animation.setDuration(300);
        animation.start();
        label.setTextSize(15);
    }

    public void slideUp(TextView label) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(label, "translationY", -40f);
        animation.setDuration(300);
        animation.start();
        label.setTextSize(12);
    }

    public void monthBackground(int month) {
        int yearThis = Integer.parseInt(txtYear.getText().toString());
        if (month == 1) {
            unSelectedMonth(yearThis);
            months[0].setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            months[0].setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (month == 2) {
            unSelectedMonth(yearThis);
            months[1].setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            months[1].setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (month == 3) {
            unSelectedMonth(yearThis);
            months[2].setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            months[2].setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (month == 4) {
            unSelectedMonth(yearThis);
            months[3].setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            months[3].setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (month == 5) {
            unSelectedMonth(yearThis);
            months[4].setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            months[4].setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (month == 6) {
            unSelectedMonth(yearThis);
            months[5].setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            months[5].setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (month == 7) {
            unSelectedMonth(yearThis);
            months[6].setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            months[6].setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (month == 8) {
            unSelectedMonth(yearThis);
            months[7].setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            months[7].setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (month == 9) {
            unSelectedMonth(yearThis);
            months[8].setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            months[8].setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (month == 10) {
            unSelectedMonth(yearThis);
            months[9].setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            months[9].setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (month == 11) {
            unSelectedMonth(yearThis);
            months[10].setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            months[10].setTextColor(mContext.getResources().getColor(R.color.color5A));
        } else if (month == 12) {
            unSelectedMonth(yearThis);
            months[11].setBackground(mContext.getResources().getDrawable(R.drawable.selected_month_bg));
            months[11].setTextColor(mContext.getResources().getColor(R.color.color5A));
        }
    }

    public void changeMonth(int month) {
        int yearThis = Integer.parseInt(txtYear.getText().toString());
        if (month < todayMonth && yearThis == todayYear) {
            Toast.makeText(mContext, "Can't select this month", Toast.LENGTH_SHORT).show();
        } else {
            selectDay.setVisibility(View.VISIBLE);
            selectMonths.setVisibility(View.GONE);
            btnDone.setVisibility(View.VISIBLE);
            monthBackground(month);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month - 1);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            selectedDate = sdf.format(calendar.getTime());
            calendarView.setDate(getMilliTime(selectedDate), true, true);
        }
    }

    public void unSelectedMonth(int year) {
        for (int i = 0; i < 12; i++) {
            if (i < (todayMonth - 1) && year == todayYear) {
                months[i].setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
                months[i].setTextColor(mContext.getResources().getColor(R.color.colorDisableDate));
            } else {
                months[i].setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
                months[i].setTextColor(mContext.getResources().getColor(R.color.colorMonthText));
            }
        }
    }

    public void monthPlusMinusBtn() {
        month = (month) % 12;
        if (month == 0) {
            month = 12;
        }
        Calendar calender = Calendar.getInstance();
        calender.set(Calendar.YEAR, year);
        calender.set(Calendar.MONTH, month - 1);
        calender.set(Calendar.DAY_OF_MONTH, day);
        txtYear.setText("" + year);
        calendarView.setDate(calender.getTimeInMillis(), true, true);
        selectedDate = sdf.format(new Date(calendarView.getDate()));
    }

    public void changeSlotStatus(int position) {
        selectedTimeSlotTemp = position;
        Log.wtf("SelectedTime", "" + selectedTimeSlotTemp);
//        statusTime = true;
//        for (int i = 0; i < list.size(); i++) {
//            if (position == i) {
//                list.get(i).status = true;
////                slotNumberTest = i;
////                selectedTimeValue.setText("- " + list.get(i).time);
//            } else {
//                list.get(i).status = false;
//            }
//        }
//        adapter = new TimeSlotAdapter(mContext, list, true);
//        adapter.notifyDataSetChanged();
//        timeRecycleView.setAdapter(adapter);
//        TimeSlotAdapter.this.notifyDataSetChanged();
    }

    public void changeSlotList() {
        if (day > todayDay) {
            list = model.getFullListNew();
            adapter = new TimeSlotAdapter(mContext, list, true);
            timeRecycleView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else if (month > todayMonth) {
            list = model.getFullListNew();
            adapter = new TimeSlotAdapter(mContext, list, true);
            timeRecycleView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else if (year > todayYear) {
            list = model.getFullListNew();
            adapter = new TimeSlotAdapter(mContext, list, true);
            timeRecycleView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            list = model.getList();
            adapter = new TimeSlotAdapter(mContext, list, false);
            timeRecycleView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

}