package com.ck.proprios.helpers;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.appcompat.app.AppCompatActivity;

import com.ck.proprios.R;

public class Loader extends AppCompatActivity {
    Dialog mDialog;
    Context context;

    public Loader(Context contextThis){
        context = contextThis;
        mDialog = new Dialog(context);
        mDialog.setContentView(R.layout.popup_dialog);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
    }
    public void showLoader() {
        mDialog.show();
    }

    public void hideLoader() {
        mDialog.dismiss();
    }
}
