package com.ck.proprios.helpers;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ck.proprios.R;
import com.ck.proprios.application.GlobalCLass;
import com.ck.proprios.intro.pojo.DashBoardInitialLoad;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

public class BottomSheetButton extends AppCompatActivity implements View.OnClickListener {

    BottomSheetDialog dialog;
    Context mContext;
    RecyclerView rvBottomSheetList;
    RecyclerView rvBottomSheet;
    RelativeLayout btnCross, btnDone;
    BottomSheetListAdapter adapter;
    TextView label, setValue;
    List<String> list = new ArrayList<>();
    NumberPicker npWhoYouAre;
    String value = "";
    int position = -1;
    static String customerTypeValue[] = new String[5];
    static int selectedValuePosition;
    static String customerTypeKeyText = null;
    static String customerTypeKey[] = new String[5];
    static String whoYouAreValue = null;
    static String whoYouAreKey = null;

    private void slideDown(TextView label) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(label, "translationY", 0f);
        animation.setDuration(300);
        animation.start();
        label.setTextSize(15);
    }

    public void slideUp(TextView label) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(label, "translationY", -40f);
        animation.setDuration(300);
        animation.start();
        label.setTextSize(12);
    }

    public void openBottomSheetList(Context context, TextView label, TextView setValue) {
        mContext = context;
        this.label = label;
        this.setValue = setValue;
        dialog = new BottomSheetDialog(mContext);
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view1 = layoutInflater.inflate(R.layout.bottom_sheet_list, null);
        dialog.setContentView(view1);
        dialog.setCancelable(false);

        list.add("Owner/Buyer");
        list.add("Tenant");
        list.add("Tradesperson");

        btnCross = view1.findViewById(R.id.btnCross);
        btnDone = view1.findViewById(R.id.btnDone);

        customerTypeValue = new String[]{"Owner/Buyer", "Tenants", "Tradeperson"};
        int length = customerTypeValue.length;

        npWhoYouAre = view1.findViewById(R.id.npWhoYouAre);
        BottomSheetValue.initValue();
        npWhoYouAre.setMaxValue(length - 1);
        npWhoYouAre.setMinValue(0);

        npWhoYouAre.setDisplayedValues(customerTypeValue);
        npWhoYouAre.setWrapSelectorWheel(false);
        value = BottomSheetValue.getBottomSheetValueArrayList().get(0).getName();
        npWhoYouAre.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                value = BottomSheetValue.getBottomSheetValueArrayList().get(newVal).getName();
                position = newVal;
//                Toast.makeText(mContext, "" + newVal, Toast.LENGTH_SHORT).show();
            }
        });
//        pickValue = new String[]{"Owner/Buyer", "Tenant", "Tradesperson"};
        btnCross.setOnClickListener(this);
        btnDone.setOnClickListener(this);
        dialog.show();
    }

    public void openBottomSheetPopUp(Context context, List<String> labelsList, List<Integer> iconsList, int fragment_number) {
        mContext = context;
        dialog = new BottomSheetDialog(mContext);
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view1 = layoutInflater.inflate(R.layout.bottom_sheet_pop_up, null);
        dialog.setContentView(view1);

        adapter = new BottomSheetListAdapter(mContext, labelsList, iconsList, fragment_number, dialog);
        rvBottomSheet = view1.findViewById(R.id.rvBottomSheet);
        rvBottomSheet.setHasFixedSize(true);
        rvBottomSheet.setLayoutManager(new LinearLayoutManager(mContext));
        rvBottomSheet.setAdapter(adapter);

        dialog.show();
    }

    //    ************************* When to remind *************************

    List<String> listWhenToRemind = new ArrayList<>();
    NumberPicker npListWhenToRemind;
    String[] stringArrayWhenToRemind;
    int positionWhenToRemind = 0;
    public static boolean isDoneWhenToRemind = false;
    NavController navController;

    public int listTimeToRemindPopUp(Context context, TextView label, TextView setValue,
                                     List<String> list, final int positionWhenToRemindNew,
                                     final NavController navController) {
        mContext = context;
        this.label = label;
        this.setValue = setValue;
        this.navController = navController;
        listWhenToRemind.clear();
        listWhenToRemind.addAll(list);
        dialog = new BottomSheetDialog(mContext);
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view1 = layoutInflater.inflate(R.layout.pop_up_list, null);
        dialog.setContentView(view1);
        dialog.setCancelable(false);

        npListWhenToRemind = view1.findViewById(R.id.npList);
        btnCross = view1.findViewById(R.id.btnCross);
        btnDone = view1.findViewById(R.id.btnDone);

        stringArrayWhenToRemind = listWhenToRemind.toArray(new String[0]);
        npListWhenToRemind.setMaxValue(listWhenToRemind.size() - 1);
        npListWhenToRemind.setMinValue(0);
        npListWhenToRemind.setDisplayedValues(stringArrayWhenToRemind);
        npListWhenToRemind.setWrapSelectorWheel(false);
        dialog.show();
        Log.wtf("positionWhenTo", "" + positionWhenToRemindNew + "" + isDoneWhenToRemind);
        if (positionWhenToRemindNew > -1 && !isDoneWhenToRemind) {
            npListWhenToRemind.setValue(positionWhenToRemindNew);
        } else {
            npListWhenToRemind.setValue(positionWhenToRemind);
        }

        npListWhenToRemind.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                positionWhenToRemind = newVal;
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDoneWhenToRemind = true;
                setWhenToRemindValue();
                navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
                    @Override
                    public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                        if (destination.getLabel().toString().equals("fragment_side_menu_add_reminder")) {
                            ((GlobalCLass) mContext.getApplicationContext()).getSideMenuAddReminderFragment().changeColor();
                        } else if (destination.getLabel().toString().equals("fragment_side_menu_edit_reminder")) {
                            ((GlobalCLass) mContext.getApplicationContext()).getSideMenuEditReminderFragment().changeColor();
                            ((GlobalCLass) mContext.getApplicationContext()).getSideMenuEditReminderFragment().positionWhenToRemind = positionWhenToRemind;
                        }
                    }
                });
            }
        });
        btnCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (!isDoneWhenToRemind) {
//                    positionWhenToRemind = positionWhenToRemindNew;
//                }
                dialog.dismiss();
            }
        });
        Log.wtf("PositionWhenToRemind", "" + positionWhenToRemind);
        if (!isDoneWhenToRemind) {
            return positionWhenToRemindNew;
        } else {
            return positionWhenToRemind;
        }
    }

    public void setWhenToRemindValue() {
        List<DashBoardInitialLoad.ReminderWhen> reminderWhenList = new ArrayList<>();
        reminderWhenList.clear();
        reminderWhenList.addAll(((GlobalCLass) mContext.getApplicationContext()).
                getDashBoardInitialLoad().getReminderWhen());
        Log.wtf("position", "" + positionWhenToRemind);
        slideUp(label);
        setValue.setVisibility(View.VISIBLE);
        setValue.setText(reminderWhenList.get(positionWhenToRemind).getOptionValue());
        dialog.dismiss();
    }

    //    ************************* property *************************

    List<String> listProperty = new ArrayList<>();
    String[] stringArrayProperty;
    int positionProperty = 0;
    public static boolean isDoneProperty = false;

    public int listPropertyPopUp(Context context, TextView label, TextView setValue,
                                 List<String> list, int positionPropertyNew, final NavController navController) {
        mContext = context;
        this.label = label;
        this.setValue = setValue;
        this.navController = navController;
        listProperty.clear();
        listProperty.addAll(list);
        dialog = new BottomSheetDialog(mContext);
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view1 = layoutInflater.inflate(R.layout.pop_up_list, null);
        dialog.setContentView(view1);
        dialog.setCancelable(false);

        npListWhenToRemind = view1.findViewById(R.id.npList);
        btnCross = view1.findViewById(R.id.btnCross);
        btnDone = view1.findViewById(R.id.btnDone);

        stringArrayProperty = listProperty.toArray(new String[0]);
        npListWhenToRemind.setMaxValue(listProperty.size() - 1);
        npListWhenToRemind.setMinValue(0);
        npListWhenToRemind.setDisplayedValues(stringArrayProperty);
        npListWhenToRemind.setWrapSelectorWheel(false);
        dialog.show();

        if (positionPropertyNew > -1 && !isDoneProperty) {
            Log.wtf("poitionProperty", "" + positionPropertyNew);
            npListWhenToRemind.setValue(positionPropertyNew + 1);
        } else {
            npListWhenToRemind.setValue(positionProperty);
        }

        npListWhenToRemind.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                positionProperty = newVal;
            }
        });

        btnCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDoneProperty = true;
                setProperty();
                navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
                    @Override
                    public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                        if (destination.getLabel().toString().equals("fragment_side_menu_add_reminder")) {
                            ((GlobalCLass) mContext.getApplicationContext()).getSideMenuAddReminderFragment().changeColor();
                        } else if (destination.getLabel().toString().equals("fragment_side_menu_edit_reminder")) {
                            ((GlobalCLass) mContext.getApplicationContext()).getSideMenuEditReminderFragment().changeColor();
                            ((GlobalCLass) mContext.getApplicationContext()).getSideMenuEditReminderFragment().positionProperty = positionProperty;
                        }
                    }
                });
            }
        });

        return positionProperty;
    }

    private void setProperty() {
        List<DashBoardInitialLoad.PropertyPortfolio> propertyPortfolioList = new ArrayList<>();
        propertyPortfolioList.clear();
        propertyPortfolioList.addAll(((GlobalCLass) mContext.getApplicationContext()).
                getDashBoardInitialLoad().getPropertyPortfolio());
        Log.wtf("position", "" + positionProperty);
        slideUp(label);
        setValue.setVisibility(View.VISIBLE);
        if (positionProperty == 0) {
            setValue.setText("All");
        } else {
            setValue.setText(propertyPortfolioList.get(positionProperty - 1).getPropertyAddress1());
        }
        dialog.dismiss();
    }

    //    ************************* auto renew *************************

    List<String> listAutoRenew = new ArrayList<>();
    String[] stringArrayAutoRenew;
    int positionAutoRenew = 0;
    public static boolean isDoneAutoRenew = false;

    public int listAutoRenewPopUp(Context context, TextView label, TextView setValue, List<String>
            list, int positionAutoRenewNew, final NavController navController) {
        mContext = context;
        this.label = label;
        this.setValue = setValue;
        this.navController = navController;
        listAutoRenew.clear();
        listAutoRenew.addAll(list);
        dialog = new BottomSheetDialog(mContext);
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view1 = layoutInflater.inflate(R.layout.pop_up_list, null);
        dialog.setContentView(view1);
        dialog.setCancelable(false);

        npListWhenToRemind = view1.findViewById(R.id.npList);
        btnCross = view1.findViewById(R.id.btnCross);
        btnDone = view1.findViewById(R.id.btnDone);

        stringArrayAutoRenew = listAutoRenew.toArray(new String[0]);
        npListWhenToRemind.setMaxValue(listAutoRenew.size() - 1);
        npListWhenToRemind.setMinValue(0);
        npListWhenToRemind.setDisplayedValues(stringArrayAutoRenew);
        npListWhenToRemind.setWrapSelectorWheel(false);
        dialog.show();

        if (positionAutoRenewNew > -1 && !isDoneAutoRenew) {
            npListWhenToRemind.setValue(positionAutoRenewNew);
        } else {
            npListWhenToRemind.setValue(positionAutoRenew);
        }

        npListWhenToRemind.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                positionAutoRenew = newVal;
            }
        });

        btnCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDoneAutoRenew = true;
                setAutoRenew();
                navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
                    @Override
                    public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                        if (destination.getLabel().toString().equals("fragment_side_menu_add_reminder")) {
                            ((GlobalCLass) mContext.getApplicationContext()).getSideMenuAddReminderFragment().changeColor();
                        } else if (destination.getLabel().toString().equals("fragment_side_menu_edit_reminder")) {
                            ((GlobalCLass) mContext.getApplicationContext()).getSideMenuEditReminderFragment().changeColor();
                        }
                    }
                });
            }
        });

        return positionAutoRenew;
    }

    private void setAutoRenew() {
        Log.wtf("position", "" + positionProperty);
        slideUp(label);
        setValue.setVisibility(View.VISIBLE);
        if (positionAutoRenew == 0) {
            setValue.setText("Yes");
        } else {
            setValue.setText("No");
        }
        dialog.dismiss();
    }

    @Override
    public void onClick(View v) {
        if (v == btnCross) {
            dialog.dismiss();
        } else if (v == btnDone) {
            dialog.dismiss();
            slideUp(label);
//            setValue.setText(value);
            setValue.setVisibility(View.VISIBLE);
            int pos = npWhoYouAre.getValue();
            String[] data = npWhoYouAre.getDisplayedValues();
            selectedValuePosition = pos;
            setValue.setText(data[pos]);
            customerTypeKeyText = customerTypeKey[pos];
            whoYouAreValue = setValue.getText().toString();
            whoYouAreKey = customerTypeKeyText;
        }
    }
}
